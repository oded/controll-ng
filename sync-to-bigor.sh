#!/bin/bash

rsync -avz src/ bigororg@bigor.org.il:controll-ng/
rsync -avz html/ bigororg@bigor.org.il:public_html/controll/
rsync -avz wordpress-plugin/ bigororg@bigor.org.il:public_html/2015/wp-content/plugins/controll/

case "$1" in
    -m)
	inotifywait -qrm -e CLOSE_WRITE src html wordpress-plugin | while read dir event path; do
	    case "$dir" in
		src*)
		    rsync -avz src/ bigororg@bigor.org.il:controll-ng/
		;;
		html*)
		    rsync -avz html/ bigororg@bigor.org.il:public_html/controll/
		;;
		wordpress-plugin*)
		    rsync -avz wordpress-plugin/ bigororg@bigor.org.il:public_html/2015/wp-content/plugins/controll/
		;;
	    esac
	done
    ;;
esac
