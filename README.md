Technologies:
=============

Thes site is built on [Kohana](https://kohanaframework.org/) as an MVC framework, with [TWIG](http://twig.sensiolabs.org) as a template engine for views and MySQL as a backend data storage.

Setup:
========
Do this to get a valid source tree checkout (git not loading submodules by default sucks):

- git clone --recursive git@gitlab.geek.co.il:oded/controll-ng.git controll-ng

After checking out the source, the Kohana sources must be patched using this process:

    cd src/system
    patch -p1 < ../../configuration/kohana-translate.patch

To run a local testing server follow these steps:

1. Edit your `/etc/hosts` file and add the line `127.0.0.1 test.bigor.org.il`
1. Install docker (if its not available in your operating system, refer to https://docs.docker.com/installation/ ).
1. It may be a good idea to give yourself access to the docker commands, otherwise the script below has to be run as root.
   Usually this can be achieved by adding yourself to the `docker` user group, by running `sudo usermod -aG docker $USER`.
1. Run `./run-docker.sh` in the root of the project.

To test, point your browser at http://test.bigor.org.il/controll to access the ConTroll main site, or http://test.bigor.org.il/2015 to see the Bigor 2015 site.
The run-docker output will follow the Apache and application error logs.

## Testing source file changes

To make changes to a running test server, just edit the files on the project - files in Bigor's site, wordpress plugin, and the application's
`classes`, `views` and `config` are automatically synced on the server. For any other file you will need to re-run the `run-docker.sh` script.

## Testing database changes

The testing server includes a MySQL server which exposes the database through port 3306 on the docker container's virtual network interface.
To figure out the IP address of the virtual interface, run `docker inspect -f '{{.NetworkSettings.IPAddress}}' bigor`.

Additionally, the `docker exec` command can be used to start the mysql command line client included in the test server:

    docker exec -ti bigor mysql bigororg_controllng

The database is part of the image and is reset whenever the local server is restarted.
To persist database changes, either:
1. edit `configuration/database/docker-update.sql` and add and DML modifications you want.
1. change the content of the `controll.sql.gz` file directly
1. use `mysqldump` to generate a new SQL dump file and replace `controll.sql.gz` with the new dump

To get the MySQL general_log dumped to the docker's log monitor, run

    docker exec -ti bigor mysql bigororg_controllng -e 'set global general_log=1'

## Additional tools

The testing server is running in an isolated docker container and as such there is no access to its data other than through the HTTP server or the MySQL
server. If you are running a recent docker version (1.3 or above), you can also use the `docer exec` tool to get "SSH like" access to the running
service by executing the following command:

    docker exec -ti bigor bash

### Accessing the site

The admin panel: http://test.bigor.org.il/controll/admin

The wordpress site: http://test.bigor.org.il/2015/

There is an admin user account with the email `admin@bigor.org.il` and the password `admin`.