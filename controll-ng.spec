Summary:        ConTroll
Name:           controll-ng
Version:        2.0.0.855
Release:        1%{?dist}
Group:          Servers/Internet
License:        Commercial, proprietary (copyright reserved, distribution rights reserved)
URL:            http://gitlab.geek.co.il/oded/controll-ng
Packager:       Oded Arbel <oded@geek.co.il> 
BuildArch:      noarch
Source0:        %{name}-%{version}.tar.gz
# add patches, if any, here
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id_u})
# build and runtime requirements here

BuildRequires:	rsync perl
Requires:       mod_php >= 5.5 php-cgi >= 5.5 php-pdo >= 5.5 php-mysql >= 5.5 php-pecl(opcache) >= 7.0
Requires:		mysql-server mysql
Requires:       policycoreutils-python

%description

%prep
rm -rf $RPM_BUILD_ROOT
%setup -q

%build

# fix app base path
perl -pi -e '
	m/define.*BASE_PATH/ and $_ = "define(\"BASE_PATH\", \"%{_datadir}/%{name}\");";
' html/index.php

%install
rm -rf $RPM_BUILD_ROOT

install -d $RPM_BUILD_ROOT%{_datadir}/%{name}

# install public HTML content and apache configuration
rsync -a html $RPM_BUILD_ROOT%{_datadir}/%{name}/
rsync -a src/ $RPM_BUILD_ROOT%{_datadir}/%{name}/

# install HTML modules
#install -d $RPM_BUILD_ROOT%{_datadir}/%{name}/html/modules
#rsync -a --exclude='.DS_Store' html-modules/html5forms/shared/ $RPM_BUILD_ROOT%{_datadir}/%{name}/html/modules/html5forms/

# install database configuration files
install -d $RPM_BUILD_ROOT%{_datadir}/%{name}/database
install -m644 configuration/database/* $RPM_BUILD_ROOT%{_datadir}/%{name}/database

# apache configuration
install -D -m644 configuration/httpd.conf %{buildroot}%{_sysconfdir}/httpd/conf.d/controll.conf

# -----------------------------------------------------------------------------

%clean
rm -rf $RPM_BUILD_ROOT

# -----------------------------------------------------------------------------

%post
semanage fcontext -a -t httpd_sys_rw_content_t "%{_datadir}/%{name}/application/(cache|logs)(/.*)?"
restorecon -R %{_datadir}/%{name}/application
(getsebool httpd_can_network_connect | grep -q on) || setsebool -P httpd_can_network_connect 1
(getsebool httpd_can_sendmail | grep -q on) || setsebool -P httpd_can_sendmail 1
# reload apache if needed
/etc/init.d/httpd status 2>&1 > /dev/null  && /etc/init.d/httpd reload 2>&1 > /dev/null
/bin/true

# make sure MySQL is up and running
chkconfig mysqld on
service mysqld status || service mysqld start

%preun

# -----------------------------------------------------------------------------

%files
%defattr(-,root,root,0755)
%config %{_sysconfdir}/httpd/conf.d/controll.conf
%config %{_datadir}/%{name}/application/config
%dir %attr(0775,apache,apache) %{_datadir}/%{name}/application/cache
%dir %attr(0775,apache,apache) %{_datadir}/%{name}/application/logs
%{_datadir}/%{name}/application/views
%{_datadir}/%{name}/application/classes
%{_datadir}/%{name}/application/vendor
%{_datadir}/%{name}/application/*.php
%{_datadir}/%{name}/system
%{_datadir}/%{name}/modules
%{_datadir}/%{name}/cli.php
%{_datadir}/%{name}/database
%{_datadir}/%{name}/html

# -----------------------------------------------------------------------------

%changelog
* Thu Aug 15 2013 Oded Arbel <oded@geek.co.il>  0:2.0-1tbl
- First build
