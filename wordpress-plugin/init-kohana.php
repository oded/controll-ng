<?php
define('BASE_PATH', '/home2/bigororg/controll-ng');
define('DOCROOT', '/home2/bigororg/public_html/controll');
$application = BASE_PATH.'/application';
$modules = BASE_PATH.'/modules';
$system = BASE_PATH.'/system';
define('EXT', '.php');
define('APPPATH', realpath($application).DIRECTORY_SEPARATOR);
define('MODPATH', realpath($modules).DIRECTORY_SEPARATOR);
define('SYSPATH', realpath($system).DIRECTORY_SEPARATOR);
unset($application, $modules, $system);

if ( ! defined('KOHANA_START_TIME'))
	define('KOHANA_START_TIME', microtime(TRUE));
if ( ! defined('KOHANA_START_MEMORY'))
	define('KOHANA_START_MEMORY', memory_get_usage());

// Bootstrap the application
require APPPATH.'bootstrap'.EXT;
