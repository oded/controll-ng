<?php
/**
 * @package ConTroll
 */
/*
Plugin Name: ConTroll
Plugin URI: http://bigor.org.il/
Description: ConTroll NG integration for Wordpress sites
Version: 0.0.1
Author: Oded Arbel
Author URI: http://geek.co.il
License: GPLv2 or later
Text Domain: controll
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define( 'CONTROLL_VERSION', '0.0.2' );
define( 'CONTROLL__MINIMUM_WP_VERSION', '4.0' );
define( 'CONTROLL__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'CONTROLL__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'CONTROLL_BASE_INSTALL', plugin_dir_path(__FILE__).'../../../../../controll-ng');
define( 'CONTROLL__DOCROOT_DIR', plugin_dir_url(__FILE__).'../../../../controll');

register_activation_hook( __FILE__, array( 'ConTroll', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'ConTroll', 'plugin_deactivation' ) );

require_once( CONTROLL__PLUGIN_DIR . 'init-kohana.php');
require_once( CONTROLL__PLUGIN_DIR . 'class.controll.php' );
//require_once( CONTROLL__PLUGIN_DIR . 'class.page-templates.php' );

add_action( 'init', array( 'ConTroll', 'init' ) );

if ( is_admin() ) {
//	require_once( CONTROLL__PLUGIN_DIR . 'class.controll-admin.php' );
//	add_action( 'init', array( 'ConTrollAdmin', 'init' ) );
}

