<?php

class ConTroll {

	private static $initialized = false;
	private static $tag_actions = [
			'controll-add-event' => 'handle_add_event',
			'controll-volunteer' => 'handle_volunteer_form',
	];
	
	public static function init() {
		if ( ! self::$initialized ) {
			self::init_hooks();
		}
	}

	/**
	 * Initializes WordPress hooks
	 */
	private static function init_hooks() {
		self::$initialized = true;
		
		register_post_type ('controll_add_event', [
			'public' => true,
			'exclude_from_search' => true,
			'show_in_nav_menus' => false,
			'hierarchical' => true,
			'capability_type' => 'page',
			'supports' => ['title','editor','page-attributes'],
			'can_export' => false,
		]);

		// add more buttons to the html editor
		//add_action( 'admin_print_footer_scripts', [ 'ConTroll', 'add_quicktags' ] );
		add_filter( 'the_content', [ 'ConTroll', 'add_controll_content' ] );
		add_action( 'wp_head', ['ConTroll', 'add_controll_header' ]);
	}
	
	public static function plugin_activation() {
	}
	
	public static function plugin_deactivation() {
	}

	public static function add_quicktags() {
		if (!wp_script_is('quicktags'))
			return;
		
		?>
		<script type="text/javascript">
		QTags.addButton( 'controll_addevent', 'ConTroll:AddEvent', '<!--add-event-->', '', '', 'Add ConTroll add event form', 12 );
	    </script>
		<?php
	}
	
	public static function add_controll_content($content) {
		Base_Template_Controller::$default = 'embed';
		
		// handle password resets, that can happen anywhere
		if (@$_GET['resetpassword'])
			return self::render_password_reset($_GET['resetpassword']);
		
		foreach (static::$tag_actions as $tag => $action) {
			if (strstr($content, "[$tag]"))
				$content = str_replace("[$tag]", self::$action(), $content);
		}
		$content = preg_replace_callback("/\[controll:([^\]]+)\]/",function($matches) {
			return self::render_controll_route($matches[1]); 
		},$content);
		
		// capture redirects to controllers that we expect be implemented in wordpress
		Base_Template_Controller::setRedirectRules([
				'/' => site_url() . "/כנס-ביגור/רשימת-ארועים",
				'/events/list' => site_url() . "/כנס-ביגור/רשימת-ארועים",
				'/event' => site_url() . "/כנס-ביגור/ארוע?id=",
				'/events/checkout' => site_url() . "/כנס-ביגור/רכישת-כרטיסים",
				'/events/registration-phone-complete' => site_url() . "/כנס-ביגור/הזמנה-טלפונית",
				'/events/registration-complete' => site_url() . "/כנס-ביגור/אישור-הרשמה",
				'/shop' => site_url() . "/כנס-ביגור/מוצר?id=",
				//'/shop' => site_url() . "/כנס-ביגור/חנות",
		]);
		
		return $content;
	}
	
	public static function render_controll_route($route) {
		// translate query string params to kohana route params
		$route = preg_replace_callback(',\{([^\}]+)\},', function($m) { return $_GET[$m[1]]; }, $route);
		$req = Request::factory($route, array(), FALSE);
		$req->query($_GET);
		$req->post($_POST);
		$req->basedir = CONTROLL__DOCROOT_DIR;
		return $req->execute()->send_headers(TRUE)->body();
	}
	
	public static function render_password_reset($email) {
		// translate query string params to kohana route params
		$req = Request::factory('/user/resetpassword', array(), FALSE);
		$req->query([ 'email' => $email ]);
		$req->post($_POST);
		$req->basedir = CONTROLL__DOCROOT_DIR;
		return $req->execute()->send_headers(TRUE)->body();
	}
	
	public static function handle_volunteer_form() {
		@list($selfpath,$noop) = explode('?',$_SERVER["REQUEST_URI"]);
		Base_Template_Controller::setRedirectRules([
			'User/volunteer' => $selfpath,
		]);
		
		$querystring = $_GET;
		$route = '/user/volunteer';		
		$req = Request::factory($route, array(), FALSE);
		$req->query($querystring);
		$req->post($_POST);
		$req->basedir = CONTROLL__DOCROOT_DIR;
		return $req->execute()->send_headers(TRUE)->body();
	}
	
	public static function handle_add_event() {
		@list($selfpath,$noop) = explode('?',$_SERVER["REQUEST_URI"]);
		Base_Template_Controller::setRedirectRules([
			'/events/add' => $selfpath,
			'/events/addthanks' => $selfpath . '?thanks=1',
		]);
		
		$querystring = $_GET;
		$route = '/events/add';
		if (@$_GET['thanks'])
			$route = '/events/addthanks';
		elseif (@$_GET['resetpassword']) {
			$route = '/user/resetpassword';
			$querystring['email'] = $_GET['resetpassword'];
		} 
		
		$req = Request::factory($route, array(), FALSE);
		$req->query($querystring);
		$req->post($_POST);
		$req->basedir = CONTROLL__DOCROOT_DIR;
		return $req->execute()
			->send_headers(TRUE)
			->body();
	}
	
	public static function add_controll_header() {
		?>
	<link rel="stylesheet" href="/controll/style/number-polyfill.css" type="text/css" media="screen,projection" />
	<link rel="stylesheet" href="/controll/style/jquery-ui.css" data-realhref="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	
	<!-- script type="text/javascript" src="/controll/script/jquery-1.11.0.min.js" data-realsrc="http://code.jquery.com/jquery-1.10.2.min.js"></script -->
	<script type="text/javascript" src="/controll/script/jquery.tools.min.js" data-realsrc="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script type="text/javascript" src="/controll/script/jquery-ui.js" data-realsrc="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script type="text/javascript" src="/controll/script/number-polyfill.min.js"></script>
	<script type="text/javascript" src="/controll/script/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/controll/script/modernizr-2.6.2.min.js"></script>
	<!--script type="text/javascript" src="/controll/script/shortcut.js"></script -->
	
	<script type="text/javascript" src="/controll/script/authenticate.js"></script>
	<script type="text/javascript" src="/controll/script/utils.js"></script>
		<?php
	}
	
}
