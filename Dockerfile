FROM centos:centos7

RUN rpm -U http://mirror.nonstop.co.il/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
RUN yum install -y httpd php php-mbstring php-mysql php-pdo php-process php-xml inotify-tools mariadb-server mariadb hostname postfix

RUN mysql_install_db --user=mysql > /dev/null
RUN mysqld_safe --user mysql --bootstrap <<<"FLUSH PRIVILEGES;GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;"
RUN chmod a+rwx /var/spool/postfix/maildrop/

ADD configuration/database /home2/bigororg/controll-data
RUN /bin/bash -xc '\
    mysqld_safe --user mysql & \
    sleep 5; \
    mysql <<<"CREATE DATABASE bigororg_controllng; CREATE DATABASE bigororg_wo2608;" && \
    gunzip < /home2/bigororg/controll-data/controll.sql.gz | mysql bigororg_controllng && \
    gunzip < /home2/bigororg/controll-data/bigor.sql.gz | mysql bigororg_wo2608 && \
    mysql <<<"GRANT ALL PRIVILEGES ON bigororg_wo2608.* TO \"bigororg_wo2608\"@\"localhost\" identified by \"rVK97osSY87n\";" && \
    mysql <<<"GRANT ALL PRIVILEGES ON bigororg_controllng.* TO \"bigororg_system\"@\"localhost\" identified by \"vfQJm7AgKSSzAuGE\";" && \
    mysql <<<"GRANT ALL PRIVILEGES ON bigororg_wo2608.* TO \"bigororg_wo2608\"@\"%\" identified by \"rVK97osSY87n\";" && \
    mysql <<<"GRANT ALL PRIVILEGES ON bigororg_controllng.* TO \"bigororg_system\"@\"%\" identified by \"vfQJm7AgKSSzAuGE\";" && \
    mysql <<<"GRANT ALL ON *.* TO \"root\"@\"%\";" && \
    mysql < /home2/bigororg/controll-data/docker-updates.sql && \
    mysqladmin shutdown'

ADD html/php.ini /etc/php.ini
ADD configuration/httpd.bigor-2015.conf /etc/httpd/conf.d/bigor-2015.conf

ADD bigor-2015-html /home2/bigororg/public_html/2015
ADD html /home2/bigororg/public_html/controll
ADD wordpress-plugin /home2/bigororg/public_html/2015/wp-content/plugins/controll
ADD src /home2/bigororg/controll-ng
ADD entrypoint.sh /

RUN chmod 777 /home2/bigororg/controll-ng/application/cache /home2/bigororg/controll-ng/application/logs

EXPOSE 80
EXPOSE 3306
CMD [ "/entrypoint.sh" ]
