use bigororg_wo2608;

update wp_rqoi_options set option_value = "http://test.bigor.org.il/2015/" where option_name in ("siteurl","home");
update wp_rqoi_options set option_value = 'a:9:{i:0;s:21:"controll/controll.php";i:1;s:45:"enable-media-replace/enable-media-replace.php";i:2;s:45:"enhanced-text-widget/enhanced-text-widget.php";i:3;s:19:"jetpack/jetpack.php";i:4;s:49:"recently-updated-pages/recently_updated_pages.php";i:5;s:41:"wordpress-importer/wordpress-importer.php";i:6;s:41:"wordpress-language/wordpress-language.php";i:7;s:37:"wp-custom-titles/wp-custom-titles.php";i:8;s:31:"wp-google-maps/wpGoogleMaps.php";}' where option_name = 'active_plugins';

use bigororg_controllng;

update event_occurrences set status = 1; 
update event_occurrences set start = date_add(from_days(to_days('2015-04-05') + floor(rand()*2)), interval (8 + floor(rand()*10)) hour) where event_id in (select id from events where convention_id = 2);

insert into users (first_name, last_name, email, credentials, registered) values ('Bigor', 'Admin', 'admin@bigor.org.il', '$2y$14$fpSov2cdfaGTwLSzl9QEd.Ayv8U1f4wOP8gPdsb1PcaZt1RsmN7na', now());
insert into administrators (user_id, role_id) values (LAST_INSERT_ID(),1);
