SET NAMES utf8;
SET character_set_client = utf8;
SET TIME_ZONE='+00:00';

-- skip the next few lines when you do not have full control over the database server
DROP DATABASE IF EXISTS controll;

CREATE DATABASE controll
	CHARACTER SET = utf8
	COLLATE = utf8_general_ci;
-- up to here
	
GRANT ALL on controll.* to 'controll'@'localhost' identified by 'controll';

USE controll; 

CREATE TABLE `organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(45) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `organizations` (`key`, `title`) VALUES ('העמותה','העמותה למשחקי תפקידים בישראל');

CREATE TABLE `conventions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(45) NOT NULL,
  `title` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `start` DATETIME NOT NULL, -- not normative - for display purposes only
  `end` DATETIME NOT NULL, -- not normative - for display purposes only
  `hostname` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `conventions` (`key`, `title`, `location`, `start`, `end`) VALUES ('ביגור-2014', 'ביגור 2014', 'איפשהו', '2014-04-16 09:00', '2014-04-18 14:00');

CREATE TABLE `convention_organizers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organization_id` int(10) unsigned NOT NULL,
  `convention_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_organizer` (`convention_id`,`organization_id`),
  FOREIGN KEY `fk_organization_id` (`organization_id`) REFERENCES `organizations`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `convention_organizers` (`organization_id`, `convention_id`) VALUES(1,1);

CREATE TABLE `parameters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `convention_id` int(10) unsigned NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `value` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX (`name`),
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `date_of_birth` DATETIME,
  `phone` VARCHAR(15), -- E.164
  `credentials` VARCHAR(255) NOT NULL, -- login token using password_hash/oauth/openid
  `registered` DATETIME NOT NULL,
  `last_login` DATETIME,
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`first_name`, `last_name`, `email`, `credentials`, `registered`) VALUES
	('עודד', 'ארבל', 'oded@geek.co.il', 'https://www.google.com/accounts/o8/id?id=AItOawl32iBPq_HbEQMzSSODIey5KjJ-0Hk29pA', NOW()),
	('איתי', 'בן עברי', 'itai.qed@gmail.com', '', NOW()),
	('קרן', 'ארבל', 'sylver@geek.co.il', '', NOW()),
	('צל', 'ליפטרסיר', 'dead@geek.co.il', '', NOW()),
	('ארז', 'רגב', 'taleslord@gmail.com', '', NOW());

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(45) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles` (`key`, `title`) VALUES
	('admin','Administrator');

CREATE TABLE `administrators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `organization_id` int(10) unsigned, -- set to NULL to be a super admin
  `convention_id` int(10) unsigned, -- set to NULL to be an organization admin
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_unique` (`user_id`, `organization_id`, `convention_id`),
  FOREIGN KEY `fk_user_id` (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_organization_id` (`organization_id`) REFERENCES `organizations`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_role_id` (`role_id`) REFERENCES `roles`(`id`) ON DELETE RESTRICT  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `administrators` (`user_id`, `role_id`) VALUES (1,1);

CREATE TABLE `organization_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `member_id` VARCHAR(50) NULL,
  `likes_spam` BOOLEAN NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_user_id` (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_organization_id` (`organization_id`) REFERENCES `organizations`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `organization_members` (`user_id`, `organization_id`) VALUES
	(2, 1), (3, 1), (4, 1);

CREATE TABLE `event_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(45) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `prevent_conflicts` BOOLEAN NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `event_types` (`key`, `title`) VALUES 
	('הרצאה','הרצאה'),
	('פאנל','פאנל'),
	('משחק-תפקידים','משחק תפקידים'),
	('משחק-חי','משחק חי'),
	('טורניר','טורניר'),
	('סדנא','סדנא'),
	('אירוע-קהילתי', 'אירוע קהילתי');

CREATE TABLE `convention_event_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_type_id` int(10) unsigned NOT NULL,
  `convention_id` int(10) unsigned NOT NULL,
  `default_price` DECIMAL(5,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_event_type_id` (`event_type_id`) REFERENCES `event_types`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE CASCADE  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `convention_event_types` (`event_type_id`, `convention_id`) VALUES
	(1,1), (2,1), (3,1), (4,1), (5,1), (6,1), (7,1);

CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `convention_id` int(10) unsigned NOT NULL,
  `key` VARCHAR(45) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `order` int(10) NOT NULL DEFAULT 0,
  UNIQUE KEY `key_UNIQUE` (`key`,`convention_id`),
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `locations` (`convention_id`, `key`, `title`) VALUES
	(1, 'חדר-301', 'חדר 301'),
	(1, 'אולם-1', 'אולם 1'),
	(1, 'אולם-3', 'אולם 3');

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `convention_id` int(10) unsigned NOT NULL,
  `key` VARCHAR(45) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `convention_event_type_id` int(10) unsigned NOT NULL,
  `description` TEXT,
  `start` DATETIME NOT NULL,
  `length` INT NOT NULL,
  `min_attendees` int(10) NOT NULL DEFAULT 0,
  `max_attendees` int(10) DEFAULT NULL,
  `tags` TEXT, -- Time Slot, Genre, System, Age Restrictions, Type of Convention Event, Cloud Tagging - uses PHP serialization
  `media` TEXT, -- embedded images and such - uses PHP serialization, maybe convert to external BLOBs?
  `status` int(10) NOT NULL DEFAULT 0, -- enum into the Model_Event::Statuses field
  `prevent_conflicts` BOOLEAN NOT NULL DEFAULT 1, -- show warning to the user when registering to two events that happen at the same time
  `location_id` int(10) unsigned,
  `price` DECIMAL(5,2) NOT NULL DEFAULT 0,
  `logistical_requirments` TEXT,
  `notes_to_attendees` TEXT,
  `notes_to_staff` TEXT,
  `registration_required` BOOLEAN NOT NULL DEFAULT 1, -- if registration isn't required, attendance is free  
  `max_free_tickets` int(10) NOT NULL DEFAULT 0,
  `reserved_tickets` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`,`convention_id`),
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE RESTRICT,
  FOREIGN KEY `fk_convention_event_type_id` (`convention_event_type_id`) REFERENCES `convention_event_types`(`id`) ON DELETE RESTRICT,
  FOREIGN KEY `fk_location_id` (`location_id`) REFERENCES `locations`(`id`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `events` (`convention_id`, `key`, `title`, `convention_event_type_id`, `start`, `length`, `min_attendees`, `max_attendees`, `registration_required`, `status`, `location_id`, `price`) VALUES
	(1, 'שירת-פילק', 'שירת פילק', 7, '2014-04-16 22:00', 240, 2, NULL, 0, 1, 1, 0),
	(1, 'טורניר-מנצקין', 'טורניר מנצ`קין', 5, '2014-04-17 15:00', 60, 6, 30, 1, 1, 2, 10),
	(1, 'אל-תוך-המבוך', 'אל תוך המבוך', 3, '2014-04-17 17:00', 240, 5, 7, 1, 1, 3, 25),
	(1, 'stuff1', 'לא מאושר 1', 4, 0, 30, 1, null, 0, 0, null, 10),
	(1, 'stuff2', 'לא מאושר 2', 4, 0, 60, 1, null, 0, 0, null, 10),
	(1, 'stuff3', 'לא מאושר 3', 4, 0, 120, 1, null, 0, 0, null, 10);

CREATE TABLE `event_hosts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_event_id` (`event_id`) REFERENCES `events`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_user_id` (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `event_hosts` (`event_id`, `user_id`) VALUES
	(1, 3), (2, 2), (3, 5);
	
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `credit` DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT '0',
  `type` VARCHAR(10) NOT NULL,
  `used` BOOLEAN NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_user_id` (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL DEFAULT 1, -- number of tickets
  `registered` TIMESTAMP NOT NULL,
  `price` DECIMAL(5,2) NOT NULL, -- 0 means "got for free"
  `status` int(3) unsigned NOT NULL DEFAULT 0, -- registrations are 'in-process', 'pending', 'fulfilled'
  `issued` BOOLEAN NOT NULL DEFAULT 0, -- whether the tickets have been given out already (or the user had self-printed)
  `discount_code` VARCHAR(45) DEFAULT NULL, -- discount code used during purchase
  `transaction_id` VARCHAR(45) DEFAULT NULL, -- the credit card transaction id returned by the payment processor, or the work "cash"
  `cancel_transaction_id` VARCHAR(45) DEFAULT NULL, -- the payment processors cancellation approvement
  PRIMARY KEY (`id`),
  INDEX `user_event` (`event_id`, `user_id`),
  INDEX ('status'),
  FOREIGN KEY `fk_event_id` (`event_id`) REFERENCES `events`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_user_id` (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* TODO: not required for mvp

CREATE TABLE `volunteering_gigs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(45) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `convention_volunteering_gigs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `volunteering_gig_id` int(10) unsigned NOT NULL,
  `convention_id` int(10) unsigned NOT NULL,
  `free_tickets` int(10) NOT NULL DEFAULT 0, -- per shift being volunteered on
  `location` VARCHAR(255) NOT NULL, -- should be the same content as `events`.`location`
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_volunteering_gig_id` (`volunteering_gig_id`) REFERENCES `volunteering_gigs`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `volunteering_shifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(45) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `start` DATETIME NOT NULL,
  `end` DATETIME NOT NULL,
  `required` int(10) unsigned NOT NULL, -- how many volunteers are needed 
  `convention_volunteering_gig_id` int(10) unsigned NOT NULL, 
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`),
  FOREIGN KEY `fk_convention_volunteering_gig_id` (`convention_volunteering_gig_id`) REFERENCES `convention_volunteering_gigs`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `volunteers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `volunteering_shift_id` int(10) unsigned NOT NULL, -- the requested shift
  `requested` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, -- time of request
  `scheduled` DATETIME DEFAULT NULL, -- time it was scheduled and the user notified of the scheduling
  `confirmed` DATETIME DEFAULT NULL, -- time the user has confirmed 
  `briefed` DATETIME DEFAULT NULL, -- the the user has been briefed after they showed up. Free tickets can only be claimed for briefed volunteerings
  `feedback` TEXT, -- stuff the user has said about the gig
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_user_id` (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_volunteering_shift_id` (`volunteering_shift_id`) REFERENCES `volunteering_shifts`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

*/

CREATE TABLE `merchandise` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(50) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT,
  `media` BLOB DEFAULT NULL,
  `price` DECIMAL(5,2) NOT NULL DEFAULT 0,
  `convention_id` int(10) unsigned NOT NULL,
  `active` BOOLEAN DEFAULT 1,
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
CREATE TABLE `merchandise_features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `merchandise_id` int(10) unsigned NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_merchandise_id` (`merchandise_id`) REFERENCES `merchandise`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `merchandise_skus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(255) NOT NULL,
  `merchandise_id` int(10) unsigned NOT NULL,
  `stock` int(10) DEFAULT NULL, -- NULL means, "on demand" i.e. no limit
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_merchandise_id` (`merchandise_id`) REFERENCES `merchandise`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `merchandise_sku_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `merchandise_feature_id` int(10) unsigned NOT NULL,
  `merchandise_sku_id` int(10) unsigned NOT NULL,
  `kind` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_merchandise_sku_id` (`merchandise_sku_id`) REFERENCES `merchandise_skus`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_merchandise_feature_id` (`merchandise_feature_id`) REFERENCES `merchandise_features`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `merchandise_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `merchandise_sku_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL DEFAULT 1, -- number of items
  `user_notes` TEXT DEFAULT NULL,
  `ordered` TIMESTAMP NOT NULL,
  `price` DECIMAL(5,2) NOT NULL, -- 0 means "got for free"
  `status` int(3) unsigned NOT NULL DEFAULT 0, -- transactions are 'in-process', 'pending', 'fulfilled'
  `fullfilled` BOOLEAN NOT NULL DEFAULT 0, -- whether the items have been given out already
  `discount_code` VARCHAR(45) DEFAULT NULL, -- discount code used during purchase
  `transaction_id` VARCHAR(45) DEFAULT NULL, -- the credit card transaction id returned by the payment processor, or the work "cash"
  `cancel_transaction_id` VARCHAR(45) DEFAULT NULL, -- the payment processors cancellation approvement
  PRIMARY KEY (`id`),
  INDEX `user_purchase` (`merchandise_sku_id`, `user_id`),
  INDEX (`status`),
  FOREIGN KEY `fk_merchandise_sku_id` (`merchandise_sku_id`) REFERENCES `merchandise_skus`(`id`) ON DELETE CASCADE,
  FOREIGN KEY `fk_user_id` (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `audit_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time` TIMESTAMP NOT NULL,
  `convention_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `entity` VARCHAR(45) NOT NULL,
  `transaction_por` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY `fk_user_id` (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT,
  FOREIGN KEY `fk_convention_id` (`convention_id`) REFERENCES `conventions`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
