#!/bin/bash
HOST="$1"
USER="$2"

login="$USER@$HOST"
basedir="$(dirname $(dirname $0))"

version="$(cat configuration/version)"

# copy build script
( cat <<EOF
set -x
# make sure we can build
rpm -q rpm-build >/dev/null || yum install -y rpm-build yum-utils
# enable access to MySQL server from remote connections (use only for dev)
rpm -q mysql-server >/dev/null || { yum install -y mysql-server; service mysqld start; mysql -e "grant all on *.* to \"root\"@\"%\""; }
rpm -q epel-release >/dev/null || { rpm -Uhv http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm; yum-config-manager --enable epel; }
rpm -q remi-release >/dev/null || { rpm -Uhv http://rpms.famillecollet.com/enterprise/remi-release-6.rpm; yum-config-manager --enable remi remi-test; }
rpmbuild -ts \$(dirname \$0)/controll-ng-$version.tar.gz && \
    yum-builddep rpmbuild/SRPMS/controll-ng-$version* -y && \
    rpmbuild --rebuild rpmbuild/SRPMS/controll-ng-$version* --clean --rmsource --rmspec && \
    yum install -y php-pecl-zip --noplugins && \
    yum localinstall --nogpgcheck --noplugins -y rpmbuild/RPMS/noarch/controll-ng-$version-*.rpm && \
    rm -rf rpmbuild \$(dirname \$0)/controll-ng-$version.tar.gz
service httpd status || service httpd start
EOF
) | ssh -x $login 'cat > build.sh'

# copy package over
(
    cd $basedir
    tar -zc . \
	--exclude=".git*" \
	--exclude="logs/*" \
	--transform "s/^./controll-ng-$version/" | ssh -x $login 'cat > controll-ng-'"$version"'.tar.gz'
)

# execute build
ssh -ttx $login 'chmod 755 build.sh; sudo -i $(pwd)/build.sh; rm -f build.sh'
