#!/bin/bash

type="${1:-monitor}"

HOST=root@vdev

PWD="$(dirname $0)"
(
    cd $PWD/..
    case "$type" in
	monitor)
	inotifywait -e modify,move,delete -r . -qm | \
	    fgrep -v \
"goutputstream
.svn
.git
configuration" --line-buffered | \
	    while read dir event file; do
		lpath="${dir#./}${file}"
		rpath="${lpath##src/}"
		(case "$rpath" in
			application/*|html/*|system/*|modules/*) 
				rsync -qavz -e 'ssh -x' $lpath $HOST:/usr/share/controll-ng/$rpath;;
		esac) && echo "$event -> $rpath" || echo "Error copying $lpath"
	    done
	;;
	full)
	;;
    esac
)
