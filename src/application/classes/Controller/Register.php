<?php

class Controller_Register extends Base_Template_Controller {
	
	const NEW_USER_CREDENTIALS = 'new-user-credential';
	protected $view = 'authenticate/new-user';
	
	public function action_newuser() {
		$this->view->return_url = $this->request->return_url;
		$this->view->email = $this->request->email;
		$this->view->fullname = $this->request->fullname;
		$this->view->date_of_birth = $this->request->dob;
		$this->view->phone = $this->request->phone;
		$this->template->content_title = 'רישום משתמש';
	}
	
	public function action_register() {
		if (!($cred = $this->session->get(self::NEW_USER_CREDENTIALS))) {
			// no new user credentials in the session store, reject
			$this->session->logout();
			$this->redirect(URL::base());
			return;
		}
	
		$this->session->delete(self::NEW_USER_CREDENTIALS);
	
		$nameparts = preg_split('/\s+/', trim($this->request->name), 2);
		$dob = DateTime::createFromFormat('d/m/Y', $this->request->date_of_birth, new DateTimeZone("UTC"));
		if ($dob !== false)
			$dob = $dob->getTimestamp();
		
		// check if the user was manually entered without an authentication toke
		try {
			$user = Model_User::byEmail($this->request->email);
			if ($user->credentials)
				$this->rejectSameEmail($cred);
			
			$user->credentials = $cred;
			$user->first_name = array_shift($nameparts);
			$user->last_name = array_pop($nameparts);
			$user->date_of_birth = $dob;
			$user->phone = $this->request->phone;
			$user->save();
			$this->session->login($user); // session login will update the last_login time and save
			$this->redirect($this->request->return_url);
		} catch (Exception_NotFound $e) {
			// fine! generate a new user
		}
		
		try {
			$user = Model_User::generate(array_shift($nameparts), array_pop($nameparts), $this->request->email,
				$dob, $this->request->phone);
			$user->credentials = $cred;
			$user->save();
			$this->session->login($user); // session login will update the last_login time and save
			$this->redirect($this->request->return_url);
		} catch (Exception_Duplicate $e) {
			// restart registration
			$this->rejectSameEmail();
		}
	}
	
	private function rejectSameEmail($cred) {
		$this->session->error("משתמש עם דואל זה כבר קיים במערכת");
		$this->session->set(self::NEW_USER_CREDENTIALS, $cred);
		$this->redirect(Route::get('authenticate')->uri([
			'controller' => 'register',
			'action' => 'newuser',
		]). '?' . join('&', [
			'return_url='.urlencode($this->request->return_url),
			'email='.urlencode($this->request->email),
			'fullname='.urlencode($this->request->name),
			'dob='.urlencode($this->request->date_of_birth),
			'phone='.urlencode($this->request->phone),
		]));
		exit;		
	}
}
