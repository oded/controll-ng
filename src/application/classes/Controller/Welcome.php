<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Base_Template_Controller {
	
	protected $auto_render = false;

	public function action_index()
	{
		$cons = Model_Convention::getAll();
		foreach ($cons as $con) {
			$uri = Route::get("convention")->uri(array('convention' => $con->key)) . '/article/1';
			error_log("Redirecting to " . $uri);
			self::redirect($uri);
			return;
		}
	}

}
