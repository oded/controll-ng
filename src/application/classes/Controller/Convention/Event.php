<?php

class Controller_Convention_Event extends Base_Template_Controller {
	
	public function action_index()
	{
		Model_Registration::garbageCollect(); // do this first because it affects 'available_tickets'
		
		switch ($this->request->post('action')) {
			case "register":
			case "update":
				return $this->action_register();
			case "checkout";
				return $this->action_checkout();
		}
		

		$this->view->event = $this->view->occurr = $this->convention->occurrenceByKey($this->request->param('event'));
		$this->view->page_title = $this->view->event->title;
		if ($this->user) {
			$this->view->user_registration = $this->view->event->registrations
				->where('user_id', '=', $this->user->id)
				->where('status', '<', Model_Registration::STATUS_CANCELLED)
				->find_all();
		}
	}

	public function action_list() {
		if ($this->request->show_event) {
			return $this->redirect('/event/' . $this->request->show_event);
		}
		Model_Registration::garbageCollect(); // do this first because it affects 'available_tickets'
		$this->setView('convention/eventlistsimple');
		$this->view->page_title = 'רשימת ארועים';
		$this->template->page_type = 'con-plan-page';
		$this->view->events = $this->convention->approved_events->order_by('event_occurrence.start')->find_all();
		$tags_heb = $tags_eng = [];
		foreach ($this->view->events as $ev) {
			foreach ($ev->event->tag_list as $tag) {
				if ($tag->title[0] <= 'z')
					$tags_eng[$tag->title] = $tag->key;
				else
					$tags_heb[$tag->title] = $tag->key;
			}
		}
		ksort($tags_eng); ksort($tags_heb);
 		$this->view->tag_list = $tags_heb + $tags_eng;
		$this->view->event_types = $this->convention->event_types->find_all();
	}
	
	public function action_register() {
		$occurr = $this->convention->occurrenceByKey($this->request->param('event'));
		if (!$this->user) {
			$this->session->error("רק משתמשים רשומים יכולים להרשם לארועים");
			$this->redirect('/event/' . $occurr->id);
		}
		
		if (! ((int)$this->request->amount) > 0) {
			$this->session->error("יש לציין את מספר הכרטיסים לרכישה");
			$this->redirect('/event/' . $occurr->id);
		}
		
		try {
			$reg = $occurr->register($this->user, $this->request->amount);
			Log::debug("Registered {$reg}");
		} catch (Database_Exception $e) {
			$this->session->error("יש כבר רישום לארוע על שמך");
		} catch (Exception_NoTickets $e) {
			Log::debug("Trying to reserve " . $this->request->amount . " tickets for " . $this->user->id .
				": " . $e->getMessage());
			$this->session->error("אין מספיק כרטיסים זמינים");
		}
		$this->redirect('/event/' . $occurr->id);
	}
	
	public function action_registrations() {
		$this->setView('convention/registrations-list');
		
		if (!$this->user) {
			// show the "must log in" page
			$this->setView("convention/login-required");
			$this->view->page_title = 'הצע ארוע';
			return;
		}
		
		if ($this->user->isAdmin() and $this->request->userid) {
			$user = new Model_User($this->request->userid);
			if (!$user->loaded())
				$this->session->error('משתמש לא קיים');
		} else {
			$user = $this->user;
		}
		
		$this->view->user = $user;
	}
	
	public function action_print() {
		$this->template = 'simple';
		$this->buildTemplate();
		$this->setView('convention/registrations-list');
		$this->view->print = true;
	}
	
	public function action_unregister() {
		$occurr = $this->convention->occurrenceByKey($this->request->param('event'));
		$nexturl = $this->request->referrer();
		if (!$nexturl) $nexturl = '/';
		if (!$this->user) {
			$this->session->error("רק משתמשים רשומים יכולים להרשם לארועים");
			$this->redirect($nexturl);
		}
		
		if ($this->request->regid){
			$reg = new Model_Registration($this->request->regid);
			if ($reg->loaded()) {
				$reg->cancel('user cancelled');
			}
			$this->redirect($nexturl);
		}
		
		foreach ($this->user->pending_registrations as $reg) {
			if ($reg->event->id == $occurr->id)
				$reg->cancel('user cancelled');
		}
		
		$this->redirect($nexturl);
	}
	
	public function action_checkout() {
		if (!$this->user) {
			// show the "must log in" page
			$this->setView("convention/login-required");
			$this->view->page_title = 'הצע ארוע';
			return;
		}
				
		$items = count($this->user->pending_registrations) + count($this->user->pending_orders); 
		if ($items < 1) {
			$this->session->error("סל הקניות ריק - יתכן וההזמנה בוטלה לאחר זמן רב של חוסר פעילות");		
			$this->redirect('/');
		}
		
		if ($this->request->type == 'phone') {
			$this->user->phone = $this->request->userphone;
			Model_Registration::setPhoneFulfilment($this->user);
			$this->redirect('/events/registration-phone-complete');
		}
		
		Model_Registration::startFulfilment($this->user, '/events/registration-complete');
	}
	
	public function action_registration_phone_complete() {
		if (!$this->user) {
			// show the "must log in" page
			$this->setView("convention/login-required");
			$this->view->page_title = 'הצע ארוע';
			return;
		}
		
		$this->setView('convention/reg-phone-thanks');
		$this->view->registrations = $this->user->phone_pending;
		return;
	}
	
	public function action_registration_complete() {
		if (!$this->user) {
			// show the "must log in" page
			$this->setView("convention/login-required");
			$this->view->page_title = 'הצע ארוע';
			return;
		}
		
		$this->setView('convention/reg-thanks');
		$this->view->page_title = 'סיום רישום';
	}
	
	public function action_add()
	{
		$this->view->page_title = 'הצע ארוע';
		if (!$this->user) {
			// show the "must log in" page
			$this->setView("convention/login-required");
			$this->view->page_title = 'הצע ארוע';
			return;
		}
		
		// handle the add form post
		if (HTTP_Request::POST == $this->request->method()) {
			$this->addEvent();
			return;	
		}			
		
		// show the form
		$this->setView("convention/addevent");
		$this->view->page_title = 'הצע ארוע';
		$this->view->event_types = $this->convention->event_types->find_all();
		
		if ($this->session->get('event_data')) {
			@list(
				$event_type, $event_title, $event_rule_system, $event_description, $event_notes_to_staff,
				$event_length, $event_min_attendees, $event_max_attendees, 
				$event_tag_kid_friendly, $event_tag_newbie_friendly, 
				$event_logistical_requirments, $event_scheduling_constraints, $event_tags
				) = $this->session->get_once('event_data');
			foreach ([
					'event_type' => $event_type, 
					'event_title' => $event_title, 
					'event_rule_system' => $event_rule_system,
					'event_desc' => $event_description, 
					'event_length' => $event_length, 
					'event_max_attendess' => $event_max_attendees, 
					'event_notes_to_staff' => $event_notes_to_staff, 
					'event_logistical_requirments' => $event_logistical_requirments,
					'event_min_attendees' => $event_min_attendees, 
					'event_tags' => $event_tags,
					'event_tag_kid_friendly' => $event_tag_kid_friendly, 
					'event_tag_newbie_friendly' => $event_tag_newbie_friendly,
					'event_scheduling_constraints' => $event_scheduling_constraints,
			] as $name => $value)
				$this->view->set($name, $value);
		}
		$this->view->rule_systems = Model_Rule_System::all();
	}
	
	public function action_addthanks() {
		$this->setView('convention/addevent-thanks');
		$this->view->page_title = 'הצע ארוע';
	}
	
	/**
	 * Handle the add event form
	 */
	public function addEvent() {
		$r = $this->request;
		if ($r->no_logistical_requirements)
			$r->event_logistical_requirments = null;
		try {
			$tags = explode(',',$r->event_tags);
			if ($r->event_tag_kid_friendly) $tags[] = Model_Event::TAG_KID_FRIENDLY;
			if ($r->event_tag_newbie_friendly) $tags[] = Model_Event::TAG_NEWBIE_FRIENDLY;
			$eventType = $this->convention->eventType($r->event_type);
			$e = Model_Event::generate($this->convention, $this->user, $eventType, 
				$r->event_title, $r->event_description, trim($r->event_rule_system), join(',',$tags)); 
			Model_Event_Occurrence::generate($e, 0, $r->event_length * 60,
				$r->event_max_attendees, $r->event_notes_to_staff, $r->event_logistical_requirments,
				$r->event_scheduling_constraints, $r->event_min_attendees, $eventType->type->prevent_conflicts);
			// send the user a confirmation email
			$email = new Email("אישור הגשת ארוע לכנס {$this->convention->title}", 'new-event-thanks', [
					'event' => $e,
			]);
			$email->send($this->user->email);
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate entry")) {
				$this->session->error("כבר קיים ארוע בשם זה, אנא בחר שם אחר");
				$this->session->set('event_data', [
						$r->event_type, $r->event_title, trim($r->event_rule_system), $r->event_description, $r->event_notes_to_staff,
						$r->event_length, $r->event_min_attendees, $r->event_max_attendees, 
						$r->event_tag_kid_friendly, $r->event_tag_newbie_friendly, 
						$r->event_logistical_requirments, $r->event_scheduling_constraints, $r->event_tags
				]);
				$this->redirectToAction('add');
				return;
			}
			Log::error("Exception in addEvent: " . $e->getMessage());
			throw new HTTP_Exception_500("Database error", null, $e);
		} catch (ORM_Validation_Exception $e) {
			Log::error("Validation error: {$e}, " . print_r($this->request->post(), true));
			$this->session->error("חסרים נתונים חיוניים בטופס הארוע. אנא בדוק את הפרטים שנית.");
			$this->session->set('event_data', [
					$r->event_type, $r->event_title, trim($r->event_rule_system), $r->event_description, $r->event_notes_to_staff,
					$r->event_length, $r->event_min_attendees, $r->event_max_attendees, 
					$r->event_tag_kid_friendly, $r->event_tag_newbie_friendly, 
					$r->event_logistical_requirments, $r->event_scheduling_constraints, $r->event_tags
			]);
			$this->redirectToAction('add');
			return;
		}
		$this->redirectToAction('addthanks');
	}
	
	public function action_show() {
		$this->template = 'embed';
		$this->buildTemplate();
		$this->setView('convention/email/new-event-thanks');
		$id = (int)$this->request->param('event');
		$event = new Model_Event($id);
		if (!$event->loaded()) throw new Exception("Failed to load event {$id}!");
		$this->view->event = $event;
		Log::info("rule system: {$this->view->event->rule_system_id}, loaded: {$this->view->event->loaded()}");
	}
	
}
