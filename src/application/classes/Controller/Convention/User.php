<?php

class Controller_Convention_User extends Base_Template_Controller {
	
	public function action_index() {
	}
	
	public function action_resetpassword() {
		$this->setView('convention/user/resetsent');
		try {
			Log::debug("Checking reset for {$this->request->email}");
			$user = Model_User::byEmail($this->request->email);
			if ($user->isPasswordAuthenticated())
				$user->sendResetPassword();
			else {
				Log::debug("User {$this->request->email} is authed by external provider");
				$this->session->error('הכתובת רשומה על ספק אימות חיצוני: '. 
						explode(':',$user->credentials)[0] . ', אנא נסו להכנס מחדש בעזרת האימות הנ"ל');
			}
		} catch (Exception_NotFound $e) {
			// nothing will happen, we show the same view as we don't wont attackers to guess addresses
		}
	}
	
	public function action_doreset() {
		try {
			$user = Model_User::byEmail($this->request->email);
			if (!$user->isPasswordAuthenticated())
				throw new Exception_NotFound();
			if (!$user->hasToken($this->request->token))
				throw new Exception_NotFound();
			$this->setView('convention/user/resetpassword');
			$this->view->email = $this->request->email;
			$this->view->token = $this->request->token;
		} catch (Exception_NotFound $e) {
			$this->session->error('לא נמצא משתמש בעל סיסמה עם כתובת זו, או שהאיפוס כבר התבצע');
			$this->setView('convention/user/resetsent');
		}
	}
	
	public function action_completereset() {
		$this->setView('convention/user/resetpassword');
		if (strlen($this->request->password1) < 6 || strlen($this->request->password2) < 6) {
			$this->session->error('הסיסמה חייבת להכיל לפחות 6 סימנים');
			return;
		}
		if ($this->request->password1 != $this->request->password2) {
			$this->session->error('שתי הסיסמאות לא זהות');
			return;
		}
		
		try {
			$user = Model_User::byEmail($this->request->email);
			if (!$user->isPasswordAuthenticated())
				throw new Exception_NotFound();
			if (!$user->hasToken($this->request->token))
				throw new Exception_NotFound();
			$user->revokeToken($this->request->token);
			$user->setPassword($this->request->password1);
			$this->setView('convention/user/password-reset-complete');
		} catch (Exception_NotFound $e) {
			$this->session->error('לא נמצא משתמש בעל סיסמה עם כתובת זו, או שהאיפוס כבר התבצע');
			$this->setView('convention/user/resetsent');
		}
	}
	
	public function action_volunteer() {
		if (!$this->user) {
			$this->setView("convention/login-required");
			return;
		}
		$this->setView('convention/user/volunteer');
		$this->view->page_title = 'התנדב לכנס';
		switch ($this->request->action) {
			case 'update':
				$this->update_volunteer_records();
				$this->view->thanks = true;
		}
	}
	
	private function update_volunteer_records() {
		$user = $this->user;
		$user->volunteerForHosting($this->convention, $this->request->want_host ? true : false);
		$user->volunteer($this->convention, $this->request->want_volunteer ? true : false);
	}
	
}
