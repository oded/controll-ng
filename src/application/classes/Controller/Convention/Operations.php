<?php

class Controller_Convention_Operations extends Base_Controller {
	
	public function action_index() {
		$this->jsonResponse(null);
	}
	
	public function action_member() {
		if (!$this->request->email) {
			$this->jsonResponse('OK');
			return;
		}
			
		try {
			$user = Model_User::byEmail($this->request->email);
			$org = $this->convention->organizations->find();
			if (!$user->isMember($org)) {
				$user->addMember($org, '000000');
			}
			$member = $user->memberships->find();
			$this->jsonResponse([$user->fullname, $member ? $member->member_id : 'unknown']);
		} catch (Exception_NotFound $e) {
			$this->jsonResponse('Invalid');
		}
	}
	
}