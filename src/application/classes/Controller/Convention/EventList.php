<?php

class Controller_Convention_EventList extends Base_Template_Controller {

	public function action_index() {
		if (@$_GET['test'])
			$this->setView('convention/eventlist2');
		Model_Registration::garbageCollect(); // do this first because it affects 'available_tickets'
		
		// get events and locations
		$locations = $this->convention->locations->order_by('order')->find_all();
		$events = $this->convention->approved_events->find_all();

		// find day start/end for each day, according to con events
		$eventdays = [];
		foreach ($events as $ev) {
			$day = date("d/m",$ev->start);
			if (!isset($eventdays[$day])) {
				$eventdays[$day] = (object)[ 'start' => $ev->start, 'end' => $ev->end ];
				continue;
			}
			$eventdays[$day]->start = min($eventdays[$day]->start, $ev->start); // shouldn't be needed because event list is sorted by start
			$eventdays[$day]->end = max($eventdays[$day]->end, $ev->end);
		}
		
		// create DayLocation objects that handle location for a day
		foreach ($eventdays as $date => &$day) {
			$day->locations = [];
			foreach ($locations as $loc) {
				$loc = new Model_DayLocation($loc, $day->start, $day->end);
				if ($loc->count)
					$day->locations[] = $loc;
			}
		}
		
		$this->view->datelist = array_values($eventdays);
		$this->view->event_types = $this->convention->event_types->find_all();
		$this->view->page_title = 'תוכנייה';
		$this->template->page_type = 'con-plan-page';
		$this->view->header = $this->getWordpressPage('כותרת רשימת ארועים');
	}

}
