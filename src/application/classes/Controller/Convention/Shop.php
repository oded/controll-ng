<?php

class Controller_Convention_Shop extends Base_Template_Controller {
	
	public function action_index()
	{
		Model_Merchandise_Transaction::garbageCollect();
		
		switch ($this->request->action) {
			case 'order':
				return $this->action_order();
			case "checkout":
				return (new Controller_Convention_Event($this->request, $this->response))->action_checkout();
		}
		
		$item = $this->request->param('item');
		if (is_null($item)) {
			$this->session->error('מוצר לא קיים');
			return $this->action_list();
		}
		
		try {
			$this->view->item = Model_Merchandise::byKey($item);
			$this->view->page_title = $this->view->item->title;
		} catch (Exception_NotFound $e) {
			Log::debug("Missing merchandise '$item', redirecting to store list");
			$this->session->error('מוצר לא קיים');
			$this->redirect('/shop/list');
		}
	}

	public function action_list() {
		switch ($this->request->action) {
			case 'order':
				return $this->action_order();
			case "checkout":
				return (new Controller_Convention_Event($this->request, $this->response))->action_checkout();
		}
		$this->setView('convention/shop/list');
		$this->view->page_title = 'רשימת מוצרים';
		$this->view->items = Model_Merchandise::forConvention($this->convention);
	}
	
	public function action_order() {
		$item = $this->request->param('item');
		if (is_null($item)) {
			$this->session->error('מוצר לא קיים');
			$this->redirect('/shop');
		}
		
		if (! $this->user) {
			$this->session->error("רק משתמשים רשומים יכולים להזמין מוצרים");
			$this->redirect('/shop/' . $item->key);
		}
		
		$item = Model_Merchandise::byKey($item);
		if (! $item->loaded()) {
			$this->session->error('מוצר לא קיים');
			$this->redirect('/shop');
		}
		
		// locate the correct SKU
		$features = [];
		foreach ($item->features->find_all() as $feature) {
			$field = "feature-" . $feature->id;
			$value = $this->request->$field;
			$features[] = [ $feature , $value ];
		}
		try {
			$sku = Model_Merchandise_Sku::byFeatures($features);
			if (!$sku->loaded()) {
				$this->session->error('לא קיימים פריטים מהסוג המבוקש');
				$this->redirect('/shop/' . $item->key);
			}
			Model_Merchandise_Transaction::generate($sku, $this->user, $this->request->amount);
			$this->redirect('/shop/' . $item->key);
		} catch (Exception_NotFound $e) {
			$this->session->error("שגיאה באיתור הפריט");
			Log::error("Failed to load SKU: " . $e->getMessage());
			$this->redirect('/shop/' . $item->key);
		}
		
	}
	
	public function action_cancel() {
		$item = $this->request->param('item');
		if (is_null($item)) {
			$this->session->error('מוצר לא קיים');
			$this->redirect('/shop');
		}
		
		$item = Model_Merchandise::byKey($item);
		if (! $item->loaded()) {
			$this->session->error('מוצר לא קיים');
			$this->redirect('/shop');
		}
		
		if (! $this->user) {
			$this->session->error("רק משתמשים רשומים יכולים להזמין מוצרים");
			$this->redirect('/shop/' . $item->key);
		}
		
		$order = new Model_Merchandise_Transaction($this->request->id);
		if (! $order->loaded()) {
			$this->session->error("הזמנה לא נמצאה");
			$this->redirect('/shop/' . $item->key);
		}
		
		if ($order->status >= Model_Merchandise_Transaction::STATUS_FULFILLED) {
			$this->session->error("לא ניתן לבטל הזמנות שהושלמו");
			$this->redirect('/shop/' . $item->key);
		}
		
		$order->cancel('user cancelled');
		$this->redirect('/shop/' . $item->key);
	}
	
}
