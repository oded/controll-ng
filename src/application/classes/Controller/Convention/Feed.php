<?php

class Controller_Convention_Feed extends Base_Controller {

	/**
	 * @var Base_Wordpress_Controller
	 */
	private $wp;
	
	public function __construct($request, $response) {
		parent::__construct($request, $response);
		$this->wp = new Base_Wordpress_Controller($request, $response);
	}
	
	public function action_articles() {
		$this->setView('convention/feed');
		$this->view->articles = array_slice($this->wp->getWordpressPosts(),0,15);		
	}
	
	public function after() {
		parent::after();
		$this->response->headers('Content-Type', 'application/rss+xml; charset=utf-8');
		$this->response->body($this->view);
	}
	
}