<?php

class Controller_Convention_Cashier extends Base_Template_Controller {
	
	public function before() {
		parent::before();
	
		if ($this->request->action() == 'authenticate')
			return;
	
		if (!$this->user or !$this->user->hasRole($this->convention,'קופאי'))
			$this->redirect('/cashier/authenticate?return_url=' . urlencode($this->request->uri()));
	}
	
	public function action_index() {
	}
	
	public function action_authenticate() {
		$this->setView('convention/admin/authenticate');
	}

	public function action_getregistrations() {
		$out = [];
		foreach (Model_Registration::getFulfilled($this->convention) as $reg) {
			$out[] = (object)$reg->as_array();
		}
		$this->jsonResponse($out);
	}
	
	public function action_getevents() {
		$eventoccurrences = $this->convention->approved_events->
			with('location')->with('users')->with('event:user')->with('event:type:type')
					->order_by('event_occurrence.start')->
			get_all_objects();
		$tags_heb = $tags_eng = [];
		foreach ($eventoccurrences as $ev) {
			foreach ($ev->event->tag_list as $tag) {
				if ($tag->title[0] <= 'z')
					$tags_eng[$tag->title] = $tag->key;
				else
					$tags_heb[$tag->title] = $tag->key;
			}
		}
		ksort($tags_eng); ksort($tags_heb);
		$this->jsonResponse((object)[
				'events' => $eventoccurrences,
				'tag_list' => $tags_heb + $tags_eng,
				'event_types' => $this->convention->event_types->get_all_objects(),
				'skus' => $this->convention->merchandise->get_all_objects(),
		]);
	}
	
	private function really_convert_to_objects($orms) {
		$out = [];
		foreach ($orms as $orm) {
			$obj = (object)$orm;
			$obj->event = (object)$orm->event;
			$out[] = $obj;
		}
		return $out;
	}
	
	public function action_getusers() {
		$this->jsonResponse((new Model_User())->get_all_objects());
	}
	
	public function action_usersearch_name() {
		$out = [];
		foreach (DB::select('id', [DB::Expr('CONCAT(first_name," ",last_name)'), 'fullname'])
				->from('users')->having('fullname','like','%'.$this->request->term.'%')->execute() as $row) {
			$out[] = (object)[ 'label' => $row['fullname'], 'value' => (object)(new Model_User($row['id']))->as_array() ];
		}
		$this->jsonResponse($out);
	}
	
	public function action_usersearch_email() {
		$out = [];
		foreach ((new Model_User())->where('email','like','%' . $this->request->term . '%')->find_all() as $user) {
			$out[] = (object)[ 'label' => $user->email, 'value' => (object)$user->as_array() ];
		}
		$this->jsonResponse($out);
	}
	
	public function action_usersearch_phone() {
		$out = [];
		foreach ((new Model_User())->where('phone','like','%' . $this->request->term . '%')->find_all() as $user) {
			$out[] = (object)[ 'label' => $user->phone, 'value' => (object)$user->as_array() ];
		}
		$this->jsonResponse($out);
	}
	
	public function action_usersearch_assocnum() {
		$out = [];
		foreach ((new Model_Organization_Member())
				->where('member_id', 'like', '%' . $this->request->term . '%')
				->where('organization_id','=',$this->convention->organizations->find()->id)
				->with('user')->find_all() as $member) {
			$out[] = (object)[ 'label' => $member->member_id, 'value' => $member->user->as_array() ];
		}
		$this->jsonResponse($out);
	}
	
	public function action_createuser() {
		$name = $this->request->name;
		$email = $this->request->email;
		$phone = $this->request->phone;
		$assocnum = $this->request->assocnum;
		$org = new Model_Organization($this->request->organization);
		if (!$org->loaded())
			$org = null;
		
		try {
			if (!$email) throw new Exception('חובה לספק דואר אלקטרוני');
			
			try {
				$user = Model_User::byEmail($email);
				if ($phone && !$user->phone) {
					// update phone if given, because users tend to not put it in the internet registration
					$user->phone = $phone;
					$user->save();
				}
				if ($assocnum && $org && !$user->isMember($org)) {
					$user->addMember($org, $assocnum);
					$user->save();
				}
			} catch (Exception_NotFound $e) {
				// need to create a new user
				list($first, $last) = @preg_split('/\s+/', $name, 2);
				$user = Model_User::generate($first, $last, $email, null, $phone ? $phone : null);
			}
			
			$this->jsonResponse((object)[
					'status' => true,
					'user' => $user->as_array(),
			]);
		} catch (Exception $e) {
			$this->jsonResponse((object)[ 'status' => false, 'message' => $e->getMessage() ]);
		}
	}
	
	public function action_addregistration() {
		$user = new Model_User($this->request->user);
		$event = new Model_Event_Occurrence($this->request->event);
		$amount = $this->request->amount;
		$price = $this->request->price;
		$coupon = $this->request->coupon;
		$creditcard = $this->request->creditcard;
		
		try {
			if (!$user->loaded()) throw new Exception("משתמש לא מזוהה");
			if (!$event->loaded()) throw new Exception("ארוע לא מזוהה");
			$reg = $event->register($user, $amount);
// 			if ($coupon) 
// 				$reg->discount_code = $coupon;
			// check if we need to consume some coupons
// 			list($price, $coupons, $discount) = Model_Registration::calculateCouponDiscount($user, $price);
// 			if ($coupons) {
// 				$reg->discount_code = ($reg->discount_code ? ',' : '') . $coupons;
// 			}
			$reg->price = $price;
			$reg->status = Model_Registration::STATUS_FULFILLED;
			$reg->transaction_id = 'cashier:' . $this->user->id . ($creditcard ? ' credit':'');
			$reg->save();
			
			$this->jsonResponse((object)[
					'status' => true,
					'registration' => $reg->as_array(),
			]);
		} catch (Exception $e) {
			$this->jsonResponse((object)[ 'status' => false, 'message' => $e->getMessage() ]);
		}
	}
	
	public function action_addmerchandise() {
		$user = new Model_User($this->request->user);
		$sku = new Model_Merchandise_Sku($this->request->sku);
		$price = $this->request->price;
		$coupon = $this->request->coupon;
		$creditcard = $this->request->creditcard;
		
		try {
			if (!$user->loaded()) throw new Exception("משתמש לא מזוהה");
			if (!$sku->loaded()) throw new Exception("SKU לא מזוהה");
			if (!$price or $price < 0) throw new Exception('מחיר לא תקין');
			$trx = $sku->sell($user, 1);
			if ($coupon) 
				$trx->discount_code = $coupon;
			// check if we need to consume some coupons
			list($price, $coupons, $discount) = $trx->calculateCouponDiscount($user, $price);
			if ($coupons) {
				$trx->discount_code = ($trx->discount_code ? ',' : '') . $coupons;
			}
			$trx->price = $price;
			$trx->status = Model_Registration::STATUS_FULFILLED;
			$trx->transaction_id = 'cashier:' . $this->user->id . ($creditcard ? ' credit':'');
			$trx->save();
			
			$this->jsonResponse((object)[
					'status' => true,
					'transaction' => $trx->as_array(),
			]);
		} catch (Exception $e) {
			$this->jsonResponse((object)[ 'status' => false, 'message' => $e->getMessage() ]);
		}
	}
	
	public function action_delregistration() {
		try {
			$reg = new Model_Registration($this->request->reg);
			if (!$reg->loaded())
				throw new Exception('רישום לא קיים');
			$reg->cancel('cashier-refund:' . $this->user->id);
			$this->jsonResponse((object)[ 'status' => true ]);
		} catch (Exception $e) {
			$this->jsonResponse((object)[ 'status' => false, 'message' => $e->getMessage() ]);
		}
	}

	public function action_print() {
		$this->setView('convention/registrations-list');
		$this->template = 'simple';
		$this->buildTemplate();
		$this->view->user = new Model_User($this->request->user);
		$this->view->user->inConvention($this->convention);
		$this->view->print = true;
		$this->template->canonical = "";
	}

}
