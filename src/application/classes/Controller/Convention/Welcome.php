<?php

class Controller_Convention_Welcome extends Base_Template_Controller {
	
	public function action_index()
	{
		$this->template->content_title = "גיבורים: אירוע משחקי התפקידים והלוח הגדול | 16-17 באפריל, תל אביב";
		$this->template->content_description = "כנס גיבורים הוא אירוע משחקי התפקידים הגדול בישראל. 16-17 באפריל, בבית דני, בדרום-מזרח תל-אביב. כנס בן יומיים שמיועד לכל חובבי משחקי התפקידים.";
		$this->view->title = "";
	
		$this->view->articles = [];
		$first = true;
		$all_posts = $this->getWordpressPosts();
		$all_pages = $this->getWordpressPages();
		$render_list = [];
		
		$max_items = 11;
		
		foreach ($all_pages as $article) {
			if ($article->menu_order < 0) continue;
			while (is_object($render_list[$article->menu_order])) { // duplicate menu order? that is not good
				$article->menu_order++;
			}
			$render_list[$article->menu_order] = $article;
		}

		// get last index
		end($render_list);
		$last = key($render_list) + 1; // convert from #last to #count
		// make sure we have enough content
		$last = max($last, min($last + count($all_posts),$max_items));
		// render all articles
		for ($i = 0; $i < $last; $i++) {
			if (is_object($render_list[$i])) {
				// set up page colors
				$render_list[$i]->page_type = $render_list[$i]->ID % 2 + 1;
			} else { // if we have a hole in the page order, fill with a recent post
				$render_list[$i] = array_shift($all_posts);
			}

			if (!is_object($render_list[$i])) continue;
			// render
			$view = Twig::factory('convention/articlebox');
			$view->convention = $this->convention;
			$view->post = $render_list[$i];
			$view->latest = $first;
			// replace object with rendered view
			$this->view->articles[] = $view;
			$first = false; // reset "first post"
		}
	}
	
}
