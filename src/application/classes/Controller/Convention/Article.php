<?php

class Controller_Convention_Article extends Base_Template_Controller {

	public function after() {
		$this->view->canonical = $this->template->canonical;
		parent::after();
	}

	public function action_listposts() {
		$this->setView('convention/magazine');
		$this->template->page_type = 'magazine';
		$this->template->content_title = "מגזין גיבורים: חדשות, עדכונים וכתבות מעולם משחקי התפקידים";
		$this->template->content_description = "המגזין האינטרנטי של כנס גיבורים, כנס משחקי התפקידים הגדול של ישראל, מציג בפניכם חדשות וכתבות מעולם משחקי התפקידים בארץ ובעולם.";
		$this->view->title = "";
		
		$this->view->articles = [];
		$first = true;
		
		// render all articles
		foreach ($this->getWordpressPosts() as $post) {
			$view = Twig::factory('convention/articlebox');
			$view->convention = $this->convention;
			$view->post = $post;
			$view->latest = $first;
			// replace object with rendered view
			$this->view->articles[] = $view;
			$first = false; // reset "first post"
		}
	}
	
	public function action_search() {
		$this->setView('convention/magazine');
		$this->template->page_type = 'magazine';
		$this->template->content_title = "מגזין גיבורים: חדשות, עדכונים וכתבות מעולם משחקי התפקידים";
		$this->template->content_description = "המגזין האינטרנטי של כנס גיבורים, כנס משחקי התפקידים הגדול של ישראל, מציג בפניכם חדשות וכתבות מעולם משחקי התפקידים בארץ ובעולם.";

		$query = new WP_Query();
		$querystring = 's=' . urlencode($this->request->query('s'));
		$this->view->articles = [];
		
		// render all articles
		foreach ($this->normalize_posts($query->query($querystring)) as $post) {
			$view = Twig::factory('convention/articlebox');
			$view->convention = $this->convention;
			$view->post = $post;
			$view->latest = false;
			// replace object with rendered view
			$this->view->articles[] = $view;
		}
	}

	public function action_viewpost() {
		$this->template->page_type = 'magazine';
		$this->view->post = $this->getWordpressPost($this->request->param('slug'));
		if (is_null($this->view->post)) {
			// invalid slug or user is not allowed to access content
			$this->setView('convention/missing');
			return;
		}

		$title = $this->view->post->custom_fields['seo_title'];
		$desc = $this->view->post->custom_fields['seo_desc'];
		$this->view->page_title = $title ? current($title) : $this->view->post->post_title;
		$this->view->page_description = $desc ? current($desc) : $this->view->post->post_title;
	}
	
	public function action_viewpage() {
		$this->setView('convention/page');
		$this->view->post = $this->getWordpressPage($this->request->param('slug'));
		if (is_null($this->view->post)) {
			// invalid slug or user is not allowed to access content
			$this->setView('convention/missing');
			return;
		}
		
		$title = $this->view->post->custom_fields['seo_title'];
		$desc = $this->view->post->custom_fields['seo_desc'];
		$this->view->post->post_type = $this->view->post->ID % 2 + 1;
		$this->view->page_title = $title ? current($title) : $this->view->post->post_title;
		$this->view->page_description = $desc ? current($desc) : $this->view->post->post_title;
	}
	
}