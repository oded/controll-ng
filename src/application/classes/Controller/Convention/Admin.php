<?php

class Controller_Convention_Admin extends Base_Template_Controller {
	
	public function before() {
		parent::before();
		
		if ($this->request->action() == 'authenticate')
			return;
		
		if (!$this->user or !$this->user->isAdmin())
			$this->redirect('/admin/authenticate?return_url=' . urlencode(URL::site($this->request->uri(), true)));
	}
	
	public function action_index() {
	}
	
	public function action_authenticate() {
		$this->setView('convention/admin/authenticate');
	}
	
	public function action_users() {
		$this->setView('convention/admin/users');
		$q = (new Model_User());
		if ($this->request->sort)
			$q = $q->order_by($this->request->sort);
		$this->view->users = $q->find_all();	
	} 
	
	public function action_give_host_coupon() {
		$user = new Model_User($this->request->user);
		Model_Coupon::giveEventHostCoupon($user);
		$return_to = 'users';
		if ($this->request->render == 'false') {
			$this->response->body($user->totalCoupons());
			$this->auto_render = false;
			return;
		}	
		if ($this->request->return_to)
			$return_to = $this->request->return_to; 
		$this->redirectToAction($return_to);
	}
	
	public function action_grant() {
		$user = new Model_User($this->request->user);
		$user->grantRole($this->convention, Model_Role::byKey($this->request->role));
		$this->redirectToAction('users', '#' . $this->request->return_to);
	}
	
	public function action_revoke() {
		$user = new Model_User($this->request->user);
		$user->revokeRole($this->convention, Model_Role::byKey($this->request->role));
		$this->redirectToAction('users', '#' . $this->request->return_to);
	}
	
	public function action_events() {
		$this->setView('convention/admin/events');
		$q = $this->convention->occurrences->with('event:user')->with('staff_contact');
		$sort_dir = $this->request->sortrev ? 'desc' : 'asc';
		switch ($this->request->sort) {
			case 'owner':
				$q = $q->order_by('event:user:first_name', $sort_dir)->order_by('event:user:last_name', $sort_dir);
				break;
			case 'status':
				$q = $q->order_by('status', $sort_dir)->order_by('start', $sort_dir);
				break;
			case 'start':
				$q = $q->order_by('start', $sort_dir)->order_by('status', $sort_dir);
				break;
			case 'staff_contact':
				$q = $q->order_by('staff_contact:first_name', $sort_dir)->order_by('staff_contact:last_name', $sort_dir);
				break;
			case 'regstatus':
				$q = $q->order_by('staff_contact:first_name', $sort_dir)->order_by('staff_contact:last_name', $sort_dir);
				break;
			case null:
				break;
			default:
				$q = $q->order_by('event:' . $this->request->sort, $sort_dir);
		}
		$this->view->sortby = $this->request->sort;
		$this->view->sortrev = $this->request->sortrev;
		$this->view->events = [];
		foreach ($q->find_all() as $oc)
			$this->view->events[] = $oc->event;
	}
	
	public function action_edit_event() {
		Log::debug("request: " .print_r($this->request->post(),true) );
		switch ($this->request->action) {
			case 'update':
				if (!$this->request->event_occurrence) {
					$this->redirectToAction('events');
					return;
				}
				$occurrence = $this->convention->eventOccurrenceById($this->request->event_occurrence);
				$event = $occurrence->event;

				$occurrence->values($this->request->post());
				$event->values($this->request->post());
				// special handling for some fields
				$event->rule_system = Model_Rule_System::forName(trim($this->request->event_rule_system));
				$occurrence->location = Model_Location::getByTitleOrCreateForConvention($this->convention, $this->request->location);
				$occurrence->prevent_conflicts = $this->request->prevent_conflicts ? true : false; // checkbox don't send values unless thy're checked
				$event->kid_friendly = $this->request->kid_friendly ? true : false;
				$event->newbie_friendly = $this->request->newbie_friendly ? true : false;
				$event->type = $this->convention->eventType($this->request->type);
				$event->registration_required = isset($this->request->registration_required);
				// length is measured in the edit box in minutes, but stored in seconds
				$occurrence->length = $this->request->length * 60;
				$occurrence->updateHosts($this->request->hosts);
				$occurrence->staff_contact = new Model_User($this->request->staff_contact);
				$occurrence->save();
				$event->save();
				
				$conflicts = count($this->convention->checkConflicts($occurrence));
				if ($conflicts > 0)
					$this->session->error("אזהרה: הארוע מתנגש עם ".$conflicts." ארועים נוספים באותו מיקום");
				if ($this->request->return_to)
					$this->redirect($this->request->return_to);
				else
					$this->redirectToAction('events');
				return;
			case 'create':
				$key = Model_Event::createKey($this->request->title);
				$rule_system = Model_Rule_System::forName($this->request->event_rule_system);
				$regreq = isset($this->request->registration_required);
				while (true) {
					try {
						$event = Model_Event::generate($this->convention, $this->request->hosts,
							$this->convention->eventType($this->request->type), $this->request->title,
							$this->request->description, $rule_system, $this->request->tags, $regreq, $key);
						break;
					} catch (Database_Exception $e) {
						if (!strstr($e->getMessage(), "Duplicate entry"))
							throw $e;
						$key = Model_Event::incrementKey($key);
					}
				}
				$occur = Model_Event_Occurrence::generate($event, 
					$this->request->start, $this->request->start + ($this->request->length * 60),
					$this->request->max_attendees, $this->request->notes_to_attendees, $this->request->notes_to_staff,
					$this->request->logistical_requirments, $this->request->min_attendees);
				$occur->staff_contact = new Model_User($this->request->staff_contact);
				$occur->save();
				$event->values($this->request->post());
				// special handling for some fields
				$occur->location = Model_Location::getByTitleOrCreateForConvention($this->convention, $this->request->location);
				$event->registration_required = $this->request->registration_required ? true : false; // checkbox don't send values unless thy're checked
				$occur->prevent_conflicts = $this->request->prevent_conflicts ? true : false; // checkbox don't send values unless thy're checked
				$event->type = $this->convention->eventType($this->request->type);
				$event->save();
				
				$conflicts = count($this->convention->checkConflicts($occur));
				if ($conflicts > 0)
					$this->session->error("אזהרה: הארוע מתנגש עם ".$conflicts." ארועים נוספים באותו מיקום");
				
				$this->redirectToAction('events');
				return;
			default:
				if (!$this->request->event_occurrence) {
					$this->redirectToAction('events');
					return;
				}
				$this->renderEditEvent($this->convention->eventOccurrenceById($this->request->event_occurrence), 'update');
				return;
		}
	}
	
	public function action_delete_event() {
		if (!$this->request->event_occurrence) {
			$this->redirectToAction('events');
			return;
		}
		$oc = $this->convention->eventOccurrenceById($this->request->event_occurrence);
		$ev = $oc->event;
		$oc->delete();
		if ($ev->occurrences->count_all() < 1) $ev->delete();
		$this->redirectToAction('events');
	}
	
	public function action_add_user() {
		try {
			$name = preg_split("/\s+/", $this->request->fullname);
			$firstname = array_shift($name);
			$lastname = join(" ", $name);
			Model_User::generate($firstname, $lastname, $this->request->email, null, $this->request->phone);
		} catch (Exception_Duplicate $e) {
			$this->session->error('המשתמש כבר קיים, בדוק את הפרטים: ' . $e->getFields());
		} catch (ORM_Validation_Exception $e) {
			$this->session->error("חובה לספק פרטי משתמש חוקיים (שם פרטי, משפחה ודואר אלקטרוני)");
		}
		$this->redirectToAction('users');
	}
	
	public function action_export() {
		$this->auto_render = false;
		require_once Kohana::find_file('vendor', 'phpexcel/Classes/PHPExcel');
		$rows = [ [ 
				'id', 'title', 'submitter', 'type', 'description', 'rules', 'tags', 'price', 'start', 'length (h)',
				'min attendees', 'max attendees', 'status', 'location', 'logistical req.', 'notes to attendees',
				'notes to stagff', 'scheduling req.', 'last update', 'registered'
				] ];
		foreach ($this->convention->occurrences->find_all() as $oc) {
			$rows[] = [
					$oc->event->id,
					stripslashes($oc->event->title),
					$oc->event->user->name,
					$oc->event->type->title,
					stripslashes($oc->event->description),
					stripslashes($oc->event->rule_system ? $oc->event->rule_system->title : ''),
					stripslashes($oc->event->tags),
					$oc->event->price,
					PHPExcel_Shared_Date::FormattedPHPToExcel( date('Y',$oc->start), date('m',$oc->start), date('d',$oc->start),
												   date('H',$oc->start), date('i',$oc->start), date('s',$oc->start)),
					($oc->length / 3600),
					$oc->min_attendees,
					$oc->max_attendees,
					$oc->statustext,
					stripslashes($oc->location ? $oc->location->title : ''),
					$oc->logistical_requirments,
					$oc->notes_to_attendees,
					$oc->notes_to_staff,
					$oc->scheduleing_constraints,
					$oc->event->update_time,
					$oc->registered_tickets
			];
		}
		
		$doc = new PHPExcel();
		$doc->setActiveSheetIndex(0);
		$doc->getActiveSheet()->fromArray($rows,'', 'A1', false);
		for ($i = 0; $i < count($rows); $i++)
			$doc->getActiveSheet()->getStyle('I' . ($i + 1))
				->getNumberFormat()
				->setFormatCode('yyyy-mm-dd HH:mm');
				
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="bigor-events.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
		$objWriter->save('php://output');
	}
	
	public function action_add_event() {
		if ($this->request->user == 'new') { // create a new user account (without auth)
			try { 
				$name = preg_split("/\s+/", $this->request->fullname);
				$firstname = array_shift($name);
				$lastname = join(" ", $name);
				$user = Model_User::generate($firstname, $lastname, $this->request->email, null, $this->request->phone);
				//and start an event add for that user
				$this->renderEditEvent($user, 'create');
			} catch (Exception_Duplicate $e) {
				$this->session->error('המשתמש כבר קיים, בדוק את הפרטים: ' . $e->getFields());
				$this->redirectToAction('add_event');
			} catch (ORM_Validation_Exception $e) {
				$this->session->error("חובה לספק פרטי משתמש חוקיים (שם פרטי, משפחה ודואר אלקטרוני)");
				$this->redirectToAction('add_event');
			}
		} elseif (is_numeric($this->request->user)) { // add an event owned by that user
			$this->renderEditEvent(new Model_User((int)$this->request->user), 'create');
		} else { // let the admin select a user to impersonate
			$this->setView('convention/admin/add-event-select-user');
			$this->view->users = (new Model_User())->find_all();
		}
	}
	
	public function renderEditEvent($target, $action) {
		$this->setView('convention/admin/edit-event');
		if ($target instanceof Model_User) {
			$this->view->event_user = $target;
		} elseif ($target instanceof Model_Event_Occurrence) {
			$this->view->occurrence = $target;
		} else {
			throw new Exception('Invalid target type ' . get_class($target) . ' for EditEvent');
		}
		$this->view->locationlist = $this->convention->locations->find_all();
		$this->view->action = $action;
		$this->view->system_users = (new Model_User())->order_by('first_name')->order_by('last_name')->find_all();
		$this->view->rule_systems = Model_Rule_System::all();
		$this->view->staffers = Model_User::byConventionRole($this->convention, Model_Role::staff());
		$this->view->referrer = $this->request->referrer();
	}
	
	public function action_registrations() {
		$this->setView('convention/admin/registrations');
		
		if ($this->request->cancel) {
			$reg = new Model_Registration($this->request->cancel);
			if ($reg->loaded()) {
				$reg->cancel('admin cancel');
				$this->redirectToAction('registrations');
			}
		} elseif ($this->request->approve) {
			$reg = new Model_Registration($this->request->approve);
			if ($reg->loaded()) {
				$reg->complete($this->request->transaction ? $this->request->transaction : 'admin manual approve');
				$this->redirectToAction('registrations');
			}
		}
		
		$dir = $this->request->desc ? 'DESC' : 'ASC';
		$q = $this->convention->registrations;
			switch ($this->request->sort) {
				case 'event': $q = $q->order_by('event:title', $dir); break;
				case 'name': $q = $q->order_by('user:first_name', $dir)->order_by('user:last_name', $dir); break;
				case 'date': $q = $q->order_by('registered', $dir); break;
				case 'status': $q = $q->order_by('status', $dir); break;
				default:
					$q = $q->order_by('id');
			}
		$this->view->registrations =  $q->find_all();
		$this->view->sorted = $this->request->sort . ($dir == 'DESC' ? '-desc' : '');
		$this->view->coupon_sum_used = Model_Coupon::getSumUsed();
	}

	public function action_merchandise() {
		$this->setView('convention/admin/merchandise');
		
		if ($this->request->cancel) {
			$reg = new Model_Merchandise_Transaction($this->request->cancel);
			if ($reg->loaded()) {
				$reg->cancel('admin cancel');
				$this->redirectToAction('merchandise');
			}
		}
		
		$this->view->transactions = $this->convention->merchandise_orders->find_all();
	}

	public function action_cancel_phone_pending() {
		$user = new Model_User($this->request->userid);
		if ($user->loaded()) {
			$c = Model_Registration::cancelPhonePending($user);
			$this->session->error("$c הזמנות בוטלו");		
		}
	}
	
	public function action_approve_phone_pending() {
		$user = new Model_User($this->request->userid);
		if ($user->loaded()) {
			$c = Model_Registration::approvePhonePending($user);
			$this->session->error("$c הזמנות אושרו");		
		}
	}
	
	public function action_refund() {
		// get the transaction to refund
		if ($this->request->regid)
			$reg = new Model_Registration($this->request->regid);
		elseif ($this->request->trxid)
			$reg = new Model_Merchandise_Transaction($this->request->trxid);
		else {
			$this->session->error('missing id');
			$this->redirect($this->request->referrer());
		}
		
		if (!$reg->loaded()) // sanity first!
			throw new Exception("Invalid transaction ID");
		
		if ($reg->status != Model_Registration::STATUS_FULFILLED)
			throw new Exception("Invalid transaction status");
		
		if ($this->request->approved) { // admin approved the refund
			$refundTickets = $this->request->post('refund-amount-' . $reg->id);
			if (!is_numeric($refundTickets))
				$refundTickets = $reg->amount;
			$reg->refund($refundTickets);
			$this->redirectToAction('registrations');
		}
		
		$this->setView('convention/admin/refund');
		$this->view->transaction_id = $reg->transaction_id;
		$this->view->customer = $reg->user;
		$this->view->registrations = [ $reg ];
		$this->view->origregid = $reg->id; 
	}
	
	public function action_log() {
		$this->setView('convention/admin/log');
		$this->view->logs = DB::select('*')->from('logs')->order_by('id', 'DESC')->limit(200)->execute();
	}

	public function action_test() {
		$this->setView('convention/admin/merchandise');
/* * /
		$shirts = [ 
				'חולצת טי-שירט רגילה' => [ 
						'price' => 20,
						'description' => 'החולצה הרשמי של כנס ביגור',
						'features' => [
							'צבע' => [ 'שחור','סגול','כחול כהה','אדום','ירוק בהיר','לבן' ],
							'גודל' => [ 'XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL' ],
						],
				],
				'חולצה צמודה (גזרת נשים)' => [
						'price' => 20,
						'description' => 'החולצה הרשמי של כנס ביגור',
						'features' => [
							'צבע' => [ 'שחור','סגול','כחול כהה','אדום','ירוק בהיר','לבן' ],
							'גודל' => [ 'XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL' ],
						],
				],
				'חולצת דרייפיט (גזרת טי-שירט)' => [
						'price' => 25,
						'description' => 'החולצה הרשמי של כנס ביגור',
						'features' => [
							'צבע' => [ 'שחור','סגול','כחול כהה','אדום','ירוק בהיר','לבן' ],
							'גודל' => [ 'XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL' ],
						],
				], 
				'סווטשירט עם כיס קדמי' => [
						'price' => 40,
						'description' => 'החולצה הרשמי של כנס ביגור',
						'features' => [
							'צבע' => [ 'שחור','סגול','כחול כהה','אדום','לבן' ],
							'גודל' => [ 'XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL' ],
						],
				],
		];
		foreach ($shirts as $title => $desc) {
			$shirt = Model_Merchandise::generate($this->convention, $title, $desc['description'], null, $desc['price']);
			// generate SKUs
			$featkeys = array_keys($desc['features']);
			$curnum = array_fill(0, count($featkeys), 0);
			$last_elem_size = count($desc['features'][end($featkeys)]);
			$skus = [];
			while(end($curnum) < $last_elem_size) {
				$sku = [];
				for ($feati = 0; $feati < count($featkeys); $feati++) {
					$sku[] = $desc['features'][$featkeys[$feati]][$curnum[$feati]];
				}
				$skus[] = $sku;
				
				$curnum[0]++; // tick
				// cascade overflows
				for ($i = 0; $i < count($curnum)-1 && $curnum[$i] >= count($desc['features'][$featkeys[$i]]); $i++) { 
					$curnum[$i+1]++;
					for ($j = 0; $j <= $i; $j++) $curnum[$j] = 0;
				}
			}
			
			// generate features
			$features = array_map(function($featurename) use ($shirt) { 
				return Model_Merchandise_Feature::generate($shirt, $featurename);
			}, $featkeys);
			
			// build SKUs
			foreach ($skus as $sku) {
				$skuname = preg_replace('/[^א-תa-zA-Z]+/','_', 'bigor 2015 shirt ' . join(' ', $sku));
				$sku_obj = Model_Merchandise_Sku::generate($shirt, $skuname);
				for ($i = 0; $i < count($sku); $i++)
					Model_Merchandise_Sku_Property::generate($sku_obj, $features[$i], $sku[$i]);
			}
		}
		/* */
		
		$this->view->items = Model_Merchandise::getAll();
	}
	
}
