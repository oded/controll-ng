<?php defined('SYSPATH') or die('No direct script access.');

define("AX_EMAIL_ID","http://axschema.org/contact/email");
define("AX_NAME_ID", "http://axschema.org/namePerson");
define("AX_NAME_FIRST_ID", "http://axschema.org/namePerson/first");
define("AX_NAME_LAST_ID", "http://axschema.org/namePerson/last");
define("OAUTH_REQUEST_TOKEN", 'oauth_request_token');
define("OAUTH_ACCESS_TOKEN", 'oauth_access_token');

// userland implementation of password_hash/password_verify because we aren't running on php 5.5 yet
require_once(APPPATH.'vendor/password_compat/lib/password.php');

/**
 * This is a Kohana3 ported version of the php-openid example consumer files 
 * see https://github.com/openid/php-openid/tree/master/examples/consumer/
 */
class Controller_Authenticate extends Base_Template_Controller {

	protected $template = 'empty';
	protected $store_path;

	public function __construct(Request $request, Response $response) {
		parent::__construct($request, $response);
		$this->store_path = APPPATH . 'cache/openid_consumer_data';
	}

	public function before()
	{
		parent::before();
		
		// Ensure this script has permission to create the data store path
		if (!file_exists($this->store_path) && !@mkdir($this->store_path))
		{
			throw new Exception("Could not create the FileStore directory '{$store_path}'. Please check the effective permissions.");
		}

		// Set the include path to the OpenID directory
		ini_set('include_path', APPPATH . 'vendor/openid');

		// Load the required OpenID files
		require_once Kohana::find_file('vendor', 'openid/Auth/OpenID/Consumer');
		require_once Kohana::find_file('vendor', 'openid/Auth/OpenID/FileStore');
		require_once Kohana::find_file('vendor', 'openid/Auth/OpenID/SReg');
		require_once Kohana::find_file('vendor', 'openid/Auth/OpenID/PAPE');
		require_once Kohana::find_file('vendor', 'openid/Auth/OpenID/AX');		
	}
	
	public function action_index() {
		$this->setView('authenticate/login');
		$this->view->return_url = $this->request->return_url;
	}
	
	public function action_mobile() {
		$this->template = null;
		parent::before(); // init default template
		$this->template->content_title = 'כניסה לאתר';
		$this->setView('authenticate/login-mobile');
		$this->view->return_url = $this->request->return_url;
	}
	
	/**
	 * Create the OAUTH entities for OAUTH 1.0a
	 * @param string $name provider name
	 * @return multitype:OAuth_Provider OAuth_Consumer
	 */
	public function getOAuth($name) {
		$name = strtolower($name);
		return [
			OAuth_Provider::factory(ucfirst($name)),
			OAuth_Consumer::factory(Kohana::$config->load('oauth')->$name),
		];
	}
	
	/**
	 * Create the OAUTH entities for OAUTH 2.0
	 * @param string $name provider name
	 * @return multitype:OAuth2_Provider OAuth2_Client
	 */
	public function getOAuth2($name) {
		$name = strtolower($name);
		return [
			OAuth2_Provider::factory(ucfirst($name)),
			OAuth2_Client::factory(Kohana::$config->load('oauth')->$name),
		];
	}
	
	public function action_oauth() 
	{
		list ($provider, $consumer) = $this->getOAuth($this->request->provider); // Load the oauth operations
		
		$callback = $finish_url = URL::site('authenticate/oauthcb?return_url=' . urlencode($this->request->return_url).
				'&provider=' . urlencode($this->request->provider), TRUE);
		$consumer->callback($callback); // register callback
		$token = $provider->request_token($consumer); // Get a request token
		$this->session->set(OAUTH_REQUEST_TOKEN, $token); // store the token for later
		$this->redirect($provider->authorize_url($token)); // start authentication
	}
	
	public function action_oauthcb() 
	{
		if (!($verifier = $this->request->oauth_verifier)) {
			$this->session->error(__("אישור גישה נכשל!"));
			$this->session->delete(OAUTH_REQUEST_TOKEN);
			$this->redirect($this->request->return_url);
		}
		
		$token = $this->session->get(OAUTH_REQUEST_TOKEN);
		if (!$token OR $token->token !== $this->request->oauth_token) { // invalid token
			$this->session->error(__("Invalid oauth token"));
			$this->session->delete(OAUTH_REQUEST_TOKEN);
			$this->redirect($this->request->return_url);
		}
		 
		list ($provider, $consumer) = $this->getOAuth($this->request->provider); // Load the oauth operations
		
		$token->verifier($verifier); // Store the verifier in the token
		$token = $provider->access_token($consumer, $token); // Exchange the request token for an access token
		$this->session->set(OAUTH_ACCESS_TOKEN, $token);
		$this->session->delete(OAUTH_REQUEST_TOKEN); // Request token is no longer needed
		$token_text = 'twitter:' . (string)$token;
		var_dump($token);
		
		try {
			Log::debug("Logging in user by OAuth token");
			$this->session->login(Model_User::byCredentials($token_text));
			$this->redirect($this->request->return_url);
			return;
		} catch (Exception_NotFound $e) {
			// we need to create a new user account - show the user the "new account page"
		}
		
		$query = [ 
			'return_url=' . urlencode($this->request->return_url),
			'fullname=' . urlencode($token->screen_name),
		];
		
		$this->session->set(Controller_Register::NEW_USER_CREDENTIALS, $token_text);
		$this->redirect(Route::get('authenticate')->uri(['controller' => 'register', 'action' => 'newuser',]). 
				'?' . join('&', $query));
	}
	
	private function getOAuth2CallbackURI($return_url, $provider) {
		/*[
				'return_url=' . urlencode($return_url),
				'provider=' . urlencode($provider)
		]*/
		return URL::site('authenticate/oauth2cb', true);
	}
	
	public function action_oauth2()
	{
		list ($provider, $client) = $this->getOAuth2($this->request->provider); // Load the oauth operations
	
		$callback = $finish_url = $this->getOAuth2CallbackURI($this->request->return_url, $this->request->provider);
		$client->callback($callback); // register callback
		$args = [ 'state' => serialize([$this->request->return_url, $this->request->provider]) ];
		switch ($this->request->provider) {
			case 'google':
				$args['scope'] = 'profile email';
				break;
			case 'facebook':
				$args['scope'] = 'email,public_profile';
				break;
		}
		$this->redirect($provider->authorize_url($client, $args));
	}
	
	public function action_oauth2cb()
	{
		if ($this->request->state) { // decode state from Google OAUTH2 response
			list($this->request->return_url, $this->request->provider) = unserialize($this->request->state);
		}
			
		$code = $this->request->code;
		if (empty($code)) {
			$this->session->error(__("אישור גישה נכשל!"));
			$this->redirect($this->request->return_url);
		}
		
		list ($provider, $client) = $this->getOAuth2($this->request->provider); // Load the oauth operations
		$client->callback($this->getOAuth2CallbackURI($this->request->return_url, $this->request->provider)); // facebook needs this set
		
		switch ($this->request->provider) {
			case 'google':
				$desired_scope = 'openid email profile';
				break;
			case 'twitter':
				$desired_scope = 'email,friends_about_me';
				break;
			default:
				$desired_scope = 'email';
		}
		
		$token = $provider->access_token($client, $this->request->code, [
				'scope' => $desired_scope,
		]);
		$this->session->set(OAUTH_ACCESS_TOKEN, $token);
		$token = $provider->get_user_details($client, $token);
		
		try {
			Log::debug("Logging in user by OAuth2 token {$token->token}");
			$this->session->login(Model_User::byCredentials($token->token));
			$this->redirect($this->request->return_url);
			return;
		} catch (Exception_NotFound $e) {
			// hack around Google API change from OpenID to OAuth
			if ($this->request->provider == 'google') {
				// assume email field is correct
				try {
					$user = Model_User::byEmail($token->email);
					//if (strstr($user->credentials, 'accounts/o8')) {
						// need to retro fit OAuth credentials instead of OpenID
						$user->credentials = $token->token;
						$user->save();
						// and we're done
						$this->session->login($user);
						$this->redirect($this->request->return_url);
						return;
					//}
				} catch (Exception_NotFound $e1){ // no such user, fallback to original behavior
				}
			}
			// hack around facebook ID change
			if ($this->request->provider == 'facebook') {
				try {
					$user = Model_User::byEmail(@$token->email);
					//if (strlen($user->credentials) <= 20) { // it looks like facebook ids change every now and then. I should really look into this
					if (strpos($user->credentials,"facebook:") === 0) { // for now accept all facebook users that facebook did auth
						// need to retro fit new OAuth credentials
						$user->credentials = $token->token;
						$user->save();
						// and we're done
						$this->session->login($user);
						$this->redirect($this->request->return_url);
						return;
					}
				} catch (Exception_NotFound $e1){ // no such user, fallback to original behavior
				}
			}
			// we need to create a new user account - show the user the "new account page"
		}
		
		$query = [ 
			'return_url=' . urlencode($this->request->return_url),
			'fullname=' . urlencode($token->screen_name),
			'email=' . urlencode(isset($token->email) ? $token->email : ''),
		];
		
		$this->session->set(Controller_Register::NEW_USER_CREDENTIALS, $token->token);
		$this->redirect(Route::get('authenticate')->uri(['controller' => 'register', 'action' => 'newuser',]). 
				'?' . join('&', $query));
	}

	public function action_openid()
	{
		if (!$this->request->openid_identity)
			return $this->action_index(); // show login page 
		
		$this->template->title = __('OpenID sign in');
		$this->setView('authenticate/openid/signin')
			->bind('errors', $errors);

		$this->view->return_url = $this->request->return_url;

		// If openid_identity variable exists in the request, then add it to POST
		if (!is_null($this->request->openid_identity))
			$this->request->post('openid_identity', strip_tags(trim($this->request->openid_identity)));

		$data = Validation::factory($this->request->post())
			->rule('openid_identity', 'not_empty')
			->rule('openid_identity', 'url');

		if (!$data->check()) { // Validation fail!
			$this->request->post($data->as_array());
			$errors = $data->errors('auth');
			return;
		}

		// Begin the OpenID authentication process
		$this->auto_render = false;
		$openid = $data['openid_identity'];

		$consumer = new Auth_OpenID_Consumer(new Auth_OpenID_FileStore($this->store_path));
		$auth_request = $consumer->begin($openid);
		if (!$auth_request)
			throw new Exception(__('Authentication error: not a valid OpenID.'));

		$sreg_request = Auth_OpenID_SRegRequest::build(array('email'), array('fullname'));
		$auth_request->addExtension($sreg_request);

		$pape_request = new Auth_OpenID_PAPE_Request(null, 180);
		$auth_request->addExtension($pape_request);
		
		// add AttributeExchange request to fetch personal details from providers that support AX (Google)
		$ax = new Auth_OpenID_AX_FetchRequest();
		$ax->add(Auth_OpenID_AX_AttrInfo::make(AX_EMAIL_ID, 2, true));
		$ax->add(Auth_OpenID_AX_AttrInfo::make(AX_NAME_FIRST_ID, 1, true));
		$ax->add(Auth_OpenID_AX_AttrInfo::make(AX_NAME_LAST_ID, 1, true));
		$auth_request->addExtension($ax);

		// Build the redirect URL with the return page included
		$finish_url = URL::site('authenticate/finish', true);
		$_SESSION['openid_return_to_url'] = $this->request->return_url;
		
		// Redirect the user to the OpenID server for authentication.
		// Store the token for this authentication so we can verify the response.
		
		// For OpenID 1, send a redirect:
		if ($auth_request->shouldSendRedirect()) {
			$redirect_url = $auth_request->redirectURL(URL::base(TRUE, TRUE), $finish_url);

			if (Auth_OpenID::isFailure($redirect_url))
				throw new Exception(__('Could not redirect to server:').' '.$redirect_url->message);

			$this->redirect($redirect_url);
			return;
		}
		
		// For OpenID 2 the OpenID library will return a full html document
		$this->response->body($auth_request->htmlMarkup(URL::base(TRUE, TRUE), $finish_url, false, array('id' => 'openid_message')
		));
	}

	public function action_finish() {
		// Get the OpenID identity
		$openid = $this->request->openid_identity;
		$return_url = $this->request->return_url ? $this->request->return_url : URL::base();
		if ($_SESSION['openid_return_to_url'])
			$return_url = $_SESSION['openid_return_to_url'];

		$consumer = new Auth_OpenID_Consumer(new Auth_OpenID_FileStore($this->store_path));
		
		// workaround incompatibility between mod_rewrite and janrain
		$url_parts = parse_url( $_SERVER[ 'REQUEST_URI' ] );
		$siteurl = URL::site($this->request->uri(),true);
		//Log::debug('trying to complete openid with',[$siteurl,$url_parts]);
		$siteurl = URL::site('authenticate/finish', true);
		$response = $consumer->complete($siteurl);

		if ($response->status == Auth_OpenID_CANCEL)
			throw new Exception(__('OpenID authentication cancelled.'));
		if ($response->status == Auth_OpenID_FAILURE) {
			Log::error("OpenID authentication failure: ".print_r($response,true));
			$this->session->error(__("An error occured during authentication, please try again."));
			$this->redirect($return_url);
			return;
		}
		if ($response->status != Auth_OpenID_SUCCESS) // sanity check
			throw new Exception(__('Unknown error from OpenID authentication:').' '.$response->message);
		
		Log::debug("Authenticated " . $response->identity_url);
		
		$openid = htmlentities( $response->getDisplayIdentifier() );
		// log in completed

		// Check if we know this user
		try {
			Log::debug("Logging in user by OpenID token");
			$this->session->login(Model_User::byCredentials($openid));
			$this->redirect($return_url);
			return;
		} catch (Exception_NotFound $e) {
			// we need to create a new user account - show the user the "new account page"
		}
		
		$query = [ 'return_url=' . urlencode($return_url) ];
		
		$sreg = Auth_OpenID_SRegResponse::fromSuccessResponse($response)->contents();
		$ax = Auth_OpenID_AX_FetchResponse::fromSuccessResponse($response);
		
		$email = @$sreg['email'] ? $sreg['email'] : null;
		if (!is_null($ax))
			$email = $ax->getSingle(AX_EMAIL_ID, $email);		
		$fullname = @$sreg['fullname'] ? $sreg['fullname'] : null;
		if (!is_null($ax)) {
			$firstname = $ax->getSingle(AX_NAME_FIRST_ID);
			$lastname = $ax->getSingle(AX_NAME_LAST_ID);
			if ($firstname and $lastname)
				$fullname = "$firstname $lastname"; 
		}
		
		if (!is_null($email))
			$query[] = 'email=' . urlencode($email);
		if (!is_null($fullname))
			$query[] = 'fullname=' . urlencode($fullname);

		// try to figure out if the user is registered with us already, but changed their token
		try {
			$user = Model_User::byEmail($email);
			$user->credentials = $openid;
			$user->save();
			Log::debug("Logging in user by email");
			$this->session->login($user); // this is probably not secure - TODO: think about it!
			$this->redirect($return_url);
			return;
		} catch (Exception_NotFound $e) {
			// user is unknown, continue with new user stuff
		}
			
		$this->session->set(Controller_Register::NEW_USER_CREDENTIALS, $openid);
		$this->redirect(Route::get('authenticate')->uri([
				'controller' => 'register',
				'action' => 'newuser',
		]). '?' . join('&', $query));
	}
	
	/**
	 * Support username/password legacy login process, with shortcut for registration triggering
	 */
	public function action_login() {
		$email = $this->request->login;
		if (!$email) {
			$this->jsonResponse(['status' => false, 'message' => 'missing user login']);
			return;
		}
		
		if ($this->request->password) {
			try {
				$this->session->login(Model_User::byPassword($email, $this->request->password));
				Log::debug("Successfully password authed $email: " . $this->session->loggedInUser());
				$this->jsonResponse(['status' => true]);
			} catch (Exception_NotFound $e) {
				Log::debug("Password verify failed for $email");
				// invalid login
				$this->jsonResponse(['status' => false, 'message' => 'שם משתמש או סיסמה שגויים']);
			}
			return;
		}
		
		// check if the user exists or not, to direct the user to registration ot login
		try {
			$user = Model_User::byEmail($email);
			if ($user->credentials) {
				$this->jsonResponse([
						'status' => true, 
						'hasPassword' => $user->isPasswordAuthenticated(),
						'provider' => $user->getAuthProvider(),
				]);
				return;
			} 
		} catch (Exception_NotFound $e) {
		}
		$this->jsonResponse(['status' => false, 'משתמש לא קיים']);
	}
	
	public function action_register() {
		$email = $this->request->login;
		if (!$email) {
			$this->jsonResponse(['status' => false, 'message' => 'missing user login']);
			return;
		}
		
		$password = $this->request->password;
		if (!$password) {
			$this->jsonResponse(['status' => false, 'message' => 'missing user login']);
			return;
		}
		
		$query = [
			'return_url=' . urlencode($this->request->return_url),
			'email=' . urlencode($email),
		];
		
		$this->session->set(Controller_Register::NEW_USER_CREDENTIALS, password_hash($password, PASSWORD_DEFAULT, ['cost'=>14]));
		$this->jsonResponse([ 
				'status' => true, 
				'register_url' => '/controll/' . Route::get('authenticate')->uri(['controller' => 'register', 'action' => 'newuser',]). '?' . join('&', $query)
			]);		
	}
	
	public function action_clienterror() {
		if ($this->request->message)
			$this->session->error($this->request->message);
		throw new HTTP_Exception_500('Client error');
	}
	
	public function action_logincost() {
		$timeTarget = $this->request->target;
		$timeTarget = ($timeTarget) ? (float)$timeTarget : 0.2;
		
		$cost = 9;
		do {
			$cost++;
			$start = microtime(true);
			password_hash("test", PASSWORD_DEFAULT, ["cost" => $cost]);
			$end = microtime(true);
		} while (($end - $start) < $timeTarget);
		
		$this->jsonResponse(['cost' => $cost]);
	}
	
	public function action_logout() {
		$this->session->logout();
		$this->redirect($this->request->return_url ? $this->request->return_url : '/');
	}
	
}

if (!function_exists("__")) {
	function __($text) {
		return $text;
	}
}
