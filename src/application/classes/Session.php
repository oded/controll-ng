<?php

abstract class Session extends Kohana_Session {
	
	private $loggedInUser;
	
	/**
	 * Log in the specified user
	 * @param Model_User $user
	 */
	public function login(Model_User $user) {
		$this->loggedInUser = $user;
		$user->last_login = time();
		//Log::debug("user: ", $user);
		$user->save();
		$this->set('logged-in-user',$user->id);
	}
	
	/**
	 * Log out the current logged in user
	 */
	public function logout() {
		$this->delete('logged-in-user');
	}

	/**
	 * Retrieve the currently logged in user
	 * @return Model_User|NULL
	 */
	public function loggedInUser() {
		if ($this->loggedInUser)
			return $this->loggedInUser;
		$userid = $this->get('logged-in-user');
		if (!$userid)
			return null; // no user logged in
		
		$user = new Model_User($userid);
		if (!$user->loaded())
			return null; // user no longer exist
		return $this->loggedInUser = $user;
	}

	/**
	 * Store or retrieve the transient error message
	 * @param string $message message to store, or null to retrieve
	 */
	public function error($message = null) {
		if (!is_null($message)) {
			$uid = is_null($this->loggedInUser) ? '' : $this->loggedInUser->id; 
			Log::warn("User $uid encountered an error: $message");
			$this->set('transient-error-message', $message);
			return;
		}
		
		return $this->get_once('transient-error-message');
	}
}