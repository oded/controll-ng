<?php

class Log extends Kohana_Log {

    private static $inited = false;
	
	public static function debug($message, $values = null) {
		$data = "";
		if ($values) {
			$values = func_get_args();
			array_shift($values); 
			$data = "\n" . print_r($values, true);
		}
		self::logmessage(self::DEBUG, $message . $data);
	}
	public static function info($message, $values = null) {
		self::logmessage(self::INFO, $message, $values);
	}
	public static function warn($message, $values = null) {
		self::logmessage(self::WARNING, $message, $values);
	}
	public static function error($message, $values = null) {
		self::logmessage(self::ERROR, $message, $values);
	}
	
	private static function logmessage($level, $message, $values = null) {
	    if (static::$inited) {
	        Kohana::$log->add($level, $message, $values);
	        return;
	    }
	    if (class_exists('Kohana',false) && Kohana::$log) {
	        static::$inited = true;
	        self::logmessage($level, $message, $values);
	        return;
	    }
	    file_put_contents('php://stderr', '[{$level}] $message '.($values?print_r($values,true):''));
	}
	
}