<?php defined('SYSPATH') or die('No direct script access.');

define('DEFAULT_TIMEZONE',"Asia/Jerusalem");

require_once TWIGPATH.'vendor/twig/lib/Twig/Autoloader.php';

class Twig extends Kohana_Twig {

    protected static function env()
    {
        // Instantiate the base Twig environment from parent class.
        $env = parent::env();
        
        $config = Kohana::$config->load('twig');
        $filters = $config->get('html_filters');
        if (is_array($filters))
	        foreach ($filters as $key => $value)
	        {
	        	$filter = new Twig_SimpleFilter($key, $value, ['pre_escape' => 'html', 'is_safe' => ['html']]);
	        	$env->addFilter($filter);
	        }        

		$env->getExtension('core')->setTimezone(DEFAULT_TIMEZONE);
		static::addMarkdown($env);

        return $env;
    }
    
    private static function addMarkdown($env) {
		foreach ([
			Kohana::find_file('vendor/markdown', 'MarkdownBundle/Converter/Markdown'),
			Kohana::find_file('vendor/markdown', 'MarkdownBundle/Extension/MarkdownExtension'),
			] as $path) {
			if (!$path)
			    return;
			require_once $path;
		}
		$env->addExtension(new \MarkdownBundle\Extension\Markdown_Extension());
    }

} // End Twig
