<?php

class Task_Resetdb extends Minion_Task {
	
	protected $_options = array(
			'target' => 'vdev',
	);
	
	private function abort($message) {
    	error_log($message);
    	exit(1);
    }
    
	/**
	 * Reset database
	 *
	 * @return null
	 */
	protected function _execute(array $params) {
		$host = $params['target'];
		$basepath = realpath(APPPATH.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..");
		
		extension_loaded('mysqli') or $this->abort("Missing extension mysqli");
		$schemafile = $basepath . DIRECTORY_SEPARATOR . "configuration/database/schema.sql";
		file_exists($schemafile) or $this->abort("missing schema file");
		
		$mysql = new mysqli($host, 'root') or $this->abort("Failed to connect to the database");
		$mysql->multi_query(file_get_contents($schemafile)) or $this->abort("Failed to deploy database schema");
		do {
			$mysql->store_result();
			if ($mysql->errno > 0) $this->abort("Mysql error: " . $mysql->error);
		} while ($mysql->next_result());
		echo "done\n";
	}
	
}