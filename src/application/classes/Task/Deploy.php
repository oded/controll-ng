<?php

/**
 * Deploy ConTroll-NG to a server
 * 
 * It can accept the following options:
 *  - target: the server to deploy to. Defaults to 'vdev'
 *
 * @author oded
 */
class Task_Deploy extends Minion_Task
{
    protected $_options = array(
        	'target' => 'vdev',
    		'site' => 'default',
    		'template' => 'default',
    );
    
    private function abort($message) {
    	error_log($message);
    	exit(1);
    }
    
    private function incrementVersion($v) { 
    	$v = explode(".",$v); 
    	$v[count($v)-1] += 1; 
    	return implode(".",$v); 
    }
    
    private function tickBuildVersion($basepath) {
    	$versionfile = $basepath."/configuration/version";
    	$version = $this->incrementVersion(trim(file_get_contents($versionfile)));
    	file_put_contents($versionfile,$version);
    	
    	$specfile = $basepath . "/controll-ng.spec";
    	$spec = file_get_contents($specfile);
    	$spec = preg_replace('/(Version:\s+)[\d\.]+/i','${1}'.$version, $spec);
    	file_put_contents($specfile, $spec);
    }

    private function processDir($srcDir, $filesToIgnore, $op) {
    	$d = dir($srcDir);
    	$ignoredfiles = array_merge(['.','..'], $filesToIgnore);
    	while($file = $d->read()) { // do this for each file in the directory
    		if (!empty(array_filter($ignoredfiles, function($p) use($file) { return fnmatch($p, $file); })))
    			continue;

    		$path = $srcDir."/".$file;
    		$op($path);
    		if (is_dir($path))
				$this->processDir($path, $filesToIgnore, $op);
    	}
    	$d->close();
    }
    
    protected function countfiles($filesToIgnore, $dirs) {
    	$count = 0;
    	foreach ($dirs as $dir)
    		$this->processDir($dir, $filesToIgnore, function($path) use (&$count) {
    			$count++;
    		});
    	return $count;
	}
	
	protected function createFileMap($filesToIgnore, $dirs) {
		$filemap = [];
		foreach ($dirs as $dir)
			$this->processDir($dir, $filesToIgnore, function($path) use (&$filemap) {
				$filemap[$path] = md5(file_get_contents($path));
			});
		return $filemap;
	}
	
	protected function parseFileMap($data) {
		$output = [];
		foreach (explode("\n", str_replace("\r", "", $data)) as $line) {
			list($filename, $md5sum) = explode("\t", $line);
			if ($filename and $md5sum)
				$output[$filename] = $md5sum;
		}
		return $output;
	}
	
	protected function serializeFileMap($map) {
		$output = [];
		foreach ($map as $file => $md5sum)
			$output[] = $file . "\t" . $md5sum;
		return join("\n",$output);
	}
	
	protected function ftp_connect() {
		print("Trying to connect\n");
		$this->con = ftp_connect($this->url['host']) or die("Failed to open ".$this->url['host']."\n");
		ftp_login($this->con, $this->url['user'], $this->url['pass']) or die("Failed to login using {$url['user']}\n");
		ftp_pasv($this->con, true);
	}
	
	protected function try_ftp_fget($outfile, $path, $type) {
		for ($i = 0; $i < 2; $i++) {
			if (@ftp_fget($this->con, $outfile, $path, $type))
				return true;
			else
				$this->ftp_connect();
		}
		return false;
	}
	
	protected function try_ftp_put($remotePath, $path, $type) {
		for ($i = 0; $i < 2; $i++) {
			if (@ftp_put($this->con, $remotePath, $path, $type))
				return true;
			else
				$this->ftp_connect();
		}
		return false;
	}

	protected function try_ftp_fput($remotePath, $handle, $type) {
		for ($i = 0; $i < 2; $i++) {
			if (@ftp_fput($this->con, $remotePath, $handle, $type))
				return true;
			else
				$this->ftp_connect();
		}
		return false;
	}

	protected function deployToFTP($host, $site, $template) {
		$url = parse_url($host);
		if ($url === false)
			die("Failed to parse URL!\n");
		$this->url = $url;
		$this->ftp_connect();
		$basepath = $url['path'];
		if (substr($basepath,-1) != '/')
			$basepath.='/';
		 
		if (!@ftp_chdir($this->con, $basepath . 'src'))
			@ftp_mkdir($this->con, $basepath . 'src') or die("Failed to create base directory: ".$basepath.'src'."\n");
		if (!@ftp_chdir($this->con, $basepath . 'public_html'))
			die("No public_html available!\n");
		 
		$ignorefiles = [ '.git*', 'logs', 'cache' ];
		$countfiles = $this->countfiles($ignorefiles, ['src','html']);
		$completedfiles = 0; $lastReport = -1;
		$printProgress = function() use (&$completedfiles, $countfiles, &$lastReport) {
			$completedfiles++;
			$percent = (int)($completedfiles/$countfiles*100);
			if ($percent % 10 == 0 and $percent != $lastReport)
				echo "...{$percent}%";
				$lastReport = $percent;
		};
		$printProgress();
		
		$filelist = $this->createFileMap($ignorefiles, [ 'src','html' ]);
		$sitefilelisth = fopen("php://memory", "r+");
		if (!$this->try_ftp_fget($sitefilelisth, $basepath.'deployed-files.lst.gz', FTP_BINARY)) {
			$sitefilelist = [];
		} else {
			rewind($sitefilelisth);
			$sitefilelist = $this->parseFileMap(gzdecode(stream_get_contents($sitefilelisth)));
			fclose($sitefilelisth);
		}
		
		$this->processDir('src', $ignorefiles, function($path) use( $basepath, $printProgress, &$sitefilelist, $filelist) {
				$remotePath = $basepath . $path;
			if (is_dir($path))
				@ftp_chdir($this->con, $remotePath) or ftp_mkdir($this->con, $remotePath);
			else if (!array_key_exists($path, $sitefilelist) or $filelist[$path] != $sitefilelist[$path]) {
				$this->try_ftp_put($remotePath, $path, FTP_BINARY) or print("Error uploading $path\n");
				echo "\n$path";
			}
			unset($sitefilelist[$path]);
			$printProgress();
		});
		$this->processDir('html', $ignorefiles, function($path) use($basepath, $printProgress, &$sitefilelist, $filelist) {
		$remotePath = $basepath . 'public_html'.substr($path, strlen('html'));
			if (is_dir($path))
				@ftp_chdir($this->con, $remotePath) or ftp_mkdir($this->con, $remotePath);
			else if (!array_key_exists($path, $sitefilelist) or $filelist[$path] != $sitefilelist[$path]) {
				$this->try_ftp_put($remotePath, $path, FTP_BINARY) or print("Error uploading $path\n");
				echo "\n$path";
			}
			unset($sitefilelist[$path]);
			$printProgress();
		});
		echo "\n";
		
		echo count($sitefilelist) . " files to delete:\n" . join("\n", array_keys($sitefilelist)) . "\n";
		foreach (array_keys($sitefilelist) as $path) {
			$path = '/' . preg_replace('/^html/', 'public_html', $path);
			ftp_chdir($this->con, dirname($path));
			ftp_delete($this->con, basename($path));
		}
	
		// create a site configuration file
		$sitefile = fopen("php://memory","r+");
		fwrite($sitefile, "<?php\n"
				.'Database::$default="'. $site . '";' 
				.'Base_Template_Controller::$default="'. $template . '";' 
				."\n");
		rewind($sitefile);
		$this->try_ftp_fput($basepath.'src/application/config/site.php', $sitefile, FTP_ASCII) or print("Error uploading site configuration\n");
		fclose($sitefile);
		$sitefile = fopen("php://memory","r+");
		fwrite($sitefile, gzencode($this->serializeFileMap($filelist)));
		rewind($sitefile);
		$this->try_ftp_fput($basepath.'deployed-files.lst.gz', $sitefile, FTP_BINARY) or print("Error uploading deployed file list\n");
	}
    
    /**
     * Deploy build
     *
     * @return null
     */
    protected function _execute(array $params)
    {
        $host = $params['target'];
        $basepath = realpath(APPPATH.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..");
        
        file_exists($basepath . DIRECTORY_SEPARATOR . ".git") or $this->abort("Task can only be run from the development tree");
        
        // future support for deploying using PHP only
        //extension_loaded("ssh2") or $this->abort("SSH2 extension version >= 0.12 must be available");
        //if (!$has_archive_support) $this->abort("Missing Archive_TAR module");
    	//$ssh = ssh2_connect($host) or $this->abort("Failed to connect to SSH2 server");
    	//ssh2_auth_agent($ssh, 'root') or $this->abort("Failed to authenticate to SSH2 server");   	
        
        $this->tickBuildVersion($basepath);
        
        if (strstr($host,'amazon')) {
			passthru("bash -x $basepath/configuration/deploy-to-vdev.sh ec2-user $user");
        } elseif (strstr($host, 'vdev')) {
			passthru("bash -x $basepath/configuration/deploy-to-vdev.sh root $user");
        } elseif (strpos($host, 'ftp://') === 0) {
        	$this->deployToFTP($host, $params['site'], $params['template']);
        } else {
        	die("Unrecognized site!\n");
        }
    }
    
}
