<?php

class Exception_Duplicate extends Exception {

	var $field;
	
	public function __construct($message = '', $code = 0, $previous = null) {
		parent::__construct($message, $code, $previous);
		if (preg_match("/for key '([^']+)'/", $message, $matches)) {
			$this->field = $matches[1];
		}
	}
	
	public function getFields() {
		return $this->field;
	}
}