<?php

class HTTP_Exception_500 extends Kohana_HTTP_Exception_500 {
	
	public function get_response()
    {
        $response = Response::factory();
 
        if (!class_exists('Twig',false)) {
            $response->body((string)$this);
            return $response;
        }
        
        $template = Twig::factory('templates/' . Base_Template_Controller::$default);
		$template->content = Twig::factory('errors/500');

        // We're inside an instance of Exception here, all the normal stuff is available.
        $message = $this->getMessage();
        if (empty($message))
			$message = (string)$this->getPrevious();
        $template->content->message = $message;
        $template->content->exception = $this->getPrevious();
 
        $response->body($template->render());
        return $response;
    }
}
