<?php defined('SYSPATH') or die('No direct script access.');

class Request extends Kohana_Request {

	private $internal = [];
	
	public function __construct($uri, $client_params = array(), $allow_external = TRUE, $injected_routes = array()) {
		parent::__construct($uri, $client_params, $allow_external, $injected_routes);
	}
	
	/**
	 * Implicit request info getter
	 * @param string $key
	 */
	public function __get($key) {
		$v = $this->post($key);
		if (is_null($v))
			$v = $this->query($key);
		if (is_null($v))
			$v = $this->cookie($key);
		if (is_null($v) && isset($this->internal[$key]))
			$v = $this->internal[$key];
		return $v;
	}
	
	public function __set($key, $value) {
		$this->internal[$key] = $value;
		return $value;
	}
	
	public function __isset($name) {
		return !is_null($this->__get($name));
	}
	
}
