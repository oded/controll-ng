<?php defined('SYSPATH') or die('No direct script access.');

class URL extends Kohana_URL {

	static $redirect_rules = [];
	
	public static function setRedirectRules($rule, $target = null) {
		if (is_array($rule))
			static::$redirect_rules = array_merge(static::$redirect_rules, $rule);
		elseif (!is_null($target))
			static::$redirect_rules[$rule] = $target;
		else
			unset(static::$redirect_rules[$rule]);
		krsort(static::$redirect_rules, SORT_STRING);
	}
	
	/**
	 * Fetches an absolute site URL based on a URI segment.
	 *
	 *     echo URL::site('foo/bar');
	 *
	 * @param   string  $uri        Site URI to convert
	 * @param   mixed   $protocol   Protocol string or [Request] class to use protocol from
	 * @param   boolean $index		Include the index_page in the URL
	 * @return  string
	 * @uses    URL::base
	 */
	public static function site($uri = '', $protocol = NULL, $index = TRUE) {
		$u = $uri;
		if ($u[0] != '/')
			$u = '/' . $uri;
		foreach (static::$redirect_rules as $key => $target) {
			if (preg_match(",^$key(/|\?|$),",$u)) {
				$target = substr_replace($u, $target, 0, strlen($key));
				Log::info("Redirecting $u to $target");
				return $target;
			}
		}
		
		return parent::site($uri, $protocol, $index);
	}
}
