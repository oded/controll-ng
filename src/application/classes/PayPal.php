<?php defined('SYSPATH') or die('No direct script access.');

class PayPal extends Kohana_PayPal {
	
	public function approved($localTrxID, $transactionID, $payerDetails, $salesData) {
		$registration = $localTrxID['regs'];
		
		if (!is_array($registration))
			throw new Exception("Invalid local transaction type in PayPal fullfilment: " . $localTrxID);
		
		foreach ($registration as $reg) {
			$reg->complete($transactionID);
		}
		
		$reg->user->sendRegisterNotification($registration);
		return 'http://' . $_SERVER['SERVER_NAME'] . '/2015/כנס-ביגור/אישור-הרשמה';
		return '/events/registration-complete';
	}
	
	public function cancelled($localTrxID) {
		//$returnURL = $localTrxID['url'];
		$registration = $localTrxID['regs'];
		foreach ($registration as $reg) {
			$reg->cancel('paypal cancelled');
		}
		return 'http://' . $_SERVER['SERVER_NAME'] . '/2015/';
		return '/';
	}
}