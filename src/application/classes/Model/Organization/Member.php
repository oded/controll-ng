<?php

class Model_Organization_Member extends Base_ORM {

	protected $_belongs_to = [
			'user' => [],
			'organization' => [],
	];
	
	/**
	 * Create a new membership
	 * @param Model_Organization $organization
	 * @param Model_Convention $convention
	 */
	public static function generate(Model_User $user, Model_Organization $organization, $memberId = null, $likesSpam = true) {
		$obj = new Model_Organization_Member();
		$obj->user= $user;
		$obj->organization = $organization;
		$obj->member_id = $memberId;
		$obj->likes_spam = $likesSpam;
		$obj->save();
		return $obj;
	}
	
	/**
	 * Retrieve all records for this model
	 * @return array of model objects
	 */
	public static function getAll() {
		return (new Model_Organization_Member())->find_all();
	}
	
}
