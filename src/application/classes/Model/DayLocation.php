<?php

class Model_DayLocation_EventList {

	public $events = [];
	
	/**
	 * Check if this event can be inserted to the list without conflicting with
	 * any existing event - and if so, add it.
	 * @param Model_Event $ev event to add
	 */
	public function tryAdd(Model_Event $ev) {
		foreach ($this->events as $curev) {
			if ($curev->start < $ev->end && $ev->start < $curev->end) 
				return false; // overlap
		}
		// does not overlap any event
		$this->events[] = $ev;
		return true;
	}
	
	/**
	 * Retrieve the event for the specified time slot, if available.
	 * Return false otherwise
	 * @param int $timeslot epoch time to check
	 * @return Model_Event|boolean
	 */
	public function getEvent($timeslot) {
		foreach ($this->events as $ev) {
			if ($ev->start <= $timeslot && $ev->end > $timeslot)
				return $ev;
		}
		return false;
	}
}

class Model_DayLocation {
	
	public $location;
	public $concurrency = 1;
	var $eventlist = [ ]; // concurrent event list storage

	public function __construct(Model_Location $location, $start, $end) {
		$this->location = $location;
		$this->eventlist[] = new Model_DayLocation_EventList(); // must have at least one list
		
		// list all events for the location
		$daystart = (new DateTime(date("d-m-Y", $start)))->getTimestamp();
		$events = $this->location->getDayEvents($daystart);
		
		// go over events and position them in the list
		foreach ($events as $ev) {
			// for each event, figure out if it fits in an available list
			if ($this->tryAdd($ev))
				continue;
			// no event list can accept this event, create a new list
			$list = new Model_DayLocation_EventList();
			$list->tryAdd($ev); // can't fail as it is the first event
			$this->eventlist[] = $list;
			$this->concurrency++;
		}
	}
	
	public function tryAdd(Model_Event $event) {
		foreach ($this->eventlist as &$list) {
			if ($list->tryAdd($event))
				return true;
		}
		return false;
	}
	
	/**
	 * Fetch an event from the specified concurrent event list, if exists.
	 * return false otherwise
	 * @param int $concurencyIndex positive index that is smaller or equal to the concurrency level of the location
	 * @param int $timeslot timestamp for which to get the event
	 * @return Model_Event|boolean
	 */
	public function getEvent($concurrencyIndex, $timeslot) {
		return $this->eventlist[$concurrencyIndex-1]->getEvent($timeslot);
	}
	
	/* (non-PHPdoc)
	 * @see Kohana_ORM::__toString()
	 */
	public function __toString() {
		return (string)$this->location->__toString();
	}
	
	public function __isset($field) {
		switch ($field) {
			case 'key':
			case 'id':
			case 'count';
				return true;	
			default:
				return false;
		}
	}
	
	public function __get($field) {
		switch ($field) {
			case 'key':
				return $this->location->key;
			case 'id':
				return $this->location->id;
			case 'count':
				$count = 0;
				foreach ($this->eventlist as &$list)
					$count += count($list->events); 
				return $count;
			default:
				return null;
		}
	}
}
