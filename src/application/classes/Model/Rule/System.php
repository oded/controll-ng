<?php

class Model_Rule_System extends Base_ORM {
	
	protected $_belongs_to = [
	];
	
	protected $_has_many = [
			'events' => [],
	];

	/**
	 * Validation rules
	 * @see Kohana_ORM::rules()
	 */
	public function rules() {
		return [
				'title' => [ ['not_empty'] ],
		];
	}
	
	/**
	 * Create a new rule system
	 * @param string $title rule system title
	 * @param string $key
	 * @return Model_Event
	 */
	public static function generate($title, $key = null) {
		if (is_null($key)) $key = self::createKey($title);
		$obj = new Model_Rule_System();
		$obj->key = $key;
		$obj->title = $title;
		$obj->save();
		return $obj;
	}
	
	/**
	 * Find a rule system with the specified title, creating one if it does not exist
	 * @param string $title rule system title
	 * @return Model_Rule_System
	 */
	public static function forName($title) {
		if (!is_string($title) || empty($title))
			return null;
		$rules = (new Model_Rule_System())->where('title', '=', $title)->find();
		if ($rules and $rules->loaded())
			return $rules;
		// if title was a mismatch, but createKey will think its the same, try by key
		$rules = (new Model_Rule_System())->where('key', '=', self::createKey($title))->find();
		if ($rules and $rules->loaded())
			return $rules;
		return self::generate($title);
	}
	
	public static function all() {
		return (new Model_Rule_System())->find_all();
	}
	
}