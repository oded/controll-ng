<?php

class Model_Organization extends Base_ORM {

	protected $_has_many = array(
			'conventions' => array('model' => 'convention', 'through' => 'convention_organizer'),
			'members' => array('model' => 'user', 'through' => 'organization_member'),
	);
	
	/**
	 * Create a new orgnization record
	 * @param String $title Title of the organization
	 * @param String $key [optional] key to use, instead of generating automatically
	 */
	public static function generate($title, $key = null) {
		if (is_null($key)) $key = self::createKey($title);
		$obj = new Model_Organization();
		$obj->title = $title;
		$obj->key = $key;
		$obj->save();
		return $obj; 
	}
	
	/**
	 * Retrieve all records for this model
	 * @return array of model objects
	 */
	public static function getAll() {
		return (new Model_Organization())->find_all();
	}
	
	/**
	 * Create a new convention for this organization
	 * @param String $title Title of the convention
	 * @param String $key [optional] key to use, instead of generating automatically
	 * @return Model_Convention convention created
	 */
	public function generateConvention($title, $key = null) {
		$conv = Model_Convention::generate($title, $key);
		Model_Convention_Organizer::generate($this, $conv);
		return $conv;
	}
	
}
