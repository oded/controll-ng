<?php

class Model_Event_Occurrence extends Base_ORM {

	const STATUS_WAIT_FOR_APPROVAL = 0;
	const STATUS_APPROVED = 1;
	const STATUS_FULL = 2;
	const STATUS_CANCELLED = 99;
	
	protected $_belongs_to = [
			'event' => [],
			'location' => [],
			'staff_contact' => [ 'model' => 'user' ],
	];
	
	protected $_has_many = [
			'registrations' => [],
			'users' => ['model' => 'User', 'through' => 'event_hosts'],
	];
	
	/**
	 * Create a new event occurence
	 * @param Model_Event $event Event of this occurence
	 * @param int $startTime epoch time of event start
	 * @param int $endTime epoch time of event end
	 * @param int $maxAttendees maximum number of attendees to allow. set to null to be free to attend (no registration required)
	 * @param string $notesToAttendees
	 * @param string $notesToStaff
	 * @param string $logicalRequirements
	 * @param int $minAttendees minimum number of attendees - the event will not start unless there are at least as much registrations
	 * @return Model_Event_Occurrence
	 */
	public static function generate(Model_Event $event, $startTime, $endTime, $maxAttendees, 
			$notesToStaff = null, $logicalRequirements = null, $scheduleing_constraints = null, $minAttendees = 0, 
			$prevent_conflicts = false) {
		$obj = new Model_Event_Occurrence();
		$obj->event_id = $event;
		$obj->start = $startTime;
		$obj->end = $endTime;
		$obj->min_attendees = $minAttendees;
		$obj->max_attendees = $maxAttendees;
		$obj->status = static::STATUS_WAIT_FOR_APPROVAL;
		$obj->prevent_conflicts = $prevent_conflicts;
		$obj->location = null;
		$obj->logistical_requirments = $logicalRequirements;
		$obj->notes_to_staff = $notesToStaff;
		$obj->scheduleing_constraints = $scheduleing_constraints;
		// $obj->registration_required = !is_null($maxAttendees);
		$obj->save();
		$obj->updateHosts([$event->user]); // an event by default has at least one host
		return $obj;
	}	

	/**
	 * Register the specified user to the event occurrence
	 * @param Model_User $user user to register
	 */
	public function register(Model_User $user, $amount = 1) {
		if (!is_null($this->available_tickets) and $this->available_tickets < $amount)
			throw new Exception_NoTickets("Not enough tickets (" . $this->available_tickets .
				") in " . $this);
		return Model_Registration::generate($this, $user, $amount);
	}
	
	public static function forConvention(Model_Convention $convention) {
		return (new Model_Event_Occurrence())
			->with('event')
			->having('event:convention_id','=', $convention->id);
	}
	
	public function where_approved() {
		return $this->where('status','>=',self::STATUS_APPROVED)
				->where('status','<',self::STATUS_CANCELLED);
	}

	
	/**
	 * Handles converstion of start/end times to epoch times
	 * @see Kohana_ORM::get()
	 */
	public function get($column) {
		switch ($column) {
			case "statustext":
				switch ($this->status) {
					case self::STATUS_WAIT_FOR_APPROVAL: return "ממתין לאישור";
					case self::STATUS_APPROVED: return "מאושר";
					case self::STATUS_FULL: return "מלא";
					case self::STATUS_CANCELLED: return "בוטל";
				}
			case "start":
				return strtotime(parent::get($column));
			case "end":
				return $this->start + $this->get('length');
			case "starttext":
				return date("d/m H:i", $this->start);
			case "length":
				return parent::get($column); // convert minutes to seconds
			case "available_tickets":
				if (is_null($this->max_attendees) || $this->event->registration_required === false)
					return null;
				$free = $this->max_attendees; // (not reserving yet) - $this->reserved_tickets;
				foreach ($this->registrations->find_all() as $reg) {
					if (!$reg->isValid()) continue; // ignore invalid registrations
					$free -= $reg->amount;
				}
				return $free;
			case 'registered_tickets':
				$tickets = 0;
				foreach ($this->registrations->find_all() as $reg) {
					if (!$reg->isValid()) continue; // ignore invalid registrations
					$tickets += $reg->amount;
				}
				return $tickets;
			case 'registration_closed':
				return ($this->start - time() < 1800); // events cannot be registered to later than 30 minutes before start
			default:
				if (parent::__isset($column)) return parent::get($column);
				if ($this->event->__isset($column)) return $this->event->get($column);
				return parent::get($column);
		}
	}

	public function as_array() {
		$obj = parent::as_array();
		$obj['starttext'] = $this->starttext;
		$obj['available_tickets'] = $this->available_tickets;
		return $obj;
	}
	
	/**
	 * Handle conversions of start/end times from epoch times
	 * @see Kohana_ORM::set()
	 */
	public function set($column, $value) {
		switch ($column) {
			case 'start':
				if (is_string($value)) { // this could be an SQL date (YYYY-MM-DD HH:MM:SS)
					$val = strtotime($value); // which strtotime likes just fine
					if ($val === false) // or it could be the javascript date widget (d/m/Y H:i)
						$val = parent::parseDateString($value); // which requires special processing
					$value = $val;
				}
				return parent::set($column, self::sqlizeDate($value));
			case 'end':
				return $this->set("length", $value - $this->start);
			case 'available_tickets':
				return $value;
			default:
				return parent::set($column, $value);
		}
	}
	
	/**
	 * Update the event hosts list to be the specified list of user models or user IDs
	 * @param array $hosts
	 */
	public function updateHosts($hosts) {
		$this->remove('users'); // remove all current event hosts
		if (count($hosts))
			$this->add('users', $hosts); // add new ones
	}
	
	public function isEventHost(Model_User $u) {
		return $this->users->where('user_id', '=', $u->id)->find()->loaded(); 
	}
	
	public function __isset($column) {
		switch ($column) {
			case 'statustext':
			case 'end':
			case 'available_tickets':
			case 'registered_tickets':
			case 'starttext':
			case 'registration_closed':
				return true;
			default: 
				return parent::__isset($column) || $this->event->__isset($column);
		}
	}
	
	public function valid() {
		return $this->status >= self::STATUS_APPROVED && $this->status < self::STATUS_CANCELLED;
	}
	
	public function save(Validation $validation = NULL) {
		/* disable coupons for now - this feature should be part of the convention configuration 
		if ($this->changed('status') && $this->status == static::STATUS_APPROVED) {
			// if event is being approved - give hosts coupons
			foreach ($this->users->find_all() as $user)
				Model_Coupon::giveEventHostCoupon($user);
		}
		*/
		return parent::save($validation);
	}
	
	public function as_object() {
		$obj = parent::as_object();
		foreach (['statustext','end','available_tickets','registered_tickets','starttext','registration_closed'] as $key)
			$obj->$key = $this->get($key);
		
		return $obj;
	}
	
}
