<?php

class Model_Administrator extends Base_ORM {
	
	protected $_belongs_to = [
			'user' => [],
			'organization' => [],
			'convention' => [],
			'role' => [],
	];

	public static function makeSuperAdmin(Model_User $user) {
		$obj = new Model_Administrator();
		$obj->user = $user;
		$obj->role = Model_Role::admin();
		$obj->save();
	} 

	public static function makeOrganizationAdmin(Model_Organization $organization, Model_User $user) {
		$obj = new Model_Administrator();
		$obj->user = $user;
		$obj->organization = $organization;
		$obj->role = Model_Role::admin();
		$obj->save();
	}
	
	public static function makeConventionRole(Model_Convention $convention, Model_User $user, Model_Role $role) {
		$obj = new Model_Administrator();
		$obj->user = $user;
		$obj->organization = $convention->organizations->find(); // select first organization. TODO: figure something better
		$obj->convention = $convention;
		$obj->role = $role;
		$obj->create();
	}
	
	public static function revokeConventionAdmin(Model_Convention $convention, Model_User $user) {
		Model::factory('Administrator')
			->where('organization_id','=',$convention->organizations->find())
			->where('user_id', '=', $user)
			->where('convention_id','=',$convention)
			->find()->delete();
	}

	public static function newConventionRole(Model_Convention $convention, Model_Role $role) {
		$obj = new Model_Administrator();
		$obj->convention = $convention;
		$obj->role = $role;
		$obj->organization = $convention->organizations->find(); // select first organization. TODO: figure something better
		return $obj;
	}
}
