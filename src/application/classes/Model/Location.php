<?php

class Model_Location extends Base_ORM {
	
	private $daycache = array();
	
	protected $_belongs_to = array(
			'convention' => array()
	);
	
	protected $_has_many = array(
			'event_occurrences' => array(),		
	);
	
	/**
	 * Create a new locatin record
	 * @param Model_Convention $convention Convention that has this location
	 * @param String $title Title of the location
	 * @param String $key [optional] key to use, instead of generating automatically
	 */
	public static function generate(Model_Convention $convention, $title, $key = null) {
		if (is_null($key)) $key = self::createKey($title);
		$obj = new Model_Location();
		$obj->convention = $convention;
		$obj->title = $title;
		$obj->key = $key;
		$obj->save();
		return $obj;
	}
	
	/**
	 * Retrieve all records for this model
	 * @return array of model objects
	 */
	public static function getAll() {
		return (new Model_Location())->find_all();
	}

	/**
	 * Get all event occurrences that happen on the date that starts with the specified epoch time
	 * @param int $daystart day start epoch time
	 * @return multitype:|multitype:Model_Event_Occurrence list of event occurrences
	 */
	public function getDayEvents($daystart) {
		if (@array_key_exists($daystart, $this->daycache))
			return $this->daycache[$daystart];
		
		// get all events between day start (inclusive) and next day start (exclusive)
		$occurrences = [];
		foreach ($this->event_occurrences
			->where('start', 'BETWEEN', array(self::sqlizeDate($daystart), self::sqlizeDate($daystart+86399)))
			->find_all() as $ev)
			if ($ev->valid())
				$occurrences[] = $ev; 
		$this->daycache[$daystart] = $occurrences;
		return $occurrences;
	}
	
	/**
	 * Get all event occurrences that happen on the date that starts with the specified epoch time
	 * within the specified timeslot
	 * @param int $daystart day start epoch time
	 * @return multitype:|multitype:Model_Event_Occurrence list of event occurrences
	 */
	public function hasEvents($daystart, $timeslot) {
		$dayeventoccurrences = $this->getDayEvents($daystart);
		$occurrences = [];
		foreach ($dayeventoccurrences as $occurrence) {
			if ($occurrence->start <= $timeslot and $occurrence->end >= $timeslot and (Model_Convention::$allowUnapproved or $occurrence->valid()))
				$occurrences[] = $occurrence;
		}
		return $occurrence;
	}
	
	/**
	 * Find a convention location with the specified title or create a new location if none such exist
	 * @param Model_Convention $con convention where the location supposed to be
	 * @param unknown $title location title
	 * @return Model_Location
	 */
	public static function getByTitleOrCreateForConvention(Model_Convention $con, $title) {
		$foundloc = $con->locations->where('title','=',$title)->find();
		if ($foundloc && $foundloc->loaded())
			return $foundloc;
		
		if (!trim($title))
			return null; // don't generate locations with no title
		return self::generate($con, $title);
	}
	
	/* (non-PHPdoc)
	 * @see Kohana_ORM::__toString()
	 */
	public function __toString() {
		return (string)$this->title;
	}

}
