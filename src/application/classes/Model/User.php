<?php

// userland implementation of password_hash/password_verify because we aren't running on php 5.5 yet
require_once(APPPATH.'vendor/password_compat/lib/password.php');

class Model_User extends Base_ORM {
	
	protected $_has_many = [
			'admins' => [ 'model' => 'Administrator', 'table_name' => 'administrators' ],
			'roles' => [ 'through' => 'administrators' ],
			'event_occurrences' => [ 'model' => 'event_occurrence', 'through' => 'event_hosts' ],
			'registrations' => [ ],
			'coupons' => [ ],
			'orders' => [ 'model' => 'Merchandise_Transaction' ],
			'memberships' => [ 'model' => 'Organization_Member' ],
	];
	protected $_load_with = [ 'admins' ];
	
	private $currentAdmin = false;
	
	private $in_convention = null;
	
	public function __construct($id = null) {
		parent::__construct($id);
	}
	
	public function rules() {
		return array(
				'first_name' => array( array('not_empty') ),
				'last_name' => array( array('not_empty') ),
				'email' => array( array('not_empty'), array('email') ),
				//'date_of_birth' => array( array('date') ),
		);
	}

	public static function generate($firstName, $lastName, $email, $dateofbirth = null, $phone = null) {
		$obj = new Model_User();
		$obj->first_name = $firstName;
		$obj->last_name = $lastName;
		$obj->email = $email;
		$obj->date_of_birth = $dateofbirth;
		$obj->phone = $phone;
		$obj->registered = time();
		try {
			$obj->save();
			return $obj;
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate"))
				throw new Exception_Duplicate($e->getMessage());
			throw $e;
		}
	}
	
	public static function getAll() {
		return (new Model_User())->find_all();
	}
	
	public function isSuperAdmin() {
		return $this->admins->where('organization_id','=', null)
			->where('role_id','=',Model_Role::admin())
			->count_all() > 0;
	}
	
	public function isOrganizationAdmin(Model_Organization $organization) {
		return $this->admin->where('organization_id','=', $organization)
			->where('role_id','=',Model_Role::admin())
			->where('convention_id','=', null)->count_all() > 0;
	}
	
	public function isConventionAdmin(Model_Convention $convention) {
		return $this->admins->where('convention_id','=', $convention)
			->where('role_id','=',Model_Role::admin())
			->count_all() > 0;
	}
	
	public function isConventionStaff() {
		return $this->admins->where('convention_id','=', $convention)
			->where('role_id','=',Model_Role::staff())
			->count_all() > 0;
	}
	
	public function isMember(Model_Organization $organization) {
		return $this->memberships->where('organization_id','=', $organization->id)->count_all() > 0;
	}
	
	public function addMember(Model_Organization $organization, $member_id) {
		Model_Organization_Member::generate($this, $organization, $member_id);
	}
	
	public function isAdmin() { 
		return $this->currentAdmin; 
	}

	public function resolveAdmin(Model_Convention $con) {
		$this->currentAdmin = $this->isSuperAdmin() || $this->isConventionAdmin($con);
	}

	public function hasRole(Model_Convention $convention, $role) {
		if (is_string($role))
			$role = Model_Role::byKey($role);
		return $this->admins->with('role')
			->where('role_id', '=', $role)
			->where('convention_id','=',$convention)->count_all() > 0;
	}
	
	public function grantRole(Model_Convention $convention, Model_Role $role) {
		// can't use add('roles') because the connection table has meta data
		Model_Administrator::makeConventionRole($convention, $this, $role); 		
	}
	
	public function revokeRole(Model_Convention $convention, Model_Role $role) {
		$admin = $this->admins->where('role_id','=', $role)
			->where('convention_id','=',$convention)->find();
		if ($admin->loaded())
			$admin->delete();
	}
	
	public function inConvention(Model_Convention $convention) {
		$this->in_convention = $convention;
	}

	public function volunteerForHosting(Model_Convention $con, $volunteer = true) {
		if ($volunteer)
			Model_Volunteer::ensure($this, $con, Model_Volunteer::TYPE_HOST);
		else
			Model_Volunteer::delist($this, $con, Model_Volunteer::TYPE_HOST);
	}
	
	public function volunteer(Model_Convention $con, $volunteer = true) {
		if ($volunteer)
			Model_Volunteer::ensure($this, $con, Model_Volunteer::TYPE_VOLUNTEER);
		else
			Model_Volunteer::delist($this, $con, Model_Volunteer::TYPE_VOLUNTEER);
	}
	
	public function didVolunteerForHosting(Model_Convention $con) {
		return in_array(Model_Volunteer::TYPE_HOST, Model_Volunteer::forUserInCon($this,$con));
	}
	
	public function didVolunteer(Model_Convention $con) {
		return in_array(Model_Volunteer::TYPE_VOLUNTEER, Model_Volunteer::forUserInCon($this,$con));
	}
	
	/**
	 * Look up a user by a credential token (password or external auth token)
	 * @param string $token
	 * @throws Exception_NotFound
	 * @return Model_User;
	 */
	public static function byCredentials($token) {
		$user = (new Model_User())->where('credentials', '=', $token)->find();
		if (!$user->loaded())
			throw new Exception_NotFound();
		return $user;
	}
	
	/**
	 * Look up a user by their email
	 * @param string $email E-Mail address 
	 * @throws Exception_NotFound
	 * @return Model_User
	 */
	public static function byEmail($email) {
		if (!$email)
			throw new Exception_NotFound("No email provided");
		$user = (new Model_User())->where('email', '=', $email)->find();
		if (!$user->loaded())
			throw new Exception_NotFound();
		return $user;
	}
	
	/**
	 * Get a user by login credentials
	 * @param string $login
	 * @param string $password
	 * @throws Exception_NotFound
	 * @return Model_User
	 */
	public static function byPassword($login, $password) {
		$user = Model_User::byEmail($login);
		if (!password_verify($password, $user->credentials))
			throw new Exception_NotFound(); // invalid password
		return $user;
	}
	
	/**
	 * Get an array of users that have the specific role in the specific convention
	 * @param Model_Convention $con
	 * @param Model_Role|string $role
	 * @return Model_User[]
	 */
	public static function byConventionRole(Model_Convention $con, $role) {
		if (is_string($role))
			$role = Model_Role::byKey($role);
		$users = [];
		foreach ((new Model_Administrator())
			->with('user')
			->with('role')
			->having('convention_id', '=', $con->id)
			->having('role:key','=', $role->key)
			->find_all() as $admin)
			$users[] = $admin->user;
		return $users;
	}
	
	public function isPasswordAuthenticated() {
		return strpos($this->credentials, '$') === 0;
	}
	
	public function getAuthProvider() {
		if($this->isPasswordAuthenticated())
			return 'password';
		$provider = explode(':', $this->credentials)[0];
		if ($provider == 'https') // retro fit Google OpenID
			return 'google';
		return $provider;
	}
	
	public function setPassword($password) {
		if (!$this->isPasswordAuthenticated())
			throw new Exception_NotFound('לא ניתן לשנות אימות מספק חיצוני לסיסמה');
		$this->credentials = password_hash($password, PASSWORD_DEFAULT, ['cost'=>14]);
		$this->save();
	}
	
	public function sendRegisterNotification($registrations) {
		Log::info("Sending registration notification to " . $this->email);
		
		$headers = join("\r\n",[
				'From: כנס ביגור <games@bigor.org.il>',
				'Reply-To: billing@bigor.org.il',
				'Content-type: text/html; charset=utf-8',
				'Message-Id: <controll-ng-' . sha1(microtime()) . '@bigor.org.il>',
		]);
		
		$view = Twig::factory('convention/registration-email');
		$view->user = $this;
		$view->registrations = $registrations;
		
		@mail($this->email, 'אישור הרשמה לארועי כנס ביגור', $view->render(), $headers);
	}
	
	public function sendResetPassword() {
		Log::info("Sending password reset to " . $this->email);
		$token = hash("sha256", microtime().$this->email.$this->credentials);
		$this->addToken($token);
		
		$email = new Email('בקשת איפוס סיסמה מכנס ביגור', 'password-reset', [
				'user' => $this,
				'token' => $token,
				]);
		$email->send($this->email);
	}
	
	public function addToken($token) {
		$tokens = $this->loadTokens();
		$tokens[$token] = time();
		$this->storeTokens($tokens);
	}
	
	public function hasToken($token) {
		$tokens = $this->loadTokens();
		return isset($tokens[$token]);
	}
	
	public function revokeToken($token) {
		$tokens = $this->loadTokens();
		unset($tokens[$token]);
		$this->storeTokens($tokens);
	}
	
	private function loadTokens() {
		$tokens = unserialize($this->tokens);
		if ($tokens === false)
			$tokens = [];
		return $tokens;
	}
	
	private function storeTokens($tokens) {
		$this->tokens = serialize($tokens);
		$this->save();
	}
	
	public function totalCoupons() {
		$credit = 0;
		foreach ($this->coupons->find_all() as $c)
			$credit += $c->credit;
		return $credit;
	}
	
	public function remainingCoupons() {
		$credit = 0;
		foreach ($this->coupons->where('used','=','0')->find_all() as $c)
			$credit += $c->credit;
		return $credit;
	}
	
	public function as_array() {
		$obj = parent::as_array();
		$obj['fullname'] = $this->fullname;
		$obj['memberships'] = $this->memberships->get_all_objects();
		$obj['remaining_coupons'] = $this->coupons->where('used','=','0')->get_all_objects();
		// this method is noramlly used to expose the user object to client code, so hide the credentials
		unset($obj['credentials']);
		return $obj;
	}
	
	public function __isset($id) {
		switch ($id) {
			case 'tokentype':
			case 'pending_registrations':
			case 'fulfilled_registrations':
			case 'discount':
			case 'name':
			case 'fullname':
			case 'hosted_events':
			case 'convention_registrations':
			case 'pending_orders':
			case 'fulfilled_orders':
			case 'ready_transactions':
			case 'pending_transactions':
			case 'fulfilled_transactions':
			case 'phone_pending':
				return true;
			default:
				return parent::__isset($id);
		}
	}
	
	/**
	 * Handles converstion of start/end times to epoch times
	 * @see Kohana_ORM::get()
	 */
	public function get($column) {
		switch ($column) {
			case 'tokentype':
				if (empty($this->credentials))
					return '';
				if (strstr($this->credentials, 'google.com'))
					return 'G';
				if (strstr($this->credentials, 'facebook:'))
					return 'F';
				if ($this->credentials[0] == '$')
					return 'P';
				return 'T';
			case "date_of_birth":
			case "registered":
			case "last_login":
				$time = strtotime(parent::get($column));
				return ($time <= -62169992440) ? null : $time; // ignore '0000-00-00 00:00:00'
			case 'convention_registrations':
				if ($this->in_convention)
					return $this->registrations->with('event_occurrence:event:convention')->where('event_occurrence:event:convention.id','=',$this->in_convention->id);
				return $this->registrations;
			case 'pending_registrations':
				return $this->convention_registrations->where('registration.status', '<=', Model_Registration::STATUS_PENDING_PHONE)->find_all();
			case 'fulfilled_registrations':
				return $this->convention_registrations->where('registration.status', '=', Model_Registration::STATUS_FULFILLED);
			case 'discount':
				$discount = 0;
				foreach ($this->coupons->where('used','=','0')->find_all() as $c)
					$discount += $c->credit;
				return $discount;
			case 'name':
			case 'fullname':
				return $this->first_name . ' ' . $this->last_name;
			case 'hosted_events':
				if ($this->in_convention)
					return $this->event_occurrences->with('event:convention')->where('event:convention.id','=',$this->in_convention->id);
				return $this->event_occurrences;
			case 'pending_orders':
				return $this->orders->where('status', '<=', Model_Registration::STATUS_PENDING_PHONE)->find_all();
			case 'fulfilled_orders':
				return $this->orders->where('status', '=', Model_Registration::STATUS_FULFILLED)->find_all();
			case 'ready_transactions':
				return $this->convention_registrations->where('registration.status', '<', Model_Registration::STATUS_FULFILLED)->find_all()->as_array()
						+ $this->orders->where('status', '<', Model_Registration::STATUS_FULFILLED)->find_all()->as_array();
			case 'pending_transactions':
				return $this->convention_registrations->where('registration.status', '<', Model_Registration::STATUS_FULFILLED)->find_all()->as_array()
						+ $this->orders->where('status', '<', Model_Registration::STATUS_FULFILLED)->find_all()->as_array();
			case 'fulfilled_transactions':
				return $this->convention_registrations->where('registration.status', '=', Model_Registration::STATUS_FULFILLED)->find_all()->as_array()
						+ $this->orders->where('status', '=', Model_Registration::STATUS_FULFILLED)->find_all()->as_array();
			case 'phone_pending':
				return $this->convention_registrations->where('registration.status', '=', Model_Registration::STATUS_PENDING_PHONE)->find_all()->as_array()
						+ $this->orders->where('status', '=', Model_Registration::STATUS_PENDING_PHONE)->find_all()->as_array();
			default:
				return parent::get($column);
		}
	}
	
	/**
	 * Handle conversions of start/end times from epoch times
	 * @see Kohana_ORM::set()
	 */
	public function set($column, $value) {
		switch ($column) {
			case "date_of_birth":
			case "registered":
			case "last_login":
				return parent::set($column, self::sqlizeDate($value));
			default:
				return parent::set($column, $value);
		}
	}
	
}
