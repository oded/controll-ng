<?php

class Model_Role extends Base_ORM {
	
	const TYPE_ADMIN = 'admin';
	const TYPE_STAFF = 'staff';
	const TYPE_CASHIER = 'קופאי';
	
	public static function admin() {
		return (new Model_Role())->where('key', '=', self::TYPE_ADMIN)->find();
	}
	
	public static function staff() {
		return (new Model_Role())->where('key', '=', self::TYPE_STAFF)->find();
	}
	
	public static function byKey($key) {
		$role = (new Model_Role())->where('key', '=', $key)->find();
		if ($role->loaded())
			return $role;
		throw new Exception_NotFound();
	}
	
}
