<?php

abstract class Model_Base_Transaction extends Base_ORM {
	
	const STATUS_IN_PROCESS = 0;
	const STATUS_PENDING = 1;
	const STATUS_PENDING_PHONE = 2;
	const STATUS_FULFILLED = 3;
	const STATUS_CANCELLED = 4;
	
	/**
	 * Cancel the registration, returning the tickets to the system and record the cancel reason.
	 * Also, refund coupon that were previously consumed by this registration.
	 * @param unknown $reason
	 */
	public function cancel($reason) {
		$this->status = self::STATUS_CANCELLED;
		$this->cancel_transaction_id = $reason;
		$this->save();
	
		if (!$this->discount_code)
			return;
		// reimburse used coupons
		foreach (explode(",", $this->discount_code) as $couponId) {
			$id = (int)$couponId;
			if ($id <= 0)
				continue; // ignore invalid coupon
			$coupon = new Model_Coupon($id);
			if (!$coupon->loaded())
				continue; // ignore invalid coupon
			if (!$coupon->used)
				continue; // ignore coupon that was alread refunded by another reg on the transaction
			$coupon->used = false;
			$coupon->save();
		}
	}
	
	public function complete($transactionId) {
		$this->status = self::STATUS_FULFILLED;
		$this->transaction_id = $transactionId;
		$this->save();
	}
	
	/**
	 * Locate all registrations done on the same transaction
	 * @param $only_fulfilled boolean only find registrations that have not been refunded yet
	 * @return multitype:array|Database_Result
	 */
	public function allInTransaction($only_fulfilled = true) {
		if (!$this->transaction_id)
			return [$this]; // could happen for zero-cost registrations that weren't billed
		if ($this->status != static::STATUS_FULFILLED)
			return []; // sanity first
		$clazz = get_class($this);
		$q = (new $clazz())->where('transaction_id','=',$this->transaction_id);
		if ($only_fulfilled)
			$q = $q->where('status', '=', static::STATUS_FULFILLED);
		return $q->find_all();
	}
	
	/**
	 * Refund the transaction that this registration belongs too, and cancel all
	 * registrations on this transaction
	 */
	public function refund($ticketcount) {
		if ($this->status != static::STATUS_FULFILLED)
			throw new Exception("Only fulfilled transactions with a transaction ID can be refunded");
		if (!$this->transaction_id) {// zero-cost registgration
			 $this->cancel("non-billable cancel");
			 return;
		}
		if (!preg_match('/^PAY-.*/', $this->transaction_id))
			throw new Exception("Only transactions fulfilled with PayPal can be refunded automatically");
		$regs = $this->allInTransaction()->as_array();
		
		if (empty($regs))
			throw new Exception("Failed to find matching transactions!");
		
		$allregs = $this->allInTransaction(false);
		
		if ($ticketcount < $this->amount)
			$this->splitAndKeep($ticketcount);
		
		if (count($regs) > 1 or count($allregs) != count($regs))
			$refundAmount = $this->price;
		else 
			$refundAmount = false;
		
		Log::info('Refunding registration :r with transaction ID :trx', [ ':trx' => $this->transaction_id, ':r' => $this->id ]);
		$cancelID = PayPal::refund($this->transaction_id, $refundAmount);
		Log::info('Successfully refunded transaction :trx : :id', [ ':trx' => $this->transaction_id, ':id' => $cancelID ]);
		$this->cancel('PayPal:' . $cancelID);
	}
	
	abstract protected function splitAndKeep($ticketsToKeep);

	public static function setPhoneFulfilment(Model_User $user, $resend = false) {
		$transactions = 0;
		$regs = [];
		foreach ($user->pending_transactions as $reg) { // phone orders can override pending
			if ($reg->price == 0) {
				// free transactions are automatically fullfiled
				$reg->status = self::STATUS_FULFILLED;
				$reg->save();
				continue;
			}
			$reg->status = self::STATUS_PENDING_PHONE;
			$reg->save();
			$transactions += $reg->price;
			$regs[] = $reg;
		}
		
		// compute discount
		list($transactions, $coupon_codes, $coupons_discount) = static::calculateCouponDiscount($user, $transactions);
		if ($coupon_codes) { // record discounts
			foreach ($regs as &$reg) {
				$reg->discount_code = $coupon_codes;
				$reg->save();
			}
		}
	
		$headers = join("\r\n",[
				'From: כנס ביגור <games@bigor.co.il>',
				'Content-type: text/html; charset=utf-8',
				'Message-Id: <controll-ng-' . sha1(microtime()) . '@bigor.org.il>',
				]);
	
		$view = Twig::factory('convention/phone-registration-email');
		$view->user = $user;
		$view->registrations = $regs;
		$view->total = $transactions;
		$view->discount = $coupons_discount;
	
		$to = 'billing@bigor.org.il,oded+bigorgames@geek.co.il';
		//$to = 'oded+bigorgames@geek.co.il';
		$res = @mail($to, 'בקשה להזמנה טלפונית', $view->render(), $headers);
		Log::info('Sending phone registration request to ' . print_r([$to,$res], true));
	}
	
	public static function cancelPhonePending(Model_User $user) {
		$count = 0;
		foreach ($user->phone_pending as $reg) {
			$reg->cancel('phone_cancel');
			$count++;
		}
		return $count;
	}
	
	public static function approvePhonePending(Model_User $user) {
		$trxid = 'phone-' . substr(md5(time()), 0, 20);
		$count = 0;
		$regs = [];
		foreach ($user->phone_pending as $reg) {
			$reg->complete($trxid);
			$regs[] = $reg;
			$count++;
		}
		$user->sendRegisterNotification($regs);
		return $count;
	}
	
	public static function startFulfilment(Model_User $user, $nopaymentNeededAction) {
		$transactions = 0;
		$regs = [];
		foreach ($user->ready_transactions as $reg) {
			if ($reg->price == 0) {
				// free transactions are automatically fullfiled
				$reg->status = self::STATUS_FULFILLED;
				$reg->save();
				continue;
			}
			$reg->status = self::STATUS_PENDING;
			$reg->save();
			$transactions += $reg->price;
			$regs[] = $reg;
		}
	
		// compute discount
		$coupons = [];
		foreach ($user->coupons->where('used','=',0)->find_all() as $coupon) {
			$transactions -= $coupon->consume($transactions); // use coupon
			if ($coupon->used)
				$coupons[] = $coupon->id;
		}
		if (count($coupons)) { // record discounts
			foreach ($regs as &$reg) {
				$reg->discount_code = join(",", $coupons);
				$reg->save();
			}
		}
	
		if ($transactions == 0) { // no pay registrations
			// make sure all registrations are fulfilled
			foreach ($regs as &$reg) {
				$reg->status = self::STATUS_FULFILLED;
				$reg->save();
			}
			Base_Template_Controller::redirect($nopaymentNeededAction);
		}
	
		Log::info("Sending $transactions ILS transaction to paypal: " . join(",", $regs));
		PayPal::payment($transactions, [ 'regs' => $regs ]);
	}
	
	public static function calculateCouponDiscount(Model_User $user, $charge) {
		$coupons = []; $coupons_discount = 0;
		foreach ($user->coupons->where('used','=',0)->find_all() as $coupon) {
			$charge -= $coupon->consume($charge); // use coupon
			if ($coupon->used) {
				$coupons[] = $coupon->id;
				$coupons_discount += $coupon->credit;
			}
		}
		
		return [ 
			$charge, 
			$coupons ? join(",", $coupons) : null,
			$coupons_discount
			];
	}

}
