<?php

class Model_Event extends Base_ORM {
	
	const TAG_KID_FRIENDLY = 'מתאים לילדים';
	const TAG_NEWBIE_FRIENDLY = 'מתאים לשחקנים מתחילים';
	
	protected $_belongs_to = [
			'convention' => [],
			'type' => ['model' => 'Convention_EventType', 'foreign_key' => 'convention_event_type_id'],
			'user' => [],
			'rule_system' => [],
	];
	
	protected $_has_many = [
			'occurrences' => [ 'model' => 'Event_Occurrence' ],
	];

	/**
	 * Validation rules
	 * @see Kohana_ORM::rules()
	 */
	public function rules() {
		return [
			'title' => [ ['not_empty'] ],
			'convention_event_type_id' => [ [ 'numeric' ] ],
		];
	}
	
	/**
	 * Create a new event
	 * @param Model_Convention $convention Convention that holds this event
	 * @param Model_User|array $user user that suggested this event. May not be the host, depending on event_hosts
	 * @param Model_Convention_EventType $eventType event type - used to set initial price and "prevent conflict"
	 * @param string $title event title
	 * @param string $description event description
	 * @param string $tags
	 * @param string $key
	 * @return Model_Event
	 */
	public static function generate(Model_Convention $convention, $user, Model_Convention_EventType $eventType, $title, 
			$description, $rule_system = null, $tags = null, $registration_required = true, $key = null) {
		if (is_null($key)) $key = self::createKey($title);
		$obj = new Model_Event();
		$obj->convention_id = $convention;
		$obj->user_id = $user;
		$obj->key = $key;
		$obj->title = $title;
		$obj->rule_system = $rule_system ? Model_Rule_System::forName($rule_system) : null;
		$obj->convention_event_type_id = $eventType;
		$obj->description = $description;
		// `tags` TEXT, -- Time Slot, Genre, System, Age Restrictions, Type of Convention Event, Cloud Tagging - uses PHP serialization
		// `media` TEXT, -- embedded images and such - uses PHP serialization, maybe convert to external BLOBs?
		$obj->price = $eventType->default_price;
		$obj->registration_required = $registration_required;
		$obj->tags = $tags;
		$obj->save();
		return $obj;
	}
	
	/**
	 * Add a running number to the end of the key or increment an existing running number
	 * @return string
	 */
	public static function incrementKey($key) {
		return preg_replace_callback("/(?:-(\d+))?$/", 
				function($m) { return "-" . (count($m) > 1 ? ($m[1] + 1) : 1); },  
				$key, 1);
	}
	
	public function addTag($tagname) {
		$tagname = trim($tagname);
		$tags = preg_split('/\s*,\s*/', trim($this->tags));
		if (in_array($tagname, $tags)) return; // already exists, no need to add
		$tags[] = $tagname;
		$this->tags = join(",",$tags);
	}
	
	public function removeTag($tagname) {
		$tagname = trim($tagname);
		$tags = preg_split('/\s*,\s*/', trim($this->tags));
		$tags = array_filter($tags, function($tag) use (&$tagname) {
			return $tag != $tagname; 
		});
		$this->tags = join(",",$tags);
	}
	
	/**
	 * Handles converstion of start/end times to epoch times
	 * @see Kohana_ORM::get()
	 */
	public function get($column) {
		switch ($column) {
			case 'occurrence': // helper getter to get the first occurrence
				return $this->occurrences->find();
			case 'tag_list':
				$tags = [];
				foreach (preg_split('/\s*,\s*/', trim($this->tags)) as $tag) {
					if (!$tag) continue;
					$tags[] = (object)[ 
							'key' => preg_replace('/\s+/', '-', trim($tag)),
							'title' => $tag,
							];
				}
				return $tags;
			case 'editor_tag_list':
				$tags = [];
				foreach (preg_split('/\s*,\s*/', trim($this->tags)) as $tag) {
					if (!$tag) continue;
					if ($tag == self::TAG_KID_FRIENDLY || $tag == self::TAG_NEWBIE_FRIENDLY) continue;
					$tags[] = (object)[ 
							'key' => preg_replace('/\s+/', '-', trim($tag)),
							'title' => $tag,
							];
				}
				return $tags;
			case 'newbie_friendly':
				return strstr($this->tags,self::TAG_NEWBIE_FRIENDLY) ? true : false;
			case 'kid_friendly':
				return strstr($this->tags, self::TAG_KID_FRIENDLY) ? true : false;
			case 'registration_required':
				return (parent::get($column) != '0');
			default:
				return parent::get($column);
		}
	}
	
	public function set($column, $value) {
		switch ($column) {
			case 'newbie_friendly':
				if ($value)
					$this->addTag(self::TAG_NEWBIE_FRIENDLY);
				else
					$this->removeTag(self::TAG_NEWBIE_FRIENDLY);
				return $value;
			case 'kid_friendly':
				if ($value)
					$this->addTag(self::TAG_KID_FRIENDLY);
				else
					$this->removeTag(self::TAG_KID_FRIENDLY);
				return $value;
			default:
				return parent::set($column, $value);
		}
	}
	
	public function as_array() {
		$obj = parent::as_array();
		$obj['tag_list'] = $this->tag_list;
		return $obj;
	}

	public function __isset($column) {
		switch ($column) {
			case 'occurrence':
			case 'tag_list':
			case 'editor_tag_list':
			case 'newbie_friendly':
			case 'kid_friendly':
				return true;
			default: return parent::__isset($column);
		}
	}
	
	public function as_object() {
		$obj = parent::as_object();
		$obj->tag_list = $this->tag_list;
		$obj->editor_tag_list = $this->editor_tag_list;
		$obj->newbie_friendly = $this->editor_tag_list;
		$obj->kid_friendly = $this->editor_tag_list;
		return $obj;
	}
}
