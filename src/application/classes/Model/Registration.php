<?php

class Model_Registration extends Model_Base_Transaction {
	
	protected $_belongs_to = [
			'event_occurrence' => [],
			'user' => [],		
	];

	public static function generate(Model_Event_Occurrence $event_occurrence, Model_User $user, $amount = 1) {
		$obj = new Model_Registration();
		$obj->user = $user;
		$obj->event_occurrence = $event_occurrence;
		$obj->amount = $amount;
		$obj->registered = DB::expr('NOW()');
		$obj->price = $event_occurrence->event->price * $amount;
		$obj->status = self::STATUS_IN_PROCESS;
		$obj->save();
		return $obj;
	}
	
	public static function garbageCollect() {
		if (rand(0,99) < 0) // garbage collect event checkout cart 100% of the time
			return;
		
		$m = new Model_Registration();
		DB::delete($m->_table_name)
			->where('status', '=', self::STATUS_IN_PROCESS)
			->where('registered', '<', DB::expr('DATE_ADD(NOW(), INTERVAL -1 HOUR)'))
			->execute($m->_db);
	}
	
	public static function getAll() {
		return (new Model_Registration())->find_all();
	}
	
	public static function getFulfilled(Model_Convention $con) {
		Log::info("Getting fulfillment status for con '{$con->key}'");
		return (new Model_Registration())
			->with('user')
			->with('event_occurrence:location')
			->with('convention')
			->where('registration.status','=',self::STATUS_FULFILLED)
			->where('convention_id', '=', $con->id)
			->order_by('user_id')->find_all();
	}
	
	protected function splitAndKeep($ticketsToKeep) {
		if ($this->amount - $ticketsToKeep <= 0)
			return null; // sanity first
		
		$priceToKeep = $this->event_occurrence->event->price * $ticketsToKeep;
		if ($priceToKeep > $this->price)
			$priceToKeep = $this->price;
		
		$obj = new Model_Registration();
		$obj->user = $this->user;
		$obj->event_occurrence = $this->event_occurrence;
		$obj->amount = $this->amount - $ticketsToKeep;
		$obj->registered = $this->registered;
		$obj->price = $this->price - $priceToKeep;
		$obj->status = $this->status;
		$obj->transaction_id = $this->transaction_id;
		$obj->discount_code = $this->discount_code;
		$obj->issued = $this->issued;
		$obj->save();
		
		$this->amount = $ticketsToKeep;
		$this->price = $priceToKeep;
		
		return $obj;
	}

	public function as_array() {
		$obj = parent::as_array();
		$obj['status_text'] = $this->status_text;
		$obj['authcode'] = $this->authcode;
		return $obj;
	}
	
	/**
	 * @see Kohana_ORM::get()
	 */
	public function get($column) {
		switch ($column) {
			case 'authcode':
				if ($this->status != self::STATUS_FULFILLED)
					return '';
				return $this->event_occurrence->id . ':' . hexdec(substr(md5($this->transaction_id),0,4));
			case 'status_text':
				switch ($this->status) {
					case self::STATUS_IN_PROCESS: return 'עגלת קניות';
					case self::STATUS_PENDING: return 'בקניה';
					case self::STATUS_PENDING_PHONE: return 'רכישה טלפונית';
					case self::STATUS_FULFILLED: return 'שולם';
					case self::STATUS_CANCELLED: return 'בוטל';
					default: return 'לא ידוע';
				}
			case 'event':
				return $this->event_occurrence;
			default:
				return parent::get($column);
		}
	}
	
	public function __isset($column) {
		switch ($column) {
			case 'authcode':
			case 'status_text':
			case 'event':
				return true;
			default: return parent::__isset($column);
		}
	}				

	public function isValid() {
		return $this->status < self::STATUS_CANCELLED;
	}
	
	/* (non-PHPdoc)
	 * @see Kohana_ORM::__toString()
	 */
	public function __toString() {
		return "<reg(".$this->id."):" . $this->event_occurrence->id . "[" . $this->amount . "],u:" . 
				$this->user->id . " => " . $this->status . ">";
	}
	
}
