<?php

class Model_Volunteer extends Base_ORM {
	
	const TYPE_HOST 	= 1;
	const TYPE_VOLUNTEER = 2; // kind of redundant, I know. It means "owned by HR and not Content"
	
	protected $_belongs_to = [
			'user' => [],
			'convention' => [],
	];
	
	public static function generate(Model_User $user, Model_Convention $convention, $type, $constraints = null) {
		$obj = new Model_Volunteer();
		$obj->user = $user;
		$obj->convention = $convention;
		$obj->type = $type;
		$obj->scheduleing_limitations = $constraints;
		$obj->save();
		return $obj;
	}
	
	public static function ensure(Model_User $user, Model_Convention $convention, $type) {
		try {
			$obj = self::generate($user, $convention, $type);
			Log::info("Created volunteering for {$user->id}: {$obj->id}");
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate entry"))
				return; // ignore dups because I just want to ensure it exists
			throw $e;
		}
	}
	
	public static function delist(Model_User $user, Model_Convention $convention, $type) {
		foreach ((new Model_Volunteer())
			->where('user_id', '=', $user)
			->where('convention_id', '=', $convention)
			->where('type','=',$type)->find_all() as $v) 
			$v->delete();
	}
	
	public static function forUserInCon(Model_User $user, Model_Convention $convention) {
		$res = [];
		foreach ((new Model_Volunteer())
			->where('user_id', '=', $user)
			->where('convention_id', '=', $convention)->find_all() as $v) {
			$res[] = $v->type;
		}
		return $res;
	} 
	
	public function type_description() {
		switch ($this->type) {
			case self::TYPE_HOST: return 'מנחה';
			case self::TYPE_VOLUNTEER: return 'מתנדב';
			default:
				return 'לא ידוע';
		}
	}
	
	public function __isset($column) {
		switch ($column) {
			case 'type_text':
			case 'typetext':
				return true;
			default:
				return parent::__isset($column);
		}
	}
	
	public function get($column) {
		switch ($column) {
			case 'type_text':
			case 'typetext':
				return $this->type_description();
			default:
				return parent::get($column);
		}
	}
}
