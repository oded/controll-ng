<?php

class Model_Audit_Log extends Base_ORM {
	
	protected $_belongs_to = array(
			'user' => array(),
			'convention' => array(),
	);

	public static function record(Model_User $user, Model_Convention $convention, $details) {
		// get the caller
		list($me, $caller) = @debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
		$obj = new Model_Audit_Log();
		$obj->convention = $convention;
		$obj->user = $user;
		$obj->entity = @$caller["class"]."::".$caller["function"];
		$obj->transaction_por = $details;
		$obj->save();
	}
}
