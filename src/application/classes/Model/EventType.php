<?php

class Model_EventType extends Base_ORM {

	protected $_table_name = "event_types";
	
	/**
	 * Create a new event type
	 * @param String $title Title of the event type
	 * @param String $key [optional] key to use, instead of generating automatically
	 */
	public static function generate($title, $preventConflicts = true, $key = null) {
		if (is_null($key)) $key = self::createKey($title);
		$obj = new Model_EventType();
		$obj->title = $title;
		$obj->key = $key;
		$obj->prevent_conflicts = $preventConflicts;
		$obj->save();
		return $obj;
	}
	
	/**
	 * Retrieve all records for this model
	 * @return array of model objects
	 */
	public static function getAll() {
		return (new Model_EventType())->find_all();
	}
	
}
