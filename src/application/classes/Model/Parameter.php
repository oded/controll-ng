<?php

class Model_Parameter extends Base_ORM {

	protected $_belongs_to = array('convention' => array());
	
	/**
	 * Create a new parameter
	 * @param Model_Convention $convention convention to configure
	 * @param unknown $name parameter type
	 * @param unknown $value parameter value
	 */
	public static function generate(Model_Convention $convention, $name, $value) {
		$obj = new Model_Parameter();
		$obj->convention = $convention;
		$obj->name = $name;
		$obj->value = $value;
		$obj->save();
		return $obj;
	}
	
}
