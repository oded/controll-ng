<?php

class Model_Merchandise_Transaction extends Model_Base_Transaction {
	
	protected $_belongs_to = [
			'sku' => [ 'model' => 'Merchandise_Sku', 'foreign_key' => 'merchandise_sku_id' ],
			'user' => [],
	];

	public static function generate(Model_Merchandise_Sku $item, Model_User $user, $amount, $notes = null) {
		$obj = new Model_Merchandise_Transaction();
		$obj->merchandise_sku_id = $item->id;
		$obj->user_id = $user->id;
		$obj->amount = $amount;
		$obj->price = $item->merchandise->price * $amount;
		$obj->user_notes = $notes;
		$obj->status = self::STATUS_IN_PROCESS;
		try {
			$obj->save();
			return $obj;
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate"))
				throw new Exception_Duplicate($e->getMessage());
			throw $e;
		}
	}

	public static function getAll() {
		return (new Model_Merchandise_Transaction())->find_all();
	}
	
	protected function splitAndKeep($ticketsToKeep) {
		// TODO: implement splitAndKeep
	}
	
	public static function garbageCollect() {
		if (rand(0,99) < 0) // garbage collect event checkout cart 100% of the time
			return;
		
		$m = new Model_Merchandise_Transaction();
		DB::delete($m->_table_name)
			->where('status', '=', self::STATUS_IN_PROCESS)
			->where('ordered', '<', DB::expr('DATE_ADD(NOW(), INTERVAL -1 HOUR)'))
			->execute($m->_db);
	}
	
	/**
	 * @see Kohana_ORM::get()
	 */
	public function get($column) {
		switch ($column) {
			case 'authcode':
				if ($this->status != self::STATUS_FULFILLED)
					return '';
				return $this->sku->id . ':' . hexdec(substr(md5($this->transaction_id),0,4));
			case 'status_text':
				switch ($this->status) {
					case self::STATUS_IN_PROCESS: return 'עגלת קניות';
					case self::STATUS_PENDING: return 'בקניה';
					case self::STATUS_PENDING_PHONE: return 'רכישה טלפונית';
					case self::STATUS_FULFILLED: return 'שולם';
					case self::STATUS_CANCELLED: return 'בוטל';
					default: return 'לא ידוע';
				}
			case 'description':
				$props = [];
				foreach ($this->sku->properties->find_all() as $prop) {
					$props[] = $prop->feature->type . ': ' . $prop->kind;
				}
				return $this->sku->merchandise->title . ' (' . join(', ', $props) . 
					($this->user_notes ? (', ' . $this->user_notes) : '') . ')';				
			default:
				return parent::get($column);
		}
	}
	
	public function __isset($column) {
		switch ($column) {
			case 'authcode':
			case 'status_text':
			case 'description':
				return true;
			default: return parent::__isset($column);
		}
	}				
	
}
