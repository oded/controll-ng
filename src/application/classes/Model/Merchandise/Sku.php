<?php

class Model_Merchandise_Sku extends Base_ORM {

	protected $_belongs_to = [
			'merchandise' => [],
	];
	
	protected $_has_many = [
			'properties' => [ 'model' => 'Merchandise_Sku_Property' ],
			'orders' => [ 'model' => 'Merchandise_Transaction' ],
	];
	
	public static function generate(Model_Merchandise $merchandise, $key, $stock = null) {
		$obj = new Model_Merchandise_Sku();
		$obj->merchandise_id = $merchandise->id;
		$obj->key = $key;
		$obj->stock = $stock;
		try {
			$obj->save();
			return $obj;
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate"))
				throw new Exception_Duplicate($e->getMessage());
			throw $e;
		}
	}
	
	public function sell(Model_User $user, $amount = 1, $notes = null) {
	    return Model_Merchandise_Transaction::generate($this, $user, $amount, $notes);
	}
	
	public static function byFeatures($features) {
		$q = (new Model_Merchandise_Sku_Property());
		foreach ($features as $feature) {
			$q = $q->or_where_open()
				->where('merchandise_feature_id','=',$feature[0]->id)
				->where('kind','=',$feature[1])
				->or_where_close();
		}
		$skus = [];
		foreach ($q->find_all() as $prop) {
			$skus[$prop->merchandise_sku_id]++;
		}
		arsort($skus);
		reset($skus);
		Log::debug('found prop ' . key($skus));
		if (current($skus) != count($features))
			throw new Exception_NotFound("Missing features required for SKU selection");
		return new Model_Merchandise_Sku(key($skus));
	}
	
	public function __toString() {
		return $this->key;
	}

}
