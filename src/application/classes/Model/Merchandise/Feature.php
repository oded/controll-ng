<?php

class Model_Merchandise_Feature extends Base_ORM {
	
	protected $_belongs_to = [
			'merchandise' => [],
	];
	
	protected $_has_many = [
			'sku_properties' => [ 'model' => 'Merchandise_Sku_Property' ],
	];

	public static function generate(Model_Merchandise $merchandise, $type) {
		$obj = new Model_Merchandise_Feature();
		$obj->merchandise_id = $merchandise->id;
		$obj->type = $type;
		try {
			$obj->save();
			return $obj;
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate"))
				throw new Exception_Duplicate($e->getMessage());
			throw $e;
		}
	}
	
	/**
	 * Return the unique list of property values across all SKUs
	 */
	public function get_sku_properties() {
		foreach ($this->sku_properties->find_all() as $prop) {
			$out[$prop->kind] = 1;
		}
		return array_keys($out);
	}
	
	public function __toString() {
		return $this->type;
	}
	
}
