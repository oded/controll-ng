<?php

class Model_Merchandise_Sku_Property extends Base_ORM {

	protected $_belongs_to = [
			'sku' => [ 'model' => 'Merchandise_Sku', 'foreign_key' => 'merchandise_sku_id'  ],
			'feature' => [ 'model' => 'Merchandise_Feature', 'foreign_key' => 'merchandise_feature_id' ],
	];
	
	public static function generate(Model_Merchandise_Sku $sku, Model_Merchandise_Feature $feature, $kind) {
		$obj = new Model_Merchandise_Sku_Property();
  		$obj->merchandise_feature_id = $feature->id;
  		$obj->merchandise_sku_id = $sku->id;
  		$obj->kind = $kind;
		try {
			$obj->save();
			return $obj;
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate"))
				throw new Exception_Duplicate($e->getMessage());
			throw $e;
		}
	}
	
}
