<?php

class Model_Convention extends Base_ORM {

	public static $allowUnapproved = false;
	
	protected $_has_many = array(
			'organizations'	=> ['model' => 'Organization', 'through' => 'convention_organizers'],
			'parameters'	=> [],
			'events'		=> [],
			'locations'		=> [],
			'event_types'	=> ['model' => 'EventType', 'through' => 'convention_event_types'],
			'merchandise'	=> [],
	);
	
	/**
	 * Create a new convention record
	 * @param String $title Title of the organization
	 * @param String $key [optional] key to use, instead of generating automatically
	 * @param String $hostname [optional] hostname for the convention (if available)
	 */
	public static function generate($title, $key = null, $hostname = null) {
		if (is_null($key)) $key = self::createKey($title);
		$obj = new Model_Convention();
		$obj->title = $title;
		$obj->key = $key;
		$obj->hostname = $hostname;
		$obj->save();
		return $obj;
	}
	
	/**
	 * Handles converstion of start/end times to epoch times
	 * @see Kohana_ORM::get()
	 */
	public function get($column) {
		switch ($column) {
			case "start":
			case "end":
				return strtotime(parent::get($column));
			case 'approved_events':
				return Model_Event_Occurrence::forConvention($this)
					->where_approved()
					->where('start','>=', parent::sqlizeDate($this->start));
			case 'occurrences':
				return Model_Event_Occurrence::forConvention($this);
			case 'registrations':
				return (new Model_Registration())
					->with('event_occurrence:event:convention')
					->with('user')
					->where('convention_id','=',$this->id);
			case 'merchandise_orders':
				return (new Model_Merchandise_Transaction())
					->with('sku:merchandise:convention')
					->where('convention_id','=',$this->id);
			default:
				return parent::get($column);
		}
	}
	
	/**
	 * Retrieve all records for this model
	 * @return array of model objects
	 */
	public static function getAll() {
		return (new Model_Convention())->find_all();
	}
	
	/**
	 * Find the convention with the specified key
	 * @param string $key convention key
	 * @return Model_Convention convention for specified key
	 */
	public static function byKey($key) {
		return (new Model_Convention())->where('key', '=', $key)->find();
	}
	
	/**
	 * Find the convention with the specified hostname or parent domain of specified hostname
	 * @param string $host hostname
	 * @return Model_Convention convention for specified host name
	 */
	public static function byHostname($host) {
		while ($host) {
			$con = (new Model_Convention())->where('hostname', '=', $host)->find();
			if ($con and $con->loaded())
				return $con;
			// remove sub-domain
			$host = join(".", array_slice(explode(".",$host),1));
		}
		return null;
	}
	
	/**
	 * Get a convention event occurrence by its' key
	 * @param string $key event key
	 * @return Model_Occurrence
	 */
	public function occurrenceByKey($key) {
		// try to load an even occurrence using event key
		$ev = (new Model_Event())
			->where('convention_id','=',$this)
			->where('key', '=', $key)
			->find();
		
		if ($ev->loaded()) // found event by key, but we need an occurrence - get the first one
			return $ev->occurrence;
		
		// try to look up an occurrence by id
		$ev = $this->eventOccurrenceById($key);
		if (!$ev->loaded())
			throw new Exception_NotFound("לא נמצא ארוע מתאים");
		return $ev;
	}
	
	/**
	 * Retrieve the correct convention event type for the specified event type key
	 * @param string $key
	 * @return Model_Convention_EventType
	 */
	public function eventType($key) {
		$conevtype = (new Model_Convention_EventType())
			->with("type")
			->where("convention_id","=",$this->pk())
			->where("key","=",$key)
			->find();
		if (!$conevtype->loaded())
			throw new Exception_NotFound("Failed to locate convention event type with key {$key}");
		Log::debug("Found convention event: " . $conevtype->title);
		return $conevtype;
	}

	/**
	 * Retrieve a convention event by its ID.
	 * @param unknown $event_id The event id
	 * @throws Exception_NotFound in case the specified event is not part of this convention
	 * @return Model_Event
	 */
	public function eventById($event_id) {
		$ev = $this->events->where('id','=',$event_id)->find();
		if (!$ev)
			throw new Exception_NotFound();
		return $ev;
	}
	
	/**
	 * Retrieve a convention event occurrence by its ID.
	 * @param unknown $event_occurrence_id The event id
	 * @throws Exception_NotFound in case the specified event occurrence is not part of this convention
	 * @return Model_Event_Occurrence
	 */
	public function eventOccurrenceById($event_occurrence_id) {
		$oc = Model_Event_Occurrence::forConvention($this)->where('event_occurrence.id','=',$event_occurrence_id)->find();
		if (!$oc)
			throw new Exception_NotFound();
		return $oc;
	}
	
	/**
	 * Check if the new event conflicts with an existing event (in the same location and at the same time)
	 * @param Model_Event $newEvent Event that is about to be added
	 * @return array conflicting events
	 */
	public function checkConflicts(Model_Event_Occurrence $newEvent) {
		return $this->checkConflictsByDetails($newEvent->location, $newEvent->start, $newEvent->end, array($newEvent->id));
	}
	
	/**
	 * Check if a new event conflicts with an existing event (in the same location and at the same time)
	 * @param Model_Location $location Location where the event should be schedules
	 * @param int $start start time of the event
	 * @param int $end end time of the event
	 * @param array $ignoreEvents when checking for conflicts ignore the following event ids
	 * @return array conflicting events occurences
	 */
	public function checkConflictsByDetails(Model_Location $location, $start, $end, $ignoreEvents = array()) {
		$conflictingEvents = [];
		
		foreach ($this->events->find_all() as $event) {
			if (array_search($event->id, $ignoreEvents) !== false)
				continue;
			if (is_null($location))
				return [];
			
			foreach ($event->occurrences->find_all() as $occurrence) {
				if ($occurrence->location && $occurrence->location->id == $location->id &&
					! ($occurrence->start >= $end || $occurrence->end <= $start) )
					$conflictingEvents[] = $occurrence;
			}
		}
		
		return $conflictingEvents;
	}
	
	public function __isset($column) {
		switch($column) {
			case 'approved_events':
			case 'registrations':
			case 'merchandise_orders':
			case 'occurrences':
				return true;
			default:
				return parent::__isset($column);
		}
	}
	
}
