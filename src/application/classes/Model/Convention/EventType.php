<?php

class Model_Convention_EventType extends Base_ORM {

	protected $_table_name = "convention_event_types";
	
	protected $_belongs_to = array(
			'type' => array('model' => 'EventType', 'foreign_key' => 'event_type_id'),
			'convention' => array(),
	);

	public function get($column) {
		switch ($column) {
			case 'title':
				return $this->type->title;
			default:
				return parent::get($column);
		}
	}
	
	public function __isset($field) {
		switch ($field) {
			case 'title':
				return true;
			default:
				return parent::__isset($field);
		}
	}
}
