<?php

class Model_Convention_Organizer extends Base_ORM {
	
	protected $_belongs_to = array(
			'convention' => array(),
			'organization' => array(),
	);

	/**
	 * Create a new organizer record
	 * @param Model_Organization $organization
	 * @param Model_Convention $convention
	 */
	public static function generate(Model_Organization $organization, Model_Convention $convention) {
		$obj = new Model_Convention_Organizer();
		$obj->organization = $organization;
		$obj->convention = $convention;
		$obj->save();
		return $obj;
	}
	
	/**
	 * Retrieve all records for this model
	 * @return array of model objects
	 */
	public static function getAll() {
		return (new Model_Convention_Organizer())->find_all();
	}
	
} 
