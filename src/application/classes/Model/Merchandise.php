<?php

class Model_Merchandise extends Base_ORM {

	protected $_table_name = 'merchandise';
	
	protected $_has_many = [
		'features' => [ 'model' => 'Merchandise_Feature' ],
		'skus' => [ 'model' => 'Merchandise_Sku' ],
	];
	
	protected $_belongs_to = [
		'convention' => [],
	];
	
	public static function generate(Model_Convention $convention, $title, $description, $media, $price, $key = null) {
		if (is_null($key))
			$key = self::createKey($title);
		$obj = new Model_Merchandise();
		$obj->convention_id = $convention->id;
		$obj->key = $key;
		$obj->title = $title;
		$obj->description = $description;
		$obj->media = $media;
		$obj->price = $price;
		try {
			$obj->save();
			return $obj;
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate"))
				throw new Exception_Duplicate($e->getMessage());
			throw $e;
		}
	}
	
	public static function getAll() {
		return (new Model_Merchandise())->find_all();
	}
	
	public static function byKey($key) {
		$obj = (new Model_Merchandise())->where('key', '=', $key)->find();
		if (!$obj->loaded())
			throw new Exception_NotFound();
		return $obj;
	}
	
	public static function forConvention(Model_Convention $convention) {
		return (new Model_Merchandise())
				->where('convention_id', '=', $convention->id)
				->find_all();
	}
	
	public function get($column) {
		switch ($column) {
			case 'orders':
				return (new Model_Merchandise_Transaction())->with('sku:merchandise')
						->where('merchandise_id', '=', $this->id);
			default:
				return parent::get($column);
		}
	}
	
	public function as_array() {
	    $obj = parent::as_array();
	    $obj['skus'] = $this->skus->get_all_objects();
	    return $obj;
	}
	
	public function __isset($column) {
		switch ($column) {
			case 'orders':
				return true;
			default:
				return parent::__isset($column);
		}
	}
}
