<?php

class Model_Coupon extends Base_ORM {

	const TYPE_EVENT_HOST = 'event-host';
	
	static $coupon_credits = [
			self::TYPE_EVENT_HOST => [ 'credit' => '20', 'max-coupons' => 2 ],
	];
	
	protected $_belongs_to = [
			'user' => [],
	];
	
	public static function generate(Model_User $user, $type, $credit) {
		$obj = new Model_Coupon();
		$obj->user = $user;
		$obj->type = $type;
		$obj->credit = $credit;
		try {
			$obj->save();
			return $obj;
		} catch (Database_Exception $e) {
			if (strstr($e->getMessage(), "Duplicate"))
				throw new Exception_Duplicate($e->getMessage());
			throw $e;
		}
	}
	
	public static function getAll() {
		return (new Model_Coupon())->find_all();
	}
	
	public static function getSumUsed() {
		foreach (DB::select(array(DB::Expr('SUM(credit)'), 'credit_used'))->from((new Model_Coupon())->_table_name)
				->where('used', '=', 1)->execute() as $row) {
			return (float)$row['credit_used'];		
		}
	}
	
	public static function byUser(Model_User $user, $type, $used = null) {
		$q = (new Model_Coupon())
			->where('user_id', '=', $user)
			->where('type', '=', $type);
		if (!is_null($used))
			$q = $q->where('used','=',$used ? 1 : 0);
		return $q->find_all();
	}
	
	public static function giveEventHostCoupon(Model_User $user) {
		// dont give event host coupon if the user already got as much as is allowed
		$existing_credit = 0;
		foreach (static::byUser($user, self::TYPE_EVENT_HOST) as $c) {
			$existing_credit += $c->credit; // count total credit, regardless if used
		}
		$coupon_type = static::$coupon_credits[self::TYPE_EVENT_HOST];
		if ($existing_credit >= ($coupon_type['credit'] * $coupon_type['max-coupons']))
			return;
		static::generate($user, self::TYPE_EVENT_HOST, $coupon_type['credit']);
	}
	
	public function consume($maxCredit = null) {
		if (is_null($maxCredit)) $maxCredit = $this->credit; // by default consume the entire credit
		if ($maxCredit <= 0) return 0; // can't consume zero credit
		if ($maxCredit >= $this->credit) { // consume the entire coupon
			$this->used = 1;
			$this->save();
			return $this->credit;
		}
		// call wants to consume coupon partially - 
		// consume this one and create a new coupon with the rest of the credit
		$remains = $this->credit - $maxCredit;
		$this->credit = $maxCredit;
		$this->used = 1;
		$this->save();
		static::generate($this->user, $this->type, $remains);
		return $maxCredit;
	}
	
}
