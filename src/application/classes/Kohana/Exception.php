<?php

class Kohana_Exception extends Kohana_Kohana_Exception {
	
	public static function response(Exception $e) {
		if ($e instanceof HTTP_Exception)
			return parent::response($e);
		
		if (@$_COOKIE['site_debug'])
			// normal exceptions are still handled by the Kohana handler if the user is debugging the site
			return parent::response($e);
		
		// log the error
		Log::error("Unexpected error in processing :uri : :message (:class)\n:trace",[
			':uri' => Request::$current ? Request::$current->uri() : $_SERVER['PHP_SELF'],
			':message' => $e->getMessage(),
			':class' => get_class($e),
			':trace' => $e->getTraceAsString(),
		]);
		
		// Create a simple HTTP error 500
		$err = HTTP_Exception::factory(500, "An unexpected error has occured: :message", [
			':message' => $e->getMessage(),
		], $e);
		if (is_null($err->request())) // make sure the exception has an attached request
			$err->request(Request::$current);
		return $err->get_response();
	}
}