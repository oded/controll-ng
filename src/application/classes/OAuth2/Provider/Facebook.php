<?php defined('SYSPATH') OR die('No direct access allowed.');

class OAuth2_Provider_Facebook extends Kohana_OAuth2_Provider_Facebook {
	public function get_user_details(OAuth2_Client $client, OAuth2_Token $token) {
		$url = 'https://graph.facebook.com/v2.2/me?fields=name,gender,birthday,email&access_token=' . urlencode($token->token);
		$details = json_decode(OAuth::remote($url,[]));
		Log::info("Got Facebook details: ".print_r($details, true));
		$details->token = 'facebook:' . $details->id;
		$details->screen_name = $details->name;
		return $details;
	}
}
