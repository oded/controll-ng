<?php defined('SYSPATH') OR die('No direct access allowed.');

class OAuth2_Provider_Google extends Kohana_OAuth2_Provider_Google {
	public function get_user_details(OAuth2_Client $client, OAuth2_Token $token) {
		$url = 'https://www.googleapis.com/plus/v1/people/me?access_token=' . urlencode($token->token);
		$details = json_decode(OAuth::remote($url,[]));
		Log::info("Got Google details: ".print_r($details, true));
		$details->token = 'google:' . $details->id;
		$details->screen_name = $details->displayName;
		foreach ($details->emails as $email) {
			if ($email->type == 'account')
				$details->email = $email->value;
		}
		return $details;
	}
}
