<?php

/**
 * Implementation of email sending of template
 * @author oded
 */
class Email {
	
	private $headers = null;
	private $view = null;
	private $subject = null;
	
	public function __construct($subject, $template, $variables = []) {
		$this->headers = join("\r\n",[
				'From: כנס ביגור <info@bigor.org.il>',
				'Reply-To: info@bigor.org.il',
				'Content-type: text/html; charset=utf-8',
				'Message-Id: <controll-ng-' . sha1(microtime()) . '@bigor.org.il>',
		]);
		$this->view = Twig::factory('convention/email/' . $template);
		foreach ($variables as $key => $value)
			$this->view->$key = $value;
		$this->subject = $subject;
	}
	
	public function send($recipients) {
		@mail($recipients, $this->subject, $this->view->render(), $this->headers);
	}
	
}