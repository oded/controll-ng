<?php

if (!defined('DEFAULT_TIMEZONE'))
	define('DEFAULT_TIMEZONE',"Asia/Jerusalem");

class Base_Controller extends Controller {
	protected $view;
	protected $convention;
	protected $session;
	protected $timezone;
	protected $user;
	protected $viewkey;
	
	/* (non-PHPdoc)
	 * @see Kohana_Controller::__construct()
	 */
	public function __construct(Request $request, Response $response) {
		$this->timezone = new DateTimeZone(DEFAULT_TIMEZONE);
		try {
			parent::__construct($request, $response);
			$this->session = Session::instance();
			date_default_timezone_set(DEFAULT_TIMEZONE);
			$this->user = $this->session->loggedInUser();
		} catch (HTTP_Exception $e) {
			throw $e;
		} catch (Exception $e) {
			// error during controller setup - report and try to be nice
			throw HTTP_Exception::factory(500, $e->getMessage(), [], $e);
		}
	}
	
	/**
	 * Sets the view for the current operation
	 * @param unknown $viewid
	 * @return View
	 */
	protected function buildView($viewid) {
		$view = Twig::factory($viewid);
		// expose logged in user
		if ($view->user = $this->user) {
			$view->user->resolveAdmin($this->convention);
			$view->user->inConvention($this->convention);
			Model_Convention::$allowUnapproved = $view->user->isAdmin();
		}
		$view->convention = $this->convention;
		$view->request = $this->request;
		return $view;
	}
	
	protected function setView($viewid) {
		return $this->view = $this->buildView($viewid);
	}
	
	public function before() {
		parent::before();
		
		if (!is_null($this->request->site_debug)) {
			setcookie('site_debug', $this->request->site_debug, time() + 3600, '/');
		} 

		// check if we have a convention key and init it
		$conkey = $this->request->param('convention');
		if ($conkey)
			$this->convention = Model_Convention::byKey($conkey);
		// otherwise, try to resolve by host name
		else {
			$this->convention = Model_Convention::byHostname($_SERVER['HTTP_HOST']);
			//Log::debug("Selecting convention by hostname '{$_SERVER['HTTP_HOST']}': " . $this->convention->key);
		}
		
		if (!$this->convention) {
			throw new Exception("כנס לא מוכר - אנא בדקי את ההגדרות");
		}
		
		// init view
		$this->viewkey = strtolower(preg_replace('/^Controller_/', '', get_class($this)));
		if (empty($this->view)) $this->view = str_replace('_','/',$this->viewkey);
		$this->view = $this->buildView($this->view);
	}
	
	public function after() {
		$this->response->headers('X-UA-Compatible', 'IE=Edge');
		parent::after();
	}
	
	protected function jsonResponse($data) {
		$this->after();
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		$this->response->body(json_encode($data,JSON_UNESCAPED_UNICODE));
	}
	
}
