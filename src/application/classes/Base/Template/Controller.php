<?php

class Base_Template_Controller extends Base_Controller {
	static $default = 'default'; // default template
	
	protected $auto_render;
	protected $template;
	protected $page_type = 'home';
	
	protected function setView($viewid) {
		parent::setView($viewid);
		return $this->template->content = $this->view;
	}
	
	public function before() {
		parent::before();
		
		// set up auto_render (if not overridden)
		if (is_null($this->auto_render)) $this->auto_render = true;
		
		// init template
		if (is_null($this->template)) $this->template = static::$default;
		date_default_timezone_set(DEFAULT_TIMEZONE);
		$this->buildTemplate();
	}
	
	protected function buildTemplate() {
		$this->template = $this->buildView('templates/' . $this->template);
		
		// bind view to template
		$this->template->content = $this->view;
		
		$this->template->current = $this->viewkey;
		$this->template->canonical = URL::base(true) . preg_replace('/^\//', '', $this->request->uri());
		$this->template->page_type = $this->page_type;
		$this->template->tagcloud = $this->getTagCloud();
	}
	
	public function after() {
		parent::after();
		
		$this->view->canonical = $this->template->canonical;		
		// retrieve transient error for display on the page
		$err = $this->session->error();
		if ($err)
			$this->view->error = $this->template->error = $err;
		
		if ($this->auto_render) {
			// pre-render the view content, so if it breaks we get a good stack trace
			$this->template->content = (string)$this->view; 
			$this->response->body($this->template);
		}
	}
	
	public function redirectToAction($action, $suffix = '') {
		$controller_name = str_replace("Controller_Convention_", "", get_class($this));
		
		$this->redirect($this->request->route()->uri(array(
				'convention' => $this->convention->key,
				'controller' => $controller_name,
				'action' => $action,
		)) . $suffix);
	}
	
	public static function setRedirectRules($rule, $target = null) {
		URL::setRedirectRules($rule, $target);
	}
	
	protected function jsonResponse($data) {
		$this->auto_render = false;
		return parent::jsonResponse($data);
	}

	private function getTagCloud() {
		$tags = [];
		foreach ($this->convention->approved_events->find_all() as $oc) {
			$tags = array_merge($tags, preg_split('/\s*,\s*/', $oc->event->tags));
		}
		if (!$tags)
			return []; // don't crash if we have no tags in the system
	
		$frequency_list = [];
		foreach ($tags as $tag) {
			if (!$tag) continue;
			@$frequency_list[$tag]++; // auto-vivification for the win!
		}
	
		$tagcloud = [];
		$fmin = min($frequency_list); $fmax = max($frequency_list);
		foreach ($frequency_list as $tag => $freq) {
			$tagcloud[] = (object)[
			'title' => $tag,
			'ratio' => $fmax == $fmin ? 1 : ($freq - $fmin) / ($fmax - $fmin),
			'key' => urldecode(preg_replace('/\s+/', '-', $tag)),
			];
		}
		return $tagcloud;
	}
	
}
