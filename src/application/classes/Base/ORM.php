<?php

class Base_ORM extends ORM {
	
	/**
	 * Generate a key based on the specified title
	 * @param String $title Title to parse
	 */
	public static function createKey($title) {
		return preg_replace('/[^0-9a-zא-ת]+/','-',strtolower($title));
	}
	
	protected static function sqlizeDate($date) {
		if (is_null($date)) return null;
		if (is_int($date)) return date('Y-m-d H:i:s',$date); // convert epoch times
		return $date; // DateTime object?
	}
	
	public static function parseDateString($text) {
		$parsed_date = date_parse_from_format("d-m-Y H:i", $text);
		extract($parsed_date);
		return mktime($hour, $minute, $second, $month, $day, $year);
	}

	public function get_all_objects() {
		$res = [];
		foreach ($this->find_all() as $obj) {
			$res[] = $obj->as_object();
		}
		return $res;
	}
	
	public function as_object() {
		$object = $this->as_array();

		foreach ($this->_related as $column => $model)
		{
			// Include any related objects that are already loaded
			$object[$column] = $model->as_object();
		}
	
		return (object)$object;
	}
	
}