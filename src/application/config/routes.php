<?php

return array(
		'authenticate' => [
				'uri' => '<controller>(/<action>)',
				'rules' => [ 'controller' => '(authenticate|register)' ],
				'defaults' => [
						'controller' => 'authenticate',
						'action' => 'index',
				]			
		],

		'convention-eventlist' => [
				'uri' => 'events',
				'defaults' => [
						'directory' => 'convention',
						'controller' => 'EventList',
						'action' => 'index',
				],
		],

		'convention-event-operations' => [
				'uri' => 'events/<action>',
				'defaults' => [
						'directory' => 'convention',
						'controller' => 'event',
				],
		],
		
		'convention-event' => [
				'uri' => 'event/<event>(/<action>)',
				'defaults' => [
						'directory' => 'convention',
						'controller' => 'event',
						'action' => 'index',
				],
		],
		
		'convention-user' => [
				'uri' => 'user/<action>(/<id>)',
				'defaults' => [
						'directory' => 'convention',
						'controller' => 'user',
						'action' => 'index',
				],
		],
		
		'convention-shop-front' => [
				'uri' => 'shop',
				'defaults' => [
						'directory' => 'convention',
						'controller' => 'shop',
						'action' => 'list',
				],
		],
		
		'convention-shop' => [
				'uri' => 'shop/<item>(/<action>)',
				'defaults' => [
						'directory' => 'convention',
						'controller' => 'shop',
						'action' => 'index',
				],
		],
		
		'convention-admin' => [
				'uri' => 'admin(/<action>)',
				'defaults' => [
						'directory' => 'convention',
						'controller' => 'admin',
						'action' => 'index',
				],
		],

		'convention-cashier' => [
				'uri' => 'cashier(/<action>)',
				'defaults' => [
						'directory' => 'convention',
						'controller' => 'cashier',
						'action' => 'index',
				],
		],

		'convention-operations' => [
				'uri' => 'api/<action>',
				'defaults' => [
					'directory' => 'convention',
					'controller' => 'operations',
				]
		],
		
// 		'general-controllers' => array(
// 			'uri' => '(<controller>(/<action>(/<id>)))',
// 			'defaults' => array(
// 				'controller' => 'welcome',
// 				'action'     => 'index',
// 			),
// 	    ),
			
);
