<?php defined('SYSPATH') OR die('No direct access allowed.');

return [
        'default' => [
                'type'                  => 'MySQLi',
                'connection'    => [
                        'hostname'      => 'localhost',
                        'username'      => 'controll',
                        'password'      => 'controll',
                        'database'      => 'controll',
                        'persistent'    => TRUE,
                        'port'			=> 3306,
                ],
                'table_prefix'  => '',
                'charset'               => 'utf8',
                'caching'               => FALSE,
                'profiling'             => FALSE,
        ],
		
		'securehost' => [
                'type'                  => 'MySQLi',
                'connection'    => [
                        'hostname'      => 'localhost',
                        'username'      => 'gibor_controlll',
                        'password'      => 'secretcontrol',
                        'database'      => 'gibor_controll',
                        'persistent'    => TRUE,
                        'port'			=> 3306,
                ],
                'table_prefix'  => '',
                'charset'               => 'utf8',
                'caching'               => TRUE,
                'profiling'             => FALSE,
		                ],
		
		'bluehost' => [
				'type'                  => 'MySQLi',
				'connection'    => [
						'hostname'      => 'localhost',
						'username'      => 'bigororg_system',
						'password'      => 'vfQJm7AgKSSzAuGE',
						'database'      => 'bigororg_controllng',
						'persistent'    => TRUE,
						'port'			=> 3306,
				],
				'table_prefix'  => '',
				'charset'               => 'utf8',
				'caching'               => TRUE,
				'profiling'             => FALSE,
		],
];