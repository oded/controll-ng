#!/bin/bash

function get_volumes() {
    for vol in \
	bigor-2015-html:/home2/bigororg/public_html/2015 \
	html:/home2/bigororg/public_html/controll \
	wordpress-plugin:/home2/bigororg/public_html/2015/wp-content/plugins/controll \
	src/application/classes:/home2/bigororg/controll-ng/application/classes \
	src/application/views:/home2/bigororg/controll-ng/application/views \
	src/application/config:/home2/bigororg/controll-ng/application/config \
	; do
	echo "-v $(pwd)/$vol"
    done
}

docker build -t bigor . && \
docker run -ti --rm -p 80:80 -p 3306:3306 --name bigor \
    $(get_volumes) \
    bigor /bin/bash -xc '
    #mysqld_safe --user mysql >/var/log/mysqld.log 2>&1 &
    /usr/libexec/mysqld --user mysql >/var/log/mysqld.log 2>&1 &
    mkdir -p /run/httpd
    /usr/sbin/httpd
    PID=
    function dotail() { tail -F -n 0 /var/log/mysqld.log /var/lib/mysql/*.log /var/log/httpd/*error* /home2/bigororg/controll-ng/application/logs/*/*/* & PID=$!; }
    dotail
    inotifywait -mrqe CREATE /home2/bigororg/controll-ng/application/logs /var/log/httpd /var/lib/mysql | while read ev; do
        [ -n "$PID" ] && kill $PID
        dotail
    done
'
