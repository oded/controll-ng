<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package Cryout Creations
 * @subpackage parabola
 * @since parabola 0.5
 */
?>	<div style="clear:both;"></div>
	</div> <!-- #forbottom -->
	</div><!-- #main -->


	<footer id="footer" role="contentinfo">
		<div id="colophon">
		
			<?php get_sidebar( 'footer' );?>
			
		</div><!-- #colophon -->

		<div id="footer2">
			<div id="footer2-inner">
				<?php cryout_footer_hook(); ?>
			</div>
		</div><!-- #footer2 -->

	</footer><!-- #footer -->

</div><!-- #wrapper -->

<?php	wp_footer(); ?>
<script>
// Include the UserVoice JavaScript SDK (only needed once on a page)
UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/5vAtSDGbNp0bNy8ZJ1Aug.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();
// Set colors
UserVoice.push(['set', { accent_color: '#e2753a', trigger_color: 'white', trigger_background_color: 'rgba(46, 49, 51, 0.6)', trigger_style: 'tab', strings: { contact_message_placeholder: 'שלח\\י משוב או בקש\\י עזרה', contact_title: 'שלח לנו הודעה', smartvote_menu_label: 'מה תרצו שנוסיף באתר?', post_suggestion_title: 'הציעו רעיון', post_suggestion_body: 'כשאתם מציעים רעיון לשיפור באתר, זה מופיע בפורום התמיכה שלנו ואנשים אחרים יכולים להעיר הערות', post_suggestion_message_placeholder: 'תאר\\י את הרעיון שלך'} }]);
UserVoice.push(['addTrigger', { mode: 'contact', trigger_position: 'left' }]);
</script>
</body>
</html>
