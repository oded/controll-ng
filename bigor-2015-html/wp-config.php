<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/** Languages */
define('WPLANG', 'he_IL');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bigororg_wo2608');

/** MySQL database username */
define('DB_USER', 'bigororg_wo2608');

/** MySQL database password */
define('DB_PASSWORD', 'rVK97osSY87n');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'HsHGz@W+Z?Wng?N-|]wEmwsv+fTIYA*AzJb!wl(Z(>%CfD]GIpwi@({{_WaZuM@>RPdCDUyQF$awctQ)OV/Vkucp&OU*VY(dC^PutZ?]$Lhug*RhB]QzJQsr/meAEq;$');
define('SECURE_AUTH_KEY', '{QNI|X+)Md];Dblc*j?GCFYZw=HsF%/]DI_hyLBnrOIW@Pi+J)QM$Eu+f/x(;-h=ZoEcy=kZlZhq=nDagmCnTeJ(QwQhI&BfBrGgblK=F@QQR([Rg^OZzx(n^HCp;/eY');
define('LOGGED_IN_KEY', 'Q-Cz>h=iP&{PJBPtqP>R^?sjeU+VjsEZPQ=cbc-MB+kg/=E$ylmvdMZ<)!rIbBOr|ea@ZfZ=i%kA)W)!si+yyxfUqK[q)BBscug|M{{y<nj^z;HkEZSkHZmc(e!TNqYD');
define('NONCE_KEY', 'D{rUIM{lU=^moG/tvxJ@llp)-gc;$pM(Svu+RXkYnhpvqiZWy_C;S?B;MrcVu<hVcqn-+n$Cw?GqFCoceVtO[leynxDTYFBlMTwv=&wN+EX)/MJVzoUN%v{Giniz/NM)');
define('AUTH_SALT', 'x[-]oK+LswcMQmzb/cy^/HjRw=!w]-^GPOtBoY>+/J%qUkF*QkJcIqzDW;akFRn{^&)JZP<xT]PIzN]cV%{m+je^DBJMiZ(s%HXcJ&=F]sR![MAYi!]HMdb$R?!s;wmV');
define('SECURE_AUTH_SALT', 'a%zddJuy!*+)kx_sOH>Bys)^;rlWgSSvhbhrCQnWnhT/Cm]C]Gz>vS-nvi%O/F=jtixpB|>J*KYJTTE[[t]w%)eW!S_f-zK;s{IdsFw>iV_M;V+{E;$<vLCv+tB/<qqO');
define('LOGGED_IN_SALT', 'Px>Y%M]%{LH}r+g|QyWJ!IEN=Z!rywJg!FxZiojNS{ucsk{=GSvMR/cAq;N!n[n]Z!(WS_N>epawfbHQB@Lhv!^@DGav&}wD}S=cyC|DjvYkJng(+uO!keu}SQOX%D;l');
define('NONCE_SALT', ')$vm^WiWJa^;Si@?Uhbx&n>I$AIwYaNprjuYOVy(_F|!zd@C)bd>}r$n+;lhLHV-&ilD=OMWLpSYJMIgjn(%Otc)kNO!ZVfXvNfGvHkq[&!SHL<hCxe{Wm+Ph[vRAta{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_rqoi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 * Include tweaks requested by hosting providers.  You can safely
 * remove either the file or comment out the lines below to get
 * to a vanilla state.
 */
if (file_exists(ABSPATH . 'hosting_provider_filters.php')) {
	include('hosting_provider_filters.php');
}