/*
	Simple OpenID Plugin
	http://code.google.com/p/openid-selector/
	
	This code is licensed under the New BSD License.
*/

openid.providers_large = {
	google : {
		name : 'Google',
		url : 'https://www.google.com/accounts/o8/id'
	},
	yahoo : {
		name : 'Yahoo',
		url : 'http://me.yahoo.com/'
	},
	/*aol : {
		name : 'AOL',
		label : 'Enter your AOL screenname.',
		url : 'http://openid.aol.com/{username}'
	},*/
	/*myopenid : {
		name : 'MyOpenID',
		label : 'Enter your MyOpenID username.',
		url : 'http://{username}.myopenid.com/'
	},*/
	openid : {
		name : 'OpenID',
		label : 'הקש את ה-OpenID שלך:',
		url : null
	}
};

openid.providers_small = {
	livejournal : {
		name : 'LiveJournal',
		label : 'הקש את שם Livejournal שלך:',
		url : 'http://{username}.livejournal.com/'
	},
	/* flickr: {
		name: 'Flickr',        
		label: 'Enter your Flickr username.',
		url: 'http://flickr.com/{username}/'
	}, */
	/* technorati: {
		name: 'Technorati',
		label: 'Enter your Technorati username.',
		url: 'http://technorati.com/people/technorati/{username}/'
	}, */
	wordpress : {
		name : 'Wordpress',
		label : 'הקש את שם Wordpress.com שלך:',
		url : 'http://{username}.wordpress.com/'
	},
	blogger : {
		name : 'Blogger',
		label : 'הקש את חשבון ה-Blogger שלך:',
		url : 'http://{username}.blogspot.com/'
	},
	verisign : {
		name : 'Verisign',
		label : 'הקש את שם Verisign שלך:',
		url : 'http://{username}.pip.verisignlabs.com/'
	},
	/* vidoop: {
		name: 'Vidoop',
		label: 'Your Vidoop username',
		url: 'http://{username}.myvidoop.com/'
	}, */
	launchpad: {
		name: 'Launchpad',
		label: 'הקש את שם Launchpad שלך:',
		url: 'https://launchpad.net/~{username}'
	}, 
	/*claimid : {
		name : 'ClaimID',
		label : 'Your ClaimID username',
		url : 'http://claimid.com/{username}'
	},*/
	/*clickpass : {
		name : 'ClickPass',
		label : 'Enter your ClickPass username',
		url : 'http://clickpass.com/public/{username}'
	},*/
	/*google_profile : {
		name : 'Google Profile',
		label : 'Enter your Google Profile username',
		url : 'http://www.google.com/profiles/{username}'
	}*/
};

openid.locale = 'he';
openid.sprite = 'he'; // reused in german& japan localization
openid.signin_text = 'כניסה';
openid.image_title = 'הכנס עם {provider}';
openid.no_sprite = true;
