var clientIsMobile = function() {
	return window.screen.width < 600;
}

var authentication_basedir = '/controll/';

var setAuthenticationBaseDir = function(dir) {
  authentication_basedir = dir +'/';
}

/**
 * Username/Password based login process
 */
var passwordLogin = function() {
	var loginButton = jQuery('#login-start');
	var emailPattern = /^[\w_\.\+-]+@(?:[a-zA-Z_]+\.)+[a-zA-Z]{2,4}$/;
	var enableEmailButton = function(e){
		if (emailPattern.test(jQuery('#login-email').val()))
			loginButton.removeAttr('disabled');
		else
			loginButton.attr('disabled', 'disabled');
	};
	jQuery('#login-email').keyup(enableEmailButton).change(enableEmailButton).mouseout(enableEmailButton).keypress(function(e) {
	    if(e.which == 13) loginButtonClickHandler();
	});
	
	loginButton.click(loginButtonClickHandler);
};

var loginButtonClickHandler = function(){
	window.loginUsername = jQuery('#login-email').val();
	// setup password reset email as needed
	jQuery('#reset-password-link').attr('href', '?resetpassword=' + window.loginUsername);
	// ask the server if the user can log in or needs to be registered first
	jQuery.post(authentication_basedir + 'authenticate/login', {
		auth: 'standard',
		login: window.loginUsername
	}, function(response) {
		if (typeof response != 'object') {
			window.location.href = authentication_basedir + 'authenticate/clienterror';
			return;
		}
		
		else if (response.status) {
			if (response.hasPassword)
				startPasswordLoginStep();
			else // try to log in using the user's auth provider
				window.location.href = jQuery('#loginbox-' + response.provider).attr('href');
		} else {
			startPasswordRegistration();				
		}
	});
};

var startPasswordRegistration = function() {
	if (clientIsMobile()) {
		jQuery('.loginbox.stage1').hide();
		jQuery('.loginbox.stage2').show();
		passwordRegistrationStep();
		return;
	}
	jQuery('.loginbox.stage2').fadeIn(400, passwordRegistrationStep);
	jQuery('.loginbox.stage1').fadeOut(400);
};

/**
 * Password based registration
 */
var passwordRegistrationStep = function() {
	jQuery('#login-password-ok').click(passwordRegistration);
	var checkpassword = function() {
		if (jQuery('#login-reg-password2').val() && jQuery('#login-reg-password1').val() != jQuery('#login-reg-password2').val())
			jQuery('#password-confirmation').fadeIn(250);
		else
			jQuery('#password-confirmation').fadeOut(250);
		
		if (jQuery('#login-reg-password1').val() == jQuery('#login-reg-password2').val()) 
			jQuery('#login-password-ok').removeAttr('disabled');
		else
			jQuery('#login-password-ok').attr('disabled', 'disabled');
	}
	jQuery('#login-reg-password2, #login-reg-password1').keyup(checkpassword).change(checkpassword);
};

var passwordRegistration = function() {
	var passwordVal = jQuery('#login-reg-password2').val();
	jQuery.post(authentication_basedir + 'authenticate/register', {
		auth: 'standard',
		login: window.loginUsername,
		password: passwordVal,
		return_url: window.location.href
	}, function(response) {
		if (typeof response != 'object') {
			window.location.href = authentication_basedir + 'authenticate/clienterror';
			return;
		}
		
		if (response.status === false) {
			window.location.href = authentication_basedir + 'authenticate/clienterror?message=' + encodeURIComponent(response.message);
			return;
		}
		
		window.location.href = response.register_url;
	});
};

var startPasswordLoginStep = function() {
	if (clientIsMobile()) {
		jQuery('.loginbox.stage2').hide();
		jQuery('.loginbox.stage3').show();
		jQuery('#login-password').focus();
		jQuery('#login-enter').click(passwordLoginStep);
		return;
	}
	jQuery('.loginbox.stage3').fadeIn(400, function() {
		jQuery('#login-password').focus().keypress(function(e) {
		    if(e.which == 13) passwordLoginStep();
		});
		jQuery('#login-enter').click(passwordLoginStep);
	});
	jQuery('.loginbox.stage2').fadeOut(400);
}

var passwordLoginStep = function() {
	var passwordVal = jQuery('#login-password').val();
	jQuery.post(authentication_basedir + 'authenticate/login', {
		auth: 'standard',
		login: window.loginUsername,
		password: passwordVal
	}, function(response){
		if (typeof response != 'object') {
			window.location.href = authentication_basedir + 'authenticate/clienterror';
			return;
		}
		
		if (response.status) {
			jQuery('#modal-authenticate').dialog('close');
			if (matches = window.location.search.match(/return_url=([^&]+)/)) {
				window.location = decodeURIComponent(matches[1]);
			} else {
				window.location.reload();
			}
		} else {
			alert(response.message);
		}
	});
};

function showAuthenticateDialog(return_url) {
	if (clientIsMobile()) {
		// start mobile view
		window.location.href = authentication_basedir + 'authenticate/mobile?return_url=' + encodeURI(return_url);
	}
	jQuery('body').append('<div id="modal-authenticate" style="display: none"></div>');
	jQuery('#modal-authenticate').load(authentication_basedir + 'authenticate?return_url=' + encodeURI(return_url),'',function(){
		jQuery('#modal-authenticate').show();
		jQuery('#modal-authenticate').dialog({
			dialogClass: 'modal-authenticate',
			modal: true, draggable: false, resizable: false,
			width: 600, height: 360,
			open: function() { 
				jQuery('.ui-widget-overlay').click(function(){ jQuery('#modal-authenticate').dialog('close'); });
				jQuery('#login-email').focus();
				passwordLogin();
			},
			close: function() { jQuery('#modal-authenticate').remove(); },
		});
	});
}

/**
 * "Login" button 
 */
jQuery(function() {
	jQuery('#authenticate').click(function(){ showAuthenticateDialog(window.location.href); });
});

